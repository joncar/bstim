<?php
$labels = array(
    "custom_U5497"=>"Nombre",
    "Email"=>"Email",
    "custom_U5493"=>"Mensaje",
    "custom_U5734"=>"Telefono",
    "custom_U5739"=>"Sexo"
);
$string_to_export = "";
$correos = file_get_contents('contactenos.json');
$correos = json_decode($correos);
//Cabecera
if(count($correos)>0){
    foreach($correos[0] as $n=>$c){
        $string_to_export .= $labels[$n]."\t";
    }
    $string_to_export .= "\n";
    
    foreach($correos as $c){
        foreach($c as $c1){
            $string_to_export .= $c1."\t";
        }
        $string_to_export .= "\n";
    }
}
//echo $string_to_export;

// Convert to UTF-16LE and Prepend BOM
$string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-16LE', 'UTF-8');
$filename = "export-".date("Y-m-d_H:i:s").".xls";
header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
header('Content-Disposition: attachment; filename='.$filename);
header("Cache-Control: no-cache");
echo $string_to_export;
die();