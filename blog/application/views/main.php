<div class="blog-page loaded" id="page-content"> <!-- blog page header with video bg --> 
        <header data-bg="<?= base_url() ?>images/blog-page-bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/blog-page-bg.jpg);">
           
            <div class="header-center-content"> 
                <div class="container text-center"> 
                    <div class="row section-intro animated fadeInUpBig"> 
                        <div class="col-md-12"> 
                            
                            <h1 class="text-uppercase" <p style="letter-spacing: 10px; ">Notícies</h1> 
                            <p style="margin: 0px; "> 
                                Estigues al dia, informa't de totes les novetats de BSTIM.
                            </p>
                            <p class="classy" style="margin: 0px;">superadornment</p>
                            <span class="vice">unhuntable vindicated</span>
                            <span class="poll">pithecanthropid</span> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </header> <!-- /.blog page header with video bg --> <!-- main content --> 
        <main> 
            <div class="blog-page-wrapp"> 
                <div class="container"> 
                    <div class="row"> <!-- sidebar --> 
                        <?php $this->load->view('frontend/_categorias'); ?>
                        
                        <div class="col-md-9">
                            <?php foreach($detail->result() as $n=>$d): ?>
                                <?php $this->load->view('frontend/_entry',array('detail'=>$d,'n'=>$n)); ?>
                            <?php endforeach ?>
                            <?php if($detail->num_rows==0): ?>
                                No es troben entrades
                            <?php endif ?>
                                
                             <!-- pagination --> 
                            <nav data-wow-delay="0.2s" class="wow fadeInUp" aria-label="Page navigation" style="animation-delay: 0.2s; animation-name: none;"> 
                                <?php $this->load->view('predesign/paginacion',$paginacion); ?>
                            </nav> <!-- /.pagination --> 
                        </div> <!-- /.blog posts intro list --> 
                    </div> 
                </div> 
            </div> 
          
</div>