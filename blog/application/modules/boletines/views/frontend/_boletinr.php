<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="ISO-8859-1">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css" id="Mail Designer General Style Sheet">
            a { word-break: break-word; }
            a img { border:none; }
            img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
            body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family:'Arial' !important;}
            .ExternalClass { width: 100%; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            #page-wrap { margin: 0; padding: 0; width: 100% !important;}
            #outlook a { padding: 0; }
            .preheader { display:none !important; }
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: 'Arial' !important; font-weight: inherit !important; line-height: inherit !important; }
        </style>
        <style type="text/css" id="Mail Designer Mobile Style Sheet">  
            .hide {
                max-height: 0 !important;
                display: none !important;
                overflow: hidden !important;
                font-size:0;
                mso-hide:all;
                width:0;
            }
            @media only screen and (max-width: 720px) {
                table[class=email-body-wrap] {
                    width: 320px !important;
                }
                td[class=page-bg-show-thru] {
                    display: none !important;
                }
                table[class=layout-block-wrapping-table] {
                    width: 320px !important;
                }
                table[class=mso-fixed-width-wrapping-table] {
                    width: 320px !important;
                }
                *[class=layout-block-full-width] {
                    width: 320px !important;
                }
                table[class=layout-block-column], table[class=layout-block-padded-column] {
                    width: 100% !important;
                }
                table[class=layout-block-box-padding] {
                    width: 100% !important;
                    padding: 5px !important;
                }
                table[class=layout-block-horizontal-spacer] {
                    display: none !important;
                }
                tr[class=layout-block-vertical-spacer] {
                    display: block !important;
                    height: 8px !important;
                }
                td[class=container-padding] {
                    display: none !important;
                }

                table {
                    min-width: initial !important;
                }
                td {
                    min-width: initial !important;
                }

                *[class~=desktop-only] { display: none !important; }
                *[class~=mobile-only] { display: block !important; }

                .hide {
                    max-height: none !important;
                    display: block !important;
                    overflow: visible !important;
                    font-size:inherit;
                }

                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-7FC5D3D0-8C6D-4FEE-9CF1-3545D1D19E3E"] { height:17px !important; } /* vertical spacer */

                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-AFADBC7C-37B4-468F-811E-2BCE29DE02F9"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-111D93B9-D011-41A8-A837-1A85B7783DFE"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-497AB408-00D3-4687-9D34-5BDE49089F01"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-1A20A89F-3924-426B-96B8-34027D605D40"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-497AB408-00D3-4687-9D34-5BDE49089F01"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-1A20A89F-3924-426B-96B8-34027D605D40"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-padding-left"] { width: 12px !important; }
                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-padding-right"] { width: 12px !important; }
                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-content-cell"] { width: 296px !important; }
            }
        </style>
        <!--[if gte mso 9]>
        <style type="text/css" id="Mail Designer Outlook Style Sheet">
                table.layout-block-horizontal-spacer {
                    display: none !important;
                }
                table {
                    border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    mso-table-bspace:0pt;
                    mso-table-tspace:0pt;
                    mso-padding-alt:0;
                    mso-table-top:0;
                    mso-table-wrap:around;
                }
                td {
                    border-collapse:collapse;
                    mso-cellspacing:0;
                }
        </style>
        <![endif]-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        
    </head>
    <body style="margin-top: 10px; margin-right: 0px; margin-bottom: 80px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 50px; padding-left: 0px; " eqid="EQMST-5B629705-2068-4643-A1B4-1BA752D88F04" background="<?= base_url('images/boletines')?>/page-bg.jpg">
        <center><a href="<?= base_url('boletines/frontend/ver/'.$detail->id) ?>" style="color:#333; font-size: 12px; margin-top: 20px; margin-bottom: 20px">Si no veus bé aquest email fes clik aquí</a></center>
        <!--[if gte mso 9]>
        <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="<?= base_url('images/boletines')?>/page-bg.jpg" />
        </v:background>
        <![endif]-->
        <table id="page-wrap" width="100%" align="center" cellspacing="0" cellpadding="0" background="<?= base_url('images/boletines')?>/page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" id="email-body" width="726" align="center" cellspacing="0" cellpadding="0" style=" margin-top: 6px">
                            <tbody>
                                <tr>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Left page bg show-thru --></td>
                                    <td id="page-body" eqid="EQMST-E42EE1D2-FD64-4CC3-81A8-CEDDEEA78958" width="666" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/background.jpg">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer" style="height: auto; ">
                                            
                                            
                                            <div eqid="EQMST-E7DD055F-9768-41B1-B11F-7203C5F91402" width="100%" class="desktop-only">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]>
                                                                                            <img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889"src="<?= base_url('images/boletines')?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><a href="http://www.bstim.cat"><!--[if gte mso 9]><img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines')?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><img alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines')?>/image-3.png" style="width: 666px; height: 131px; display: block; " width="666" height="131" border="0"><!--[if gte mso 9]></div><![endif]--></a>
                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-5FCF1FB0-06C7-4DBF-9C38-98D3D32C7BBE" class="mobile-only hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; ">
                                                <table class="mso-fixed-width-wrapping-table hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 320px; " width="320">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; ">
                                                                                    <img alt="" eqid="EQMST-C85E404B-222F-4BD1-B410-7D945C51E731" src="<?= base_url('images/boletines')?>/image-5.jpg" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; width: 320px; height: 144px; " class="hide" width="320" height="144" border="0">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-188CA4C8-0993-41A1-96D4-3F44AF105B12" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><a href="<?= $detail->link_banner ?>"><img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/'.$detail->banner)?>" border="0" style="display: block; width: 666px; height: 394px;"></a><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><a href='<?= $detail->link_banner ?>'><img alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/'.$detail->banner)?>" style="width: 666px; height: 394px; display: block; " width="666" height="394" border="0"></a>
                                                                                    <!--[if gte mso 9]></div><![endif]--></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-F3370785-473B-4F8E-82BC-57FADBF50FD7" class="mobile-only hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; ">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 320px; " width="320">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; ">
                                                                                    <a href='<?= $detail->link_banner ?>'><img alt="" eqid="EQMST-AEB42D1F-6CB6-471E-ADD2-9BF45C7DF26C" src="<?= base_url('images/boletines/'.$detail->banner)?>" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; width: 320px; height: auto; " class="hide" width="320" height="172" border="0"></a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div style="margin:25px 50px">
                                                <?= $detail->contenido ?>
                                            </div>
                                            <!---------- FIN CONTENIDO ------------>
                                            <div eqid="EQMST-66A68E4D-7B41-487F-833C-BCCA15FB7D50" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 260px;text-align:center" width="260">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>"><img src="<?= base_url('images/boletines/logo.png' ) ?>"style="margin-top: 30px;margin-bottom:30px " ></a>
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            

                                            <div width="100%" eqid="EQMST-1FC5CE77-C861-41F2-881E-4E7A975FF07A" class="mobile-only hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; margin-top: 7px;">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                            <td class="layout-block-full-width" style="min-width: 320px; " width="320" valign="top">
                                                                <table class="layout-block-full-width hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="text-align: center; min-width: 230px;" width="130">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/images/boletines/logo.png" style=" width: 80px; margin-top: 10px; margin-left: px; margin-bottom: 10px ">
                                                                                </a>
                                                                                <br/>
                                                                                <a href="https://www.facebook.com/bstimfair/" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/images/boletines/face.png" style="">
                                                                                </a>
                                                                                <a href="https://twitter.com/bstim_fair" style="text-decoration:none">
                                                                                    <img src="http://bstim.cat/blog/images/boletines/twit.png" style="padding-bottom: 3px;">
                                                                                </a>
                                                                                
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 25px; " width="25">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 616px; " width="616" valign="top" align="left">
                                                                <table class="layout-block-column" style="min-width: 616px; " width="616" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: px; padding-right: 10px; min-width: 596px; " eqid="EQMST-6F1F647C-304E-4260-A64E-BFC53CA5C677" width="596" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-2.jpg">
                                                                                <table class="layout-block-box-padding" style="min-width: 596px; " width="596" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 596px; " width="596" align="left">
                                                                                                <div class="heading" eqid="EQMST-3B228E9E-2621-4001-908E-74A5025C2D05" style="font-size: 16px; font-family:'Arial'; text-align: center; ">
                                                                                                <div style=" margin-left: px; margin-top: 20px;padding-bottom: 30px; margin-bottom: 20px">
                                                                                                  <a href="https://www.facebook.com/bstimfair/"><img src="<?= base_url('images/boletines/face.png') ?>" style=""></a>
                                                                                                    <a href="https://twitter.com/bstim_fair"><img src="<?= base_url('images/boletines/twit.png') ?>" style=" padding-top: 8px;"></a>
                                                                                                    </div>
                                                                                                    
                                                                                
                                                                                                                                                                                                   </a>                                                                                                  </a>
                                                                                                        
                                                                                                        <div style="color:rgb(94,94,94);font-family:'Arial','Helvetica Neue';font-size:11px" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.bstim.cat/blog/main/unsubscribe&amp;source=gmail&amp;ust=1482428959994000&amp;usg=AFQjCNETdFS7_JoTq9RFSVY2L0uZ6exhlQ">    
                                                                                                        En compliment de la LOPD 15/99 i LSSI 34/2002, i altres disposicions legals, l'informem que les seves dades de caràcter personal passaran a formar 
                                                                                                        part d'un fitxer automatitzat de caràcter  personal el Responsable del qual és FIRA D'IGUALADA. Autoritza la utilització dels mateixos per a les 
                                                                                                        comunicacions, incloent les realitzades via correu electrònic, que FIRA D'IGUALADA realitzi amb finalitats promocionals o informatives de les activitats 
                                                                                                        que organitza i/o dóna suport amb la seva logística. En cas que contracti algun servei amb FIRA D'IGUALADA, queda informat que les seves dades podran ser 
                                                                                                        comunicades, amb obligació de confidencialitat, a les empreses col·laboradores de FIRA D'IGUALADA, sempre que això sigui necessari a les fins que aquestes 
                                                                                                        realitzin el servei contractat. Queda igualment informat de la possibilitat d'exercitar sobre les dades els drets d'accés, rectificació, cancel·lació i oposició, 
                                                                                                        per la qual cosa haurà de dirigir-se per e-mail  <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 10px; color:blue; padding-top: 8px; ">    
                                                                                                    info@firaigualada.org.<br><br><br></a>
                                                                                                    
                                                                                                    <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px;">
                                                                                                        Avis Legal  
                                                                                                    </a>| 
                                                                                                    <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px; ">    
                                                                                                        Donar-se de baixa <br><br><br></a>       
                                                                                                    
                            
                                                                                                    
                                                                                                    </a>
                                                                                                  
                                                                                                </div>
                                                                                                
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 25px; " width="25">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; " eqid="EQMST-FCDD6FA3-CF8E-41CC-9CFD-75501AE32C20" class="mobile-only hide" background="<?= base_url('images/boletines')?>/background-2.jpg">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/background-2.jpg">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 15px; " width="15">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 290px; " width="290" valign="top" align="left">
                                                                <table class="layout-block-column hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 290px; " width="290" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: 10px; padding-right: 10px; min-width: 270px; " eqid="EQMST-1AE683C0-6D4C-4055-820A-174A10512B82" width="270" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-10.jpg">
                                                                                <table class="layout-block-box-padding hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 270px; " width="270" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 270px; " width="270" align="left">
                                                                                                <div class="heading" style="font-size: 16px; font-family: 'Arial'; text-align: center;  margin-top: 30px; margin-bottom:30px " eqid="EQMST-05B94A82-9C8E-469C-B219-ED3EB3450257">
                                                                                                    
                                                                                                        
                                                                                                        <div style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 10px;  ">    
                                                                                                        En compliment de la LOPD 15/99 i LSSI 34/2002, i altres disposicions legals, l'informem que les seves dades de caràcter personal passaran a formar 
                                                                                                        part d'un fitxer automatitzat de caràcter  personal el Responsable del qual és FIRA D'IGUALADA. Autoritza la utilització dels mateixos per a les 
                                                                                                        comunicacions, incloent les realitzades via correu electrònic, que FIRA D'IGUALADA realitzi amb finalitats promocionals o informatives de les activitats 
                                                                                                        que organitza i/o dóna suport amb la seva logística. En cas que contracti algun servei amb FIRA D'IGUALADA, queda informat que les seves dades podran ser 
                                                                                                        comunicades, amb obligació de confidencialitat, a les empreses col·laboradores de FIRA D'IGUALADA, sempre que això sigui necessari a les fins que aquestes 
                                                                                                        realitzin el servei contractat. Queda igualment informat de la possibilitat d'exercitar sobre les dades els drets d'accés, rectificació, cancel·lació i oposició, 
                                                                                                        per la qual cosa haurà de dirigir-se per e-mail <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family:'Arial', 'Helvetica Neue'; font-size: 10px; color:blue ">    
                                                                                                    info@firaigualada.org.<br><br><br></a>
                                                                                                    
                                                                                                    <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px; ">
                                                                                                        Avis Legal  
                                                                                                    </a>|
                                                                                                    <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px; ">    
                                                                                                        Donar-se de baixa <br><br><br></a>
                                                                                                         
                                                                                                    </a>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 15px; " width="15">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" style="background-image:url(background-3.jpg); background-position: 0% 0%; " class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('images/boletines')?>/background-3.jpg">
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; " eqid="EQMST-6A6B0687-E4D6-4886-B45A-43BA011F8747" class="mobile-only hide" width="100%" background="<?= base_url('images/boletines')?>/background-4.jpg">
                                                <table class="layout-block-wrapping-table hide" style="display: none; max-height: 0; overflow: hidden; line-height:0; font-size:0;  mso-hide: all; min-width: 320px; " width="320" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('images/boletines')?>/background-4.jpg">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <!--End of layout container -->

                                    </td>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                            </tbody>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>