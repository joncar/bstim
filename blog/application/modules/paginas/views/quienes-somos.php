<div class="about-page loaded" id="page-content">
<header data-bg="<?= base_url() ?>images/about_header_bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/about_header_bg.jpg);">
    <?= $this->load->view('includes/template/menu2') ?>
    <div class="header-center-content"> 
        <div class="container text-center"> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
                <i class="icon icon-dial"style=" font-size: 60px; color: #f71259;"></i>
                    <h1 class="text-uppercase">NUESTRO TRABAJO ES HACERLO BIEN</h1> 
                    <h4>La experiencia es nuestra mejor presentación</h4> 
                </div> 
            </div> 
        </div> 
    </div> 
</header> <!-- /.about page header --> <!-- main content --> 
<main> 
    <section class="about-tabs"> <!-- Tab panes --> 
        <div class="container-fluid"> 
            <div class="row about-tabs-control"> 
                <div class="container"> <!-- Nav tabs --> 
                    <ul role="tablist" class="nav nav-tabs"> 
                        <li class="active" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="mission" href="#mission">Equipo</a>
                            <p class="sown" style="margin: 0px;">estimably</p>
                            <span class="kiwi">puritanism voroshilovsk</span>
                        </li> 
                        <li role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="values" href="#values">Valores</a>
                            <p class="bura" style="margin: 0px;">pseudosubtle</p>
                            <span class="kiwi">hypercalcinuria inexorable</span>
                        </li> 
                        <li role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="experience" href="#experience">Experiencia</a>
                            <p class="coma" style="margin: 0px;">exudative</p>
                            <span class="vice">thiobacilli agitable</span>
                        </li> 
                    </ul> <!-- /.Nav tabs --> 
                </div>
            </div> 
            <div class="row"> 
                <div class="tab-content"> 
                    <div id="mission" class="tab-pane fade in active" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/mission_bg.jpg" class="text-uppercase tab-bg" style="background-image: url(<?= base_url() ?>images/mission_bg.jpg);"> 
                                <h1 class="tab-title">Nuestro equipo</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <p style="margin: 0px;"> 
Nuestros asistentes/coordinadores irán directamente a vuestro instituto para conoceros si lo necesitáis, y ofreceros todas nuestras opciones de viaje.</p>
                                <p class="coma" style="margin: 0px;">preobligating</p>
                                <span class="poll">noninfantry nonponderous</span>
                                <span class="vice">acceleratory</span> 
                                <span> Ese asistente será vuestro mejor amigo hasta la vuelta de la aventura, pues estará con vosotros desde el primer momento, asesorando, gestionando, y resolviendo las dudas que le puedan surgir al grupo, siempre estará identificado con un carnet profesional de ASISTENTE  de Kanvoy. 
                                </span>
                                <span class="classy">dynamotor</span> 
                                <ul class="list-unstyled"> 
                                    <li><a href="#">Nuestro viaje</a>
                                        <p class="vice" style="margin: 0px;">unprefaced</p>
                                        <span class="classy">interlinearly crystallographical</span>
                                    </li> 
                                    <li>
                                        <a href="#">Nuestro blog</a>
                                        <p class="fitz" style="margin: 0px;">neronizing</p>
                                        <span class="classy">agitable rationalizing</span>
                                    </li> <li><a href="#">Galeria de fotos</a>
                                        <p class="frow" style="margin: 0px;">stipulation</p>
                                        <span class="kiwi">overneutralized pseudopoetical</span>
                                    </li> 
                                </ul> 
                            </div> 
                        </div> 
                    </div> 
                    <div id="values" class="tab-pane fade" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/values_bg.jpg" class="text-uppercase tab-bg" style="background-image: url(<?= base_url() ?>images/values_bg.jpg);"> 
                                <h1 class="tab-title">Nuestros valores</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <p style="margin: 0px;"> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in cul ed ut perspiciatis unde omnis iste natus. 
                                </p>
                                <p class="lehi" style="margin: 0px;">megalomania</p>
                                <span class="divc">bathymetry comptometer</span><span class="frow">unfaceted</span> 
                                <span> • Somos como vosotros</br>• Pensamos como vosotros</br>• Vivimos como vosotros</br>• Ya hemos estado allí y seguimos estando</br>• Sabemos lo que os gusta y lo que necesitáis
 
                                </span>
                                <span class="lehi">untapering</span> 
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">Nuestro valores en el Blog</a>
                                        <p class="classy" style="margin: 0px;">medullization</p>
                                        <span class="sown">noninvincibility voroshilovsk</span>
                                    </li> 
                                </ul> 
                            </div> 
                        </div> 
                    </div> 
                    <div id="experience" class="tab-pane fade" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/experience_bg.jpg" class="text-uppercase tab-bg" style="background-image: url(<?= base_url() ?>images/experience_bg.jpg);"> 
                                <h1 class="tab-title">Nuestra experiencia</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <p style="margin: 0px;"> NUESTRA EXPERIENCIA, MALLORCA ISLAND EXPERIENCE (MIEX) la compone un amplio grupo de profesionales que harán de vuestro viaje la mayor experiencia de vuestras vidas. 
                                </p>
                                <p class="bura" style="margin: 0px;">semipractical</p>
                                <span class="kiwi">hyperelegance unfugitive</span>
                                <span class="mydiv">mesomorphic</span> 
                                <span> Todo nuestro equipo, conoce el destino que os proponemos a la perfección, lo que hará que podamos resolver todas vuestras dudas o inquietudes acerca de cualquiera tema. </span>
                                <span class="coma">ferrovanadium</span> 
                                <ul class="list-unstyled"> 
                                    <li>
                                        <a href="#">Nuestro blog</a>
                                        <p class="lehi" style="margin: 0px;">zaporozhye</p>
                                        <span class="poll">neogean apsarases</span>
                                    </li> 
                                    <li>
                                        <a href="#">Galeria de fotos</a>
                                        <p class="frow" style="margin: 0px;">nonunanimous</p>
                                        <span class="kiwi">hematocyst vectorially</span>
                                    </li> 
                                </ul> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> <!-- /.Tab panes --> <!-- trip categories --> 
        <div class="row trip-categories"> 
            <div class="container"> <!-- section-intro --> 
                <div class="row text-center section-intro"> 
                    <div class="col-md-offset-2 col-md-8">
                            <h1 class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">
                                TE LO QUEREMOS PONER FÁCIL
                            </h1> 
                            <p style="margin: 0px;">Tanto es así, que Mallorca Island Experience, nos ocuparemos de todo</p>
                            <p class="lefty" style="margin: 0px;">comptometer</p>
                            <span class="divc">zygotically hypercalcinuria</span>
                            <span class="coma">aetiology</span> 
                    </div> 
                </div> <!-- /.section-intro --> 
                <div class="row"> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-telescope"></i> 
                            <h5 class="text-uppercase category-title"> Información de reserva de vuestro viaje</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="lehi" style="margin: 0px;">lovableness</p>
                            <span class="sown">diakonikon nonponderous</span>
                            <span class="bura">unsegregated</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="coma" style="margin: 0px;">personifying</p>
                            <span class="frow">nonexertion rationalizing</span> 
                        </div> 
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-wine"></i> 
                            <h5 class="text-uppercase category-title">Gestión de Rooming-List / Check-in & Check-out</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="libeled" style="margin: 0px;">saxicolous</p>
                            <span class="mydiv">pithecanthropid ornamental</span>
                            <span class="lefty">unhuntable</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="lehi" style="margin: 0px;">interantagonism</p>
                            <span class="lehi">dissonancy truculently</span> 
                        </div> 
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-bike"></i> 
                            <h5 class="text-uppercase category-title">Traslados y embarques en cada vuelo de ida/vuelta</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Sed non mauris. 
                            </p>
                            <p class="frow" style="margin: 0px;">variolitic</p>
                            <span class="mydiv">preascetic bipinnaria</span>
                            <span class="lehi">lovableness</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="kiwi" style="margin: 0px;">annexationism</p>
                            <span class="fitz">amusable overdiscouraging</span> 
                        </div>
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">Alojamientos y actividades</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="poll" style="margin: 0px;">intermetatarsal</p>
                            <span class="coma">nontheocratical americium</span>
                            <span class="bura">hypognathism</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="vice" style="margin: 0px;">retabulated</p>
                            <span class="lehi">mischaracterize preeliminate</span> 
                        </div> 
                    </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">Asistencia en origen y destino</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="poll" style="margin: 0px;">intermetatarsal</p>
                            <span class="coma">nontheocratical americium</span>
                            <span class="bura">hypognathism</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="vice" style="margin: 0px;">retabulated</p>
                            <span class="lehi">mischaracterize preeliminate</span> 
                        </div> 
                        </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">Organización de fiestas y excursiones</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="poll" style="margin: 0px;">intermetatarsal</p>
                            <span class="coma">nontheocratical americium</span>
                            <span class="bura">hypognathism</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="vice" style="margin: 0px;">retabulated</p>
                            <span class="lehi">mischaracterize preeliminate</span> 
                        </div> 
                        </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">Gestión y producción de merchandising y sorteos</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="poll" style="margin: 0px;">intermetatarsal</p>
                            <span class="coma">nontheocratical americium</span>
                            <span class="bura">hypognathism</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="vice" style="margin: 0px;">retabulated</p>
                            <span class="lehi">mischaracterize preeliminate</span> 
                        </div> 
                        </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">Y un largo etc, que hará de vuestro viaje inolvidable</h5> 
                            <p style="margin: 0px;"> Vel velit auctor aliquet. Aenean sollicdin, lorem quis bibendum auctor. Duis sed odio sit amet nibh vulputatecursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris. 
                            </p>
                            <p class="poll" style="margin: 0px;">intermetatarsal</p>
                            <span class="coma">nontheocratical americium</span>
                            <span class="bura">hypognathism</span> 
                            <a class="btn text-uppercase" href="#">Browse trips</a>
                            <p class="vice" style="margin: 0px;">retabulated</p>
                            <span class="lehi">mischaracterize preeliminate</span> 
                        </div> 
                        
                 
                </div> 
            </div> 
        </div> <!-- /.trip categories --> 
    </section> 
    <section class="about"> 
        <div class="about-video-bg"> 
            <div id="player"></div> 
        </div> 
        <div class="container"> 
            <div class="row text-center"> 
                <div class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6"> 
                    <p class="about-foundation" style="margin: 0px;"> 
                        <span>Since </span>
                        <span class="divc">anticlericalism</span>1987 
                    </p>
                    <p class="lefty" style="margin: 0px;">crystallographical</p>
                    <span class="fitz">dynamotor unsegregated</span>
                    <span class="frow">semibouffant</span> 
                </div> 
            </div> 
            <div class="row text-center"> 
                <div data-wow-duration="1s" data-wow-delay="0.2s" class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6 wow animated fadeIn" style="animation-duration: 1s; animation-delay: 0.2s; animation-name: none;"> 
                    <div class="about-description"> 
                        <i class="icon icon-compass"></i> 
                        <h1 class="text-uppercase">¿que es miex?</h1> 
                        <p style="margin: 0px;"> MALLORCA ISLAND EXPERIENCE una experiencia para poder ofrecer una alternativa diferente.
 </p>
                        <p class="sown" style="margin: 0px;">megalomania</p>
                        <span class="libeled">deoxygenate superinfirmity</span>
                        <span class="classy">americium</span> 
                        <span> Destinado a estudiantes de Bachillerato para su fin de curso, independiente al instituto y después de la selectividad, No te lo vayas  perder!. </span>
                        <span class="libeled">subeditorial</span> 
                        <button id="about-play" data-wow-delay="0.1s" class="btn play-btn text-uppercase wow animated slideInRight hovered" style="animation-delay: 0.1s; animation-name: none;"> 
                            Mira nuestro video 
                        </button> 
                        <button id="about-stop" class="btn play-btn text-uppercase hovered"> Para nuestro video </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section> 
    <section class="our-team"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8"> 
                    <i data-wow-delay="0.2s" class="icon icon-puzzle wow animated fadeInUp" style="animation-delay: 0.2s; animation-name: none;"></i>
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">NUESTRO EQUIPO</h1>
                    <p style="margin: 0px;">Estas personas hacen que el viaje sea increible</p>
                    <p class="vice" style="margin: 0px;">pinguidity</p>
                    <span class="coma">agitable detribalise</span>
                    <span class="poll">hematocyst</span> 
                </div> 
            </div> <!-- /.section-intro --> <!-- team members --> 
            <div class="row"> 
                <div class="col-md-3 col-sm-6 text-center"> 
                    <div class="team-member"> 
                        <img alt="team member photo" class="img-responsive member-photo" src="<?= base_url() ?>images/man1.jpg"> 
                        <h5 class="text-uppercase">Jordi Magaña</h5> 
                        <span class="text-capitalize">Animador</span>
                        <span class="bura">unmilitarised</span> 
                        <ul class="list-inline member-socials">
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-facebook-square"></i></a>
                                <p class="coma" style="margin: 0px;">underscoring</p>
                                <span class="lehi">overspicing vectorially</span> 
                            </li> 
                            <li> 
                                <a href="#"><i aria-hidden="true" class="fa fa-twitter-square"></i></a>
                                <p class="kiwi" style="margin: 0px;">nonponderous</p>
                                <span class="sown">undermelody nonannexable</span> 
                            </li> 
                            <li>
                                <a href="#">
                                    <i aria-hidden="true" class="fa fa-instagram"></i>
                                </a>
                                <p class="coma" style="margin: 0px;">anticlericalism</p>
                                <span class="frow">unhuntable nonconjunction</span> 
                            </li> 
                        </ul> 
                    </div> 
                </div> 
                <div class="col-md-3 col-sm-6 text-center"> 
                    <div class="team-member"> 
                        <img alt="team member photo" class="img-responsive member-photo" src="<?= base_url() ?>images/girl1.jpg"> 
                        <h5 class="text-uppercase">Melisa Donahill</h5> 
                        <span class="text-capitalize">Gerente</span>
                        <span class="kiwi">overenvious</span> 
                        <ul class="list-inline member-socials"> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-facebook-square"></i></a>
                                <p class="lefty" style="margin: 0px;">cyclopentadiene</p>
                                <span class="lehi">prepenetration anticlinorium</span> 
                            </li> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-twitter-square"></i></a>
                                <p class="fitz" style="margin: 0px;">revalidation</p>
                                <span class="poll">bathymetry inimical</span> 
                            </li> 
                            <li> <a href="#">
                                    <i aria-hidden="true" class="fa fa-instagram"></i>
                                </a>
                                <p class="fitz" style="margin: 0px;">patripassianist</p>
                                <span class="bura">hexadecane aristodemocracy</span> 
                            </li> 
                        </ul> 
                    </div> 
                </div>
                <div class="col-md-3 col-sm-6 text-center"> 
                    <div class="team-member"> 
                        <img alt="team member photo" class="img-responsive member-photo" src="<?= base_url() ?>images/man2.jpg"> 
                        <h5 class="text-uppercase">Andrew Kramer</h5> 
                        <span class="text-capitalize">Animador</span>
                        <span class="sown">unengineered</span> 
                        <ul class="list-inline member-socials"> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-facebook-square"></i></a>
                                <p class="kiwi" style="margin: 0px;">phonogramic</p>
                                <span class="classy">unengineered inexorable</span> 
                            </li> 
                            <li> 
                                <a href="#"><i aria-hidden="true" class="fa fa-twitter-square"></i></a>
                                <p class="lefty" style="margin: 0px;">superrefined</p>
                                <span class="lefty">overenvious exorability</span> 
                            </li> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
                                <p class="libeled" style="margin: 0px;">noninfantry</p>
                                <span class="lefty">hyperelegance paulownia</span> 
                            </li> 
                        </ul> 
                    </div> 
                </div> 
                <div class="col-md-3 col-sm-6 text-center"> 
                    <div class="team-member"> 
                        <img alt="team member photo" class="img-responsive member-photo" src="<?= base_url() ?>images/girl2.jpg"> 
                        <h5 class="text-uppercase">alice serres</h5> 
                        <span class="text-capitalize">Monitor</span>
                        <span class="kiwi">regretable</span> 
                        <ul class="list-inline member-socials"> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-facebook-square"></i></a>
                                <p class="poll" style="margin: 0px;">detoxicated</p>
                                <span class="frow">nontheocratical graphitizing</span> 
                            </li> 
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-twitter-square"></i></a>
                                <p class="sown" style="margin: 0px;">incardinating</p>
                                <span class="libeled">supraorbital natatoriums</span> 
                            </li> 
                            <li> 
                                <a href="#"><i aria-hidden="true" class="fa fa-instagram"></i></a>
                                <p class="divc" style="margin: 0px;">thiobacilli</p>
                                <span class="coma">nonglobular monogyny</span> 
                            </li> 
                        </ul> 
                    </div> 
                </div> 
            </div> <!-- /.team members --> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8"> 
                    <i data-wow-delay="0.2s" class="icon icon-speedometer wow animated fadeInUp" style="animation-delay: 0.2s; animation-name: none;"></i> 
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">
                        Satisfacción garantizada
                    </h1> 
                    <p style="margin: 0px;">Hará de vuestro viaje una experiencia inolvidable</p>
                    <p class="lefty" style="margin: 0px;">conclusiveness</p><span class="lehi">familiarised controversial</span>
                    <span class="lefty">pseudosubtle</span> 
                </div> 
            </div> <!-- /.section-intro --> <!-- tips -->
            <div class="row"> 
                <div class="col-md-4 text-center">
                    <div class="tip"> <i class="icon icon-wallet"></i> 
                        <h5 class="text-uppercase">VIAJE SEGURO</h5> 
                        <p style="margin: 0px;"> Consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
                        <p class="divc" style="margin: 0px;">gloriousness</p>
                        <span class="classy">euphemia ununitable</span>
                        <span class="bura">noninfantry</span> 
                    </div> 
                </div> 
                <div class="col-md-4 text-center"> 
                    <div class="tip"> 
                        <i class="icon icon-linegraph"></i> 
                        <h5 class="text-uppercase">Soporte informativo</h5> 
                        <p style="margin: 0px;"> Consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
                        <p class="coma" style="margin: 0px;">nonannexable</p>
                        <span class="frow">philosophicalness angledozer</span>
                        <span class="libeled">unobsolete</span> 
                    </div> 
                </div> 
                <div class="col-md-4 text-center"> 
                    <div class="tip"> 
                        <i class="icon icon-happy"></i> 
                        <h5 class="text-uppercase">Buen ambiente</h5> 
                        <p style="margin: 0px;"> Consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. </p>
                        <p class="frow" style="margin: 0px;">annexationism</p>
                        <span class="sown">bathymetry underscoring</span>
                        <span class="classy">cyclopentadiene</span> 
                    </div> 
                </div> 
            </div> <!-- /.tips --> 
        </div>
    </section>     
    <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>Go Up</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow -->
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>