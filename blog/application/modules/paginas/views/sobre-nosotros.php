<div class="about-page loaded" id="page-content">
<header data-bg="<?= base_url() ?>images/about_header_bg2.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/about_header_bg2.jpg);">
    <?= $this->load->view('includes/template/menu2') ?>
    <div class="header-center-content"> 
        <div class="container text-center"> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
                 <i class="icon icon-dial"style=" font-size: 60px; color: #f71259;"></i>
                    <h1 class="text-uppercase">NOSOTROS SOMOS la fiesta </h1> 
                    <h4>Empieza a conocer todo lo que incluimos</h4> 
                </div> 
            </div> 
        </div> 
    </div> 
</header> <!-- /.about page header --> <!-- main content --> 
<main> 
    <section id="trip-experience"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro bordered">
                <div class="col-md-12"> 
               
                    </p>
                </div> 
            </div> <!-- /.section-intro --> <!-- small intro --> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8">
                    <div class="small-intro">
                        <p style="margin: 0px;">El viaje fin de curso a Mallorca para los estudiantes de Bachiller es un producto registrado Kanvoy bajo la denominación Mallorcaislandexperience.com
Kanvoy desarrolla su actividad en el sector turístico de forma especializada en los jóvenes y estudiantes y sus experiencias, viajes de fin de curso y demás.
 Nuestro amplio abanico de actividades dentro del sector nos permite no sólo estar diversificados sino alcanzar importantes sinergias y la posibilidad de ofrecer a nuestros clientes una gestión global de sus necesidades.
 </p>
                    
                                <span class="poll">noninfantry nonponderous</span>
                                <span class="vice">acceleratory</span> 
                                <span> Ahora que nos conocen un poco mejor, queremos comunicarles que estar más y mejor informados sobre las actividades que realizarán sus hijos, nos importa.
A continuación os detallamos los servicios incluidos y que sus hijos disfrutarán durante el viaje: 
                                </span></div> 
                </div> 
            </div> <!-- /.small intro --> <!-- trip background -->                 
        </div> 
    </section> 
    <section class="adventure-select"> 
        <div class="container"> 
            <div class="row"> <!-- adventure list --> 
                <div class="text-uppercase adventure-list experience"> 
                    <div data-wow-duration="1s" data-wow-delay="0.1s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-actividades-incluidas') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/actividades.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-telescope"></i> 
                                <h5>ACTIVIDADES INCLUIDAS</h5> 
                            </div> 
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.2s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-actividades-extras') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/actividadesextras.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> <i class="icon icon-wine"></i> 
                                <h5>activiades extras</h5> 
                            </div> 
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.3s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-alojamientos') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/alojamientos.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-bike"></i> 
                                <h5>ALOJAMIENTOS</h5> 
                            </div> 
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.4s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-discotecas') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/discotecas.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-streetsign"></i> <h5>DISCOTECAS</h5> 
                            </div>
                     	</a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.4s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-seguros') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/seguros.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-streetsign"></i> <h5>SEGUROS</h5> 
                            </div>
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.4s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-material-viaja-gratis') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/materialviajagratis.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-streetsign"></i> <h5>MATERIAL VIAJA GRATIS</h5> 
                            </div>
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.4s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-autorizaciones-paternas') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/paternas.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-streetsign"></i> <h5>AUTORIZACIONES paternas</h5> 
                            </div>
                        </a>
                    </div> 
                    <div data-wow-duration="1s" data-wow-delay="0.4s" class="col-md-6 col-sm-6 animated fadeInUp"> 
                        <a href="<?= base_url('p/actividades#trip-preguntas-frecuentes') ?>"> 
                            <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/preguntas.jpg"> 
                            <div class="overlay-lnk text-uppercase text-center"> 
                                <i class="icon icon-streetsign"></i> <h5>PREGUNTAS FRECUENTES</h5> 
                            </div>
                        </a>                                                                                              
                    </div> 
                </div> <!-- /.adventure list --> 
            </div> 
        </div> 
    </section>
    <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg);"> 
            <div class="container">
                <div id="gallery" class="row" style="top:0px;"> 
                    <div class="col-md-4 text-uppercase"> 
                        <div class="gallery-title"> 
                            <i class="icon icon-camera"></i> 
                            <h1>Foto Galeria</h1> 
                        </div> 
                    </div> 
                    <ul class="list-inline gallery-photos"> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv2.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv2.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv3.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv3.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/contact.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/contact.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv5.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv5.jpg">
                            </a>
                        </li> 
                        <li>
                            <a href="<?= base_url() ?>images/zadv6.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv6.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv7.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv7.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv8.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv8.jpg"
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv9.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv9.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv10.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv10.jpg">
                            </a>
                        </li> 
                        <li> 
                            <a href="<?= base_url() ?>images/zadv11.jpg">
                                <img class="img-responsive" alt="photo-gallery" src="<?= base_url() ?>images/zadv11.jpg">
                            </a>
                        </li> 
                    </ul> 
                </div> 
            </div> 
        </section> 
    <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>Go Up</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow -->
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>