<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('blog_categorias',array('idioma'=>$_SESSION['lang']));
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }
        
        public function index(){
            $blog = new Bdsource();
            $blog->limit = array('6','0');
            $blog->order_by = array('fecha','DESC');
            if(!empty($_GET['direccion'])){
                $blog->like('titulo',$_GET['direccion']);
            }
            if(!empty($_GET['blog_categorias_id'])){
                $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
            }
            $blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $blog->limit = array(($_GET['page']-1),6);
            }
            $blog->init('blog');
            $totalpages = round($this->db->get_where('blog')->num_rows/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            foreach($this->blog->result() as $n=>$b){
                $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
            }
            
            $this->loadView(
                    array(
                        'view'=>'frontend/main',
                        'detail'=>$this->blog,
                        'total_pages'=>$totalpages,
                        'title'=>'Blog',
                        'categorias'=>$this->get_categorias()
                    ));
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $blog = new Bdsource();
                $blog->where('id',$id);
                $blog->init('blog',TRUE);
                $comentarios = new Bdsource();
                $comentarios->where('blog_id',$this->blog->id);
                $comentarios->init('comentarios');
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->blog,
                        'title'=>$this->blog->titulo,
                        'comentarios'=>$this->comentarios,
                        'categorias'=>$this->get_categorias()
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                                ->set_rules('texto','Comentario','required')
                                                ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));
            }else{
                echo $this->error('Comentario no enviado con éxito');
            }
        }
    }
?>
