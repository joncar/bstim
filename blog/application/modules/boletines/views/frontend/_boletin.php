<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="Generator" content="Made with Mail Designer from equinux">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css" id="Mail Designer General Style Sheet">
            a { word-break: break-word; }
            a img { border:none; }
            img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
            body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
            .ExternalClass { width: 100%; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            #page-wrap { margin: 0; padding: 0; width: 100% !important; line-height: 100% !important; }
            #outlook a { padding: 0; }
            .preheader { display:none !important; }
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }
        </style>
        <style type="text/css" id="Mail Designer Mobile Style Sheet">
            @media only screen and (max-width: 580px) {
                table[class=email-body-wrap] {
                    width: 320px !important;
                }
                td[class=page-bg-show-thru] {
                    display: none !important;
                }
                table[class=layout-block-wrapping-table] {
                    width: 320px !important;
                }
                table[class=mso-fixed-width-wrapping-table] {
                    width: 320px !important;
                }
                *[class=layout-block-full-width] {
                    width: 320px !important;
                }
                table[class=layout-block-column], table[class=layout-block-padded-column] {
                    width: 100% !important;
                }
                table[class=layout-block-box-padding] {
                    width: 100% !important;
                    padding: 5px !important;
                }
                table[class=layout-block-horizontal-spacer] {
                    display: none !important;
                }
                tr[class=layout-block-vertical-spacer] {
                    display: block !important;
                    height: 8px !important;
                }
                td[class=container-padding] {
                    display: none !important;
                }

                table {
                    min-width: initial !important;
                }
                td {
                    min-width: initial !important;
                }

                *[class~=desktop-only] { display: none !important; }
                *[class~=mobile-only] { display: block !important; }

                .hide {
                    max-height: none !important;
                    display: block !important;
                    overflow: visible !important;
                }

                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-7FC5D3D0-8C6D-4FEE-9CF1-3545D1D19E3E"] { height:17px !important; } /* vertical spacer */

                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-A96504FD-F8B8-4282-AB75-962C6BB0348E"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-AFADBC7C-37B4-468F-811E-2BCE29DE02F9"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-A5601FD8-37B3-4243-80E5-D4FB295AC3A6"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-6F8381C7-CA21-4D77-87E5-BE2345484C37"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-111D93B9-D011-41A8-A837-1A85B7783DFE"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-497AB408-00D3-4687-9D34-5BDE49089F01"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-1A20A89F-3924-426B-96B8-34027D605D40"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-51B33CBE-AB41-4DE1-A2F0-42AFD3EEF489"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-497AB408-00D3-4687-9D34-5BDE49089F01"] { height:31px !important; } /* vertical spacer */

                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-59993048-3D38-46DA-A8C7-C8335644A9E3"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-D902B111-49C8-4989-91C3-19446738F585"] [class~="layout-block-content-cell"] { width: 290px !important; }
                td[eqID="EQMST-1A20A89F-3924-426B-96B8-34027D605D40"] { height:5px !important; } /* vertical spacer */

                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-left"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-padding-right"] { width: 15px !important; }
                div[eqID="EQMST-64931755-30F9-4850-BB59-A8325537585F"] [class~="layout-block-content-cell"] { width: 290px !important; }

                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-padding-left"] { width: 12px !important; }
                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-padding-right"] { width: 12px !important; }
                div[eqID="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5"] [class~="layout-block-content-cell"] { width: 296px !important; }
            }
        </style>
        <!--[if gte mso 9]>
        <style type="text/css" id="Mail Designer Outlook Style Sheet">
                table.layout-block-horizontal-spacer {
                    display: none !important;
                }
                table {
                    border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    mso-table-bspace:0pt;
                    mso-table-tspace:0pt;
                    mso-padding-alt:0;
                    mso-table-top:0;
                    mso-table-wrap:around;
                }
                td {
                    border-collapse:collapse;
                    mso-cellspacing:0;
                }
        </style>
        <![endif]-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:regular,100" rel="stylesheet" type="text/css" class="EQWebFont">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body style="margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; " background="<?= base_url('images/boletines/page-bg.jpg') ?>" eqid="EQMST-5B629705-2068-4643-A1B4-1BA752D88F04"><!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
    <v:fill type="tile" src="<?= base_url('images/boletines/') ?>/page-bg.jpg" />
    </v:background>
    <![endif]--> 
        
        <table width="100%" cellspacing="0" cellpadding="0" id="page-wrap" align="center" background="page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" width="726" cellspacing="0" cellpadding="0" id="email-body" align="center">
                            <tbody>
                                <tr>
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Left page bg show-thru --></td>
                                    <td width="666" id="page-body" eqid="EQMST-E42EE1D2-FD64-4CC3-81A8-CEDDEEA78958" bgcolor="#ffffff" background="background.jpg">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer" style="height: 3197px; ">

                                            <div eqid="EQMST-E7DD055F-9768-41B1-B11F-7203C5F91402" width="100%" class="desktop-only">
                                                <table width="666" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table" style="min-width: 666px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" class="layout-block-full-width" width="666" style="min-width: 666px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="666" style="min-width: 666px; ">
                                                                    <tbody><tr>
                                                                            <td width="666" style="min-width: 666px; ">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889"src="<?= base_url('images/boletines/') ?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><a href="http://www.bstim.cat"><!--[if gte mso 9]><img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines/') ?>/image-3.png" border="0" style="display: block; width: 666px; height: 131px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><img width="666" height="131" alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines/') ?>/image-3.png" border="0" style="width: 666px; height: 131px; display: block; "><!--[if gte mso 9]></div><![endif]--></a>
                                                                                    <!--[if gte mso 9]></div><![endif]--></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-5FCF1FB0-06C7-4DBF-9C38-98D3D32C7BBE" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; ">
                                                <table width="320" cellspacing="0" cellpadding="0" class="mso-fixed-width-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 320px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" class="layout-block-full-width" width="320" style="min-width: 320px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width hide" width="320" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 320px; ">
                                                                    <tbody><tr>
                                                                            <td width="320" style="min-width: 320px; ">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; ">
                                                                                    <img width="320" height="144" alt="" eqid="EQMST-C85E404B-222F-4BD1-B410-7D945C51E731" src="<?= base_url('images/boletines/') ?>/image-5.jpg" border="0" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; width: 320px; height: 144px; " class="hide">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            <div eqid="EQMST-188CA4C8-0993-41A1-96D4-3F44AF105B12" width="100%" class="desktop-only">
                                                <table width="666" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 666px; ">
                                                    <tbody><tr>
                                                            <td valign="top" class="layout-block-full-width" width="666" style="min-width: 666px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width" width="666" style="min-width: 666px; ">
                                                                    <tbody><tr>
                                                                            <td width="666" style="min-width: 666px; ">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/') ?>/image-1.png" border="0" style="display: block; width: 666px; height: 394px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]--><img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/') ?>/image-1.png" border="0" style="width: 666px; height: 394px; display: block; ">
                                                                                    <!--[if gte mso 9]></div><![endif]--></div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            <?php foreach($detail->cuerpo->result() as $c): ?>
                                            
                                            
                                            <div width="100%" eqid="EQMST-F3370785-473B-4F8E-82BC-57FADBF50FD7" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; ">
                                                <table width="320" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 320px; ">
                                                    <tbody><tr>
                                                            <td valign="top" class="layout-block-full-width" width="320" style="min-width: 320px; ">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-full-width hide" width="320" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 320px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="320" style="min-width: 320px; ">
                                                                                <div class="layout-block-image hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; ">
                                                                                    <img width="320" height="172" alt="" eqid="EQMST-AEB42D1F-6CB6-471E-ADD2-9BF45C7DF26C" src="<?= base_url('images/boletines/'.$c->foto) ?>" border="0" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; width: 320px; height: 172px; " class="hide">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D" width="100%">
                                                <table width="666" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 666px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="31" class="layout-block-padding-left" style="font-size: 1px; min-width: 31px; ">&nbsp;</td>
                                                            <td height="36" background="box-bg-10.jpg" class="layout-block-content-cell" bgcolor="#ffffff" width="604" eqid="EQMST-7FC5D3D0-8C6D-4FEE-9CF1-3545D1D19E3E" style="min-width: 604px; "><!--[if gte mso 9]>
            <v:rect style="width:604px;height:36px;" strokecolor="none">
            <v:fill type="tile" color="#ffffff" src="<?= base_url('images/boletines/') ?>/box-bg-10.jpg"></v:fill>
            </v:rect><![endif]-->
                                                                <div class="spacer">
                                                                </div><!--[if gte mso 9]>
                </v:rect>
                <![endif]--></td>
                                                            <td width="31" class="layout-block-padding-right" style="font-size: 1px; min-width: 31px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-9684568B-42D9-480B-88F9-A8A5628A3E95" width="100%" class="desktop-only">
                                                <table width="666" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 666px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="31" class="layout-block-padding-left" style="min-width: 31px; ">&nbsp;</td>
                                                            <td valign="top" align="left" class="layout-block-content-cell" width="604" style="min-width: 604px; ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column" width="604" style="min-width: 604px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="584" valign="top" align="left" background="box-bg.jpg" style="padding-left: 10px; padding-right: 10px; min-width: 584px; " bgcolor="#ffffff" eqid="EQMST-832D1D65-C6DF-4E58-8BEC-3E50F3BC55F4">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="584" style="min-width: 584px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" width="584" style="min-width: 584px; ">
                                                                                                <div class="heading" eqid="EQMST-5D3E8BB4-426D-4955-9639-2A3F9445ED32" style="font-size: 16px; font-family: 'Lucida Grande'; ">
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Helvetica Neue', Arial; font-size: 14px; font-style: normal; font-weight: bold; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                        <?= $c->titulo_rojo ?>
                                                                                                    </div>
                                                                                                    <div style="color: rgb(0, 0, 0); font-family: 'Open Sans', 'Helvetica Neue', Arial; font-size: 32px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                        <?= $c->titulo ?>
                                                                                                    </div>                                                                                                            
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="31" class="layout-block-padding-right" style="min-width: 31px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div width="100%" eqid="EQMST-118C5171-76CC-4FBD-AF03-2291162432BB" class="mobile-only hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; ">
                                                <table width="320" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table hide" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 320px; ">
                                                    <tbody><tr>
                                                            <td width="14" class="layout-block-padding-left" style="min-width: 14px; ">&nbsp;</td>
                                                            <td valign="top" align="left" class="layout-block-content-cell" width="289" style="min-width: 289px; ">
                                                                <table cellspacing="0" cellpadding="0" align="left" class="layout-block-column hide" width="289" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 289px; ">
                                                                    <tbody><tr>
                                                                            <td width="269" valign="top" align="left" background="box-bg-10.jpg" style="padding-left: 10px; padding-right: 10px; min-width: 269px; " bgcolor="#ffffff" eqid="EQMST-85E094AF-ACDE-4935-992D-18DFFF3B162E">
                                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding hide" width="269" style="display: none; max-height: 0px; overflow: hidden;  mso-hide: all; min-width: 269px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" class="layout-block-column" width="269" style="min-width: 269px; ">
                                                                                                <div class="heading" style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Arial; color: rgb(177, 0, 26); font-style: normal; font-weight: bold; line-height: 1.5; text-align: left; text-decoration: none; " eqid="EQMST-B11CCBB3-7809-4188-A2B9-F8E54120832B">
                                                                                                    <div>
                                                                                                        <?= $c->titulo_rojo ?>
                                                                                                    </div>
                                                                                                    <div>
                                                                                                        <span style="color: rgb(0, 0, 0); font-size: 32px; font-weight: normal; ">
                                                                                                            <?= $c->titulo ?>
                                                                                                        </span>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="17" class="layout-block-padding-right" style="min-width: 17px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-15EFB455-8901-4B16-BCE0-32964008FAE1" width="100%">
                                                <table width="666" cellspacing="0" cellpadding="0" class="layout-block-wrapping-table" style="min-width: 666px; ">
                                                    <tbody>
                                                        <tr>
                                                            <td width="31" class="layout-block-padding-left" style="min-width: 31px; ">&nbsp;</td>
                                                            <td width="584" valign="top" align="left" background="box-bg-10.jpg" class="layout-block-content-cell" style="padding-left: 10px; padding-right: 10px; min-width: 584px; " bgcolor="#ffffff" eqid="EQMST-54C2CF3D-2105-4A2A-9A23-5BACFB933DE8">
                                                                <table cellspacing="0" cellpadding="0" class="layout-block-box-padding" width="584" style="min-width: 584px; ">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="584" align="left" class="layout-block-column-padded-column" style="min-width: 584px; "><!--[if mso]><table width="584" border="0" cellpadding="0" cellspacing="0" align="center"><tr><![endif]-->
                                                                                    <!--[if mso]><td width="134"><![endif]-->
                                                                                <table width="134" cellspacing="0" cellpadding="0" align="left" class="layout-block-padded-column" style="min-width: 134px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td align="left" valign="top" class="layout-block-padded-column" width="134" style="min-width: 134px; ">
                                                                                                <div class="text" eqid="EQMST-FA5F9659-6359-4ABD-8366-4CE582DEEA7D" style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Arial; color: rgb(66, 66, 66); font-style: normal; font-weight: bold; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                    <?= $c->descripcion ?>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                    <!--[if mso]></td><td width="29"><![endif]--><table cellspacing="0" cellpadding="0" align="left" class="layout-block-horizontal-spacer" width="29" style="min-width: 29px; ">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td width="29" class="layout-block-horizontal-spacer" style="min-width: 29px; ">&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody></table>
                                                                                    <!--[if mso]></td><td width="421"><![endif]-->
                                                                                <table width="421" cellspacing="0" cellpadding="0" align="left" class="layout-block-padded-column" style="min-width: 421px; ">
                                                                                    <tbody><!--[if !mso]><!--><!--[if !mso]><!--><!--[if !mso]><!-->
                                                                                        <tr class="layout-block-vertical-spacer" style="display:none; height:0px; ">
                                                                                            <td width="421" style="min-width: 421px; ">
                                                                                                <div class="spacer"></div>
                                                                                            </td>
                                                                                        </tr><!--<![endif]--><!--<![endif]--><!--<![endif]-->
                                                                                        <tr>
                                                                                            <td align="left" valign="top" width="421" style="min-width: 421px; ">
                                                                                                <div class="text" eqid="EQMST-C5A235AB-8CB5-4EA1-A22B-9FA541CBC33B" style="font-size: 14px; font-family: 'Open Sans', 'Helvetica Neue', Arial; color: rgb(94, 94, 94); font-style: normal; font-weight: normal; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                    <?= $c->texto ?>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]></tr></table><![endif]--></td>
                                                                        </tr>
                                                                    </tbody></table>
                                                            </td>
                                                            <td width="31" class="layout-block-padding-right" style="min-width: 31px; ">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            
                                            <div width="100%" class="desktop-only" eqid="EQMST-C6B36497-890C-4973-9EB6-CEF2BF21C059">
                                                    <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="min-width: 666px; " width="666"><!--[if mso]><table width="666" border="0" cellpadding="0" cellspacing="0" align="center"><tr><![endif]-->
                                                                    <!--[if !gte mso 9]><!-->
                                                                    <!--[if mso]><td width="202"><![endif]-->
                                                                    <table class="layout-block-full-width" style="min-width: 202px; " width="202" align="left" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="layout-block-column" style="min-width: 202px; " width="202" valign="top" align="center">
                                                                                    <div class="layout-block-image">
                                                                                            <!--[if gte mso 9]><img width="202" height="16" alt="" eqid="EQMST-7AA1D66A-AF53-487D-A0C7-5350FDD03012" src="image-19.png" border="0" style="display: block; width: 202px; height: 16px;">
                                                                                            <div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;">
                                                                                            <![endif]-->
                                                                                        <img alt="" eqid="EQMST-7AA1D66A-AF53-487D-A0C7-5350FDD03012" src="<?= base_url('images/boletines/'.$c->foto) ?>" style="width: 202px; height: 16px; display: none; " width="202" height="16" border="0">
                                                                                        <!--[if gte mso 9]></div><![endif]-->
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                        <!--[if mso]></td><td width="421"><![endif]-->
                                                                    <table class="layout-block-full-width" style="min-width: 421px; " width="421" align="left" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="layout-block-column" style="min-width: 421px; " width="421" valign="top" align="center">
                                                                                    <div class="layout-block-image">
                                                                                            <!--[if gte mso 9]>
                                                                                            <img width="421" height="210" alt="" eqid="EQMST-8CCC3962-D37E-466C-B2A8-BE3F84BE1048" src="image-20.png" border="0" style="display: block; width: 421px; height: 210px;">
                                                                                            <div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;">
                                                                                            <![endif]-->
                                                                                            <img alt="" eqid="EQMST-8CCC3962-D37E-466C-B2A8-BE3F84BE1048" src="<?= base_url('images/boletines/'.$c->foto) ?>" style="width: 421px; height: 210px; display: block; " width="421" height="210" border="0">
                                                                                        <!--[if gte mso 9]>
                                                                                        </div>
                                                                                        <![endif]-->
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                        <!--[if mso]></td><td width="43"><![endif]-->
                                                                    <table class="layout-block-full-width" style="min-width: 43px; " width="43" align="left" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="layout-block-column" style="min-width: 43px; " width="43" valign="top" align="center">
                                                                                    <div class="layout-block-image">
                                                                                            <!--[if gte mso 9]>
                                                                                            <img width="43" height="16" alt="" eqid="EQMST-856636B7-D4FD-46D7-86FF-2179530AA760" src="image-21.png" border="0" style="display: block; width: 43px; height: 16px;">
                                                                                            <div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;">
                                                                                            <![endif]-->
                                                                                            <img alt="" eqid="EQMST-856636B7-D4FD-46D7-86FF-2179530AA760" src="<?= base_url('images/boletines/'.$c->foto) ?>" style="width: 43px; height: 16px; display: none; " width="43" height="16" border="0">
                                                                                        <!--[if gte mso 9]></div><![endif]-->
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <!--<![endif]-->
                                                                    <!--[if gte mso 9]>
                                                                    <table cellspacing="0" cellpadding="0"><tbody><tr><td valign="top"><img width="202" height="16" alt="" eqid="EQMST-7AA1D66A-AF53-487D-A0C7-5350FDD03012" src="image-19.png" border="0" style="display: block; width: 202px; height: 16px;"></td><td valign="top"><img width="421" height="210" alt="" eqid="EQMST-8CCC3962-D37E-466C-B2A8-BE3F84BE1048" src="image-20.png" border="0" style="display: block; width: 421px; height: 210px;"></td><td valign="top"><img width="43" height="16" alt="" eqid="EQMST-856636B7-D4FD-46D7-86FF-2179530AA760" src="image-21.png" border="0" style="display: block; width: 43px; height: 16px;"></td></tr></tbody></table>
                                                                    <![endif]-->
                                                                    <!--[if mso]></tr></table><![endif]-->
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            
                                            
                                          <?php endforeach ?>  
                                        </div>
                                        <!--End of layout container -->
                                    </td>
                                    <td width="30" class="page-bg-show-thru">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                            </tbody>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->           
    </body>
</html>