<?php

class ajax_grocery_CRUD extends grocery_CRUD {

        protected $unset_ajax_extension			= false;
        protected $state_code 			= null;
        private $slash_replacement	= "_agsl_";
        protected $relation_dependency		= array();
        public $restrict_operator_search = TRUE;
        protected $callback_relation_dependency = array();
        protected $unset_clone = true;
        public $norequireds = array();
        function __construct()
        {
                parent::__construct();
                $this->states[101]='ajax_extension';
                $this->states[302]='cropper';
                $this->states[303]='json_list';
                $this->states[304] = 'clone';
                $db_driver = get_instance()->db->platform();
                $model_name = 'Grocery_CRUD/Grocery_crud_model_'.$db_driver;
                $model_alias = 'm'.substr(md5(rand()), 0, rand(4,15) );                     
                if (file_exists(APPPATH.'models/'.$model_name.'.php')){                    
                    unset(get_instance()->{$model_name});
                    get_instance()->load->model('Grocery_CRUD/Grocery_crud_model');
                    get_instance()->load->model('Grocery_CRUD/Grocery_crud_generic_model');
                    get_instance()->load->model($model_name,$model_alias);
                    $this->basic_model = get_instance()->{$model_alias};                    
                }
        }
        
        public function callback_relation_dependency($field,$callback){
            $this->callback_relation_dependency[$field] = $callback;
        }
        
        public function set_model($model_name_model)
        {
                $db_driver = get_instance()->db->platform();
                $model_name = 'grocery_crud/grocery_crud_model_'.$db_driver;
                $model_alias = 'm'.substr(md5(rand()), 0, rand(4,15) );                     
                if (file_exists(APPPATH.'models/'.$model_name.'.php')){
                    unset(get_instance()->{$model_name});
                    get_instance()->load->model('grocery_crud/grocery_crud_model');
                    get_instance()->load->model('grocery_crud/grocery_crud_generic_model');
                    get_instance()->load->model($model_name,$model_alias);
                    get_instance()->load->model($model_alias);
                    get_instance()->load->model($model_name_model);
                }
                $this->basic_model = get_instance()->{$model_name_model};
        }

        public function inline_js($inline_js = '')
        {
                $this->_inline_js($inline_js);
        }


        public function set_relation_dependency($target_field, $source_field, $relation_field_on_source_table)
        {
                $this->relation_dependency[$target_field] = array($target_field, $source_field,$relation_field_on_source_table);
                return $this;
        }

        private function render_relation_dependencies()
        {

                foreach($this->relation_dependency as $dependency)
                {
                        $this->render_relation_dependency($dependency[0],$dependency[1],$dependency[2]);
                }

        }

        private function render_relation_dependency($target_field, $source_field, $relation_field_on_source_table){

                $sourceElement = "'#field-$source_field'";
                $targetElement = "'#field-$target_field'";

                $js_text = "
                        $(document).ready(function() {
                                $(document).on('change',$sourceElement,function(e) {
                                    if($(this).val()!=''){
                                            e.stopPropagation();
                                            var selectedValue = $($sourceElement).val();					
                                            $.post('ajax_extension/$target_field/$relation_field_on_source_table/'+encodeURI(selectedValue.replace(/\//g,'$this->slash_replacement')), {}, function(data) {					
                                            var \$el = $($targetElement);
                                                      var newOptions = data;
                                                      \$el.empty(); // remove old options
                                                      \$el.append(\$('<option></option>').attr('value', '').text(''));
                                                      \$.each(newOptions, function(key, value) {
                                                        \$el.append(\$('<option></option>')
                                                           .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                                        });
                                                      //\$el.attr('selectedIndex', '-1');
                                                      \$el.chosen().trigger('liszt:updated');

                                    },'json');
                                    $($targetElement).change();
                                }
                                });
                        });
                        ";

                $this->inline_js($js_text);

        }                

        public function render($state_code = '')
        {
                $this->pre_render();
                $this->get_relations();                
                $this->state_code = empty($state_code)?$this->getStateCode():$state_code;                
                if( $this->state_code != 0 )
                {
                        $this->state_info = $this->getStateInfo();
                }
                else
                {
                        throw new Exception('The state is unknown , I don\'t know what I will do with your data!', 4);
                        die();
                }                
                switch ($this->state_code) {
                        case 1://list
				if($this->unset_list)
				{
					throw new Exception('You don\'t have permissions for this operation', 14);
					die();
				}

				if($this->theme === null)
					$this->set_theme($this->default_theme);
				$this->setThemeBasics();

				$this->set_basic_Layout();

				$state_info = $this->getStateInfo();

				$this->showList(false,$state_info);

			break;
                        case 2://add
                            $this->render_relation_dependencies();
                            $output = parent::render();
                        break;
                        case 3://edit
                                $this->render_relation_dependencies();
                                $output = parent::render();
                        break;
                        case 5: //insert
                            if($this->unset_add)
                            {
                                    throw new Exception('This user is not allowed to do this operation', 14);
                                    die();
                            }
                            $state_info = $this->getStateInfo();
                            $insert_result = $this->db_insert($state_info);
                            $this->insert_layout($insert_result);
                        break;
                        case 6://update
                                $this->render_relation_dependencies();
                                $output = parent::render();
                        break;

                        case 101://ajax_extension

                                $state_info = $this->getStateInfo();

                                $ajax_extension_result = $this->ajax_extension($state_info);

                                $ajax_extension_result[""] = " ";
                                
                                if(!empty($this->callback_relation_dependency[$state_info->target_field_name])){
                                    echo call_user_func($this->callback_relation_dependency[$state_info->target_field_name], $state_info->filter_value, null, $state_info);
                                }
                                else{
                                    echo json_encode($ajax_extension_result);
                                }
                        die();

                        break;
                        case 304: //Clone
                            $state_info = $this->getStateInfo();
                            $this->clonerow($state_info);
                            header("Location:".$this->getListUrl().'/success');
                            die();
                        break;
                        case 302: //Cropper
                            $state_info = $this->getStateInfo();
                            $ajax_extension_result = $this->cropper($state_info);
                            $ajax_extension_result[""] = "";
                            
                            echo json_encode($ajax_extension_result);
                            die();
                        break;
                        case 303: //json
                            if($this->unset_list)
                            {
                                    throw new Exception('You don\'t have permissions for this operation', 14);
                                    die();
                            }

                            if($this->theme === null)
                                    $this->set_theme($this->default_theme);
                            $this->setThemeBasics();

                            $this->set_basic_Layout();

                            $state_info = $this->getStateInfo();
                            $this->set_ajax_list_queries($state_info);                            

                            $this->showListJson(true);
                        break;
                        default:

                                $output = parent::render();

                        break;

                }

                if(empty($output)){
                        $output = $this->get_layout();
                }else{
                }

                return $output;
        }

        protected function get_relations(){
            $data = get_instance()->db->field_data($this->basic_db_table);
            foreach($data as $d){
                $cell = $d->name;
                $cell = explode('_',$cell);
                $table = str_replace('_id','',$d->name);
                
                if(count($cell)>1 && empty($this->change_field_type[$d->name]) && in_array('id',$cell) && get_instance()->db->table_exists($table) && empty($this->relation[$d->name])){
                    $rel = get_instance()->db->field_data($table);                    
                    $this->relation[$d->name] = array($d->name,$table,$rel[1]->name,null,null);
                }
            }
        }
        
        protected function showList($ajax = false, $state_info = null)
	{
		$data = $this->get_common_data();

		$data->order_by 	= $this->order_by;

		$data->types 		= $this->get_field_types();

		$data->list = $this->get_list();
		$data->list = $this->change_list($data->list , $data->types);
		$data->list = $this->change_list_add_actions($data->list);

		$data->total_results = $this->get_total_results();

		$data->columns 				= $this->get_columns();

		$data->success_message		= $this->get_success_message_at_list($state_info);

		$data->primary_key 			= $this->get_primary_key();
		$data->add_url				= $this->getAddUrl();
		$data->edit_url				= $this->getEditUrl();
		$data->delete_url			= $this->getDeleteUrl();
		$data->read_url				= $this->getReadUrl();
                $data->clone_url			= $this->getCloneUrl();                
		$data->ajax_list_url		= $this->getAjaxListUrl();
		$data->ajax_list_info_url	= $this->getAjaxListInfoUrl();
                $data->json_list_url                   = $this->getJsonListUrl();
		$data->export_url			= $this->getExportToExcelUrl();
		$data->print_url			= $this->getPrintUrl();
		$data->actions				= $this->actions;
		$data->unique_hash			= $this->get_method_hash();
		$data->order_by				= $this->order_by;

		$data->unset_add			= $this->unset_add;
		$data->unset_edit			= $this->unset_edit;
		$data->unset_read			= $this->unset_read;
		$data->unset_delete			= $this->unset_delete;
		$data->unset_export			= $this->unset_export;
		$data->unset_print			= $this->unset_print;
                $data->unset_clone                      = $this->unset_clone;
                
		$default_per_page = $this->config->default_per_page;
		$data->paging_options = $this->config->paging_options;
		$data->default_per_page		= is_numeric($default_per_page) && $default_per_page >1 && in_array($default_per_page,$data->paging_options)? $default_per_page : 25;

		if($data->list === false)
		{
			throw new Exception('It is impossible to get data. Please check your model and try again.', 13);
			$data->list = array();
		}

		foreach($data->list as $num_row => $row)
		{
                    if(empty($this->primary_key_list)){
                        $data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->clone_url = $data->clone_url.'/'.$row->{$data->primary_key};
                    }
                    else{
                        $data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->clone_url = $data->clone_url.'/'.$row->{$this->primary_key_list};
                    }
		}

		if(!$ajax)
		{
			$this->_add_js_vars(array('dialog_forms' => $this->config->dialog_forms));

			$data->list_view = $this->_theme_view('list.php',$data,true);
			$this->_theme_view('list_template.php',$data);
		}
		else
		{
			$this->set_echo_and_die();
			$this->_theme_view('list.php',$data);
		}
	}
        
        protected function get_list(){            
                if(!empty($this->group_by)){
                    foreach($this->group_by as $g){
                        $this->basic_model->group_by($g[0]);
                    }
                }
                return parent::get_list();
        }


        public function showListJson($ajax = true, $state_info = null){
                $data = $this->get_common_data();

                $data->order_by 	= $this->order_by;

                $data->types 		= $this->get_field_types();

                $data->list = $this->get_list();
                $data->list = $this->change_list($data->list , $data->types);
                $data->list = $this->change_list_add_actions($data->list);

                $data->total_results = $this->get_total_results();

                $data->columns 				= $this->get_columns();

                $data->success_message		= $this->get_success_message_at_list($state_info);

                $data->primary_key 			= $this->get_primary_key();
                $data->add_url				= $this->getAddUrl();
                $data->edit_url				= $this->getEditUrl();
                $data->delete_url			= $this->getDeleteUrl();
                $data->read_url				= $this->getReadUrl();
                $data->clone_url			= $this->getCloneUrl();
                $data->ajax_list_url                    = $this->getAjaxListUrl();
                $data->ajax_list_info_url               = $this->getAjaxListInfoUrl();               
                $data->json_list_url                    = $this->getJsonListUrl();
                $data->export_url			= $this->getExportToExcelUrl();                
                $data->print_url			= $this->getPrintUrl();
                $data->actions				= $this->actions;
                $data->unique_hash			= $this->get_method_hash();
                $data->order_by				= $this->order_by;

                $data->unset_add			= $this->unset_add;
                $data->unset_edit			= $this->unset_edit;
                $data->unset_read			= $this->unset_read;
                $data->unset_delete			= $this->unset_delete;
                $data->unset_export			= $this->unset_export;
                $data->unset_print			= $this->unset_print;
                $data->unset_clone                      = $this->unset_clone;                

                $default_per_page = $this->config->default_per_page;
                $data->paging_options = $this->config->paging_options;
                $data->default_per_page		= is_numeric($default_per_page) && $default_per_page >1 && in_array($default_per_page,$data->paging_options)? $default_per_page : 25;

                if($data->list === false)
                {
                        throw new Exception('It is impossible to get data. Please check your model and try again.', 13);
                        $data->list = array();
                }

                foreach($data->list as $num_row => $row)
                {
                    if(empty($this->primary_key_list)){
                        $data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$data->primary_key};
                        $data->list[$num_row]->clone_url = $data->clone_url.'/'.$row->{$data->primary_key};
                    }
                    else{
                        $data->list[$num_row]->edit_url = $data->edit_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->delete_url = $data->delete_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->read_url = $data->read_url.'/'.$row->{$this->primary_key_list};
                        $data->list[$num_row]->clone_url = $data->clone_url.'/'.$row->{$data->primary_key_list};
                    }
                }

                if(!$ajax)
                {
                        $this->_add_js_vars(array('dialog_forms' => $this->config->dialog_forms));

                        $data->list_view = $this->_theme_view('list.php',$data,true);
                        $this->_theme_view('list_template.php',$data);
                }
                else
                {
                        $this->set_echo_and_die();
                        $this->_theme_view('json.php',$data);
                }
            }


    protected function getJsonListUrl()
    {
            return $this->state_url('json_list');
    }



    public function getStateInfo()
        {
                if(empty($this->state_code)){
                    $state_code = $this->getStateCode();
                }else{
                    $state_code = $this->state_code;
                }
                $segment_object = $this->get_state_info_from_url();

                $first_parameter = $segment_object->first_parameter;

                $second_parameter = $segment_object->second_parameter;

                $third_parameter = $segment_object->third_parameter;


                $state_info = (object)array();
                
                switch ($state_code) {
                        case 101: //ajax_extension
                                $state_info->target_field_name = $first_parameter;
                                $state_info->relation_field_on_source_table = $second_parameter;
                                $state_info->filter_value = $third_parameter;

                        break;
                        case 302: //Cropper
                            $state_info->target_field_name = $first_parameter;                            
                        break;
                    
                        case 304: //Clone
                            $state_info->target_field_name = $first_parameter;
                        break;
                        case 5:
                                    if(!empty($_POST))
                                    {
                                            $state_info = (object)array('unwrapped_data' => $_POST);
                                    }
                                    else
                                    {
                                            throw new Exception('On the state "insert" you must have post data',8);
                                            die();
                                    }
                            break;
                        case 303:
                        case 7:
                        case 8:
                        case 16: //export to excel
                        case 17: //print
                                $state_info = (object)array();
                                if(!empty($_POST['per_page']))
                                {
                                        $state_info->per_page = is_numeric($_POST['per_page']) ? $_POST['per_page'] : null;
                                }
                                if(!empty($_POST['page']))
                                {
                                        $state_info->page = is_numeric($_POST['page']) ? $_POST['page'] : null;
                                }
                                //If we request an export or a print we don't care about what page we are
                                if($state_code === 16 || $state_code === 17)
                                {
                                        $state_info->page = 1;
                                        $state_info->per_page = 1000000; //a big number
                                }
                                if(!empty($_POST['order_by'][0]))
                                {
                                        $state_info->order_by = $_POST['order_by'];
                                }
                                if(!empty($_POST['search_text']))
                                {
                                        if(empty($_POST['search_field']))
                                        {
                                                $search_text = strip_tags($_POST['search_field']);
                                                $state_info->search = (object)array( 'field' => null , 'text' => $_POST['search_text'] );
                                        }
                                        else
                                        {
                                                if(is_array($_POST['search_field'])){
                                                    foreach($_POST['search_field'] as $n=>$p){
                                                        $_POST['search_field'][$n] = strip_tags($p);
                                                    }
                                                }
                                                else $_POST['search_field'] = strip_tags ($_POST['search_field']);
                                                $operator = empty($this->operator_search_default)?'where':$this->operator_search_default;
                                                $operator = !empty($_POST['operator']) && !$this->restrict_operator_search?$_POST['operator']:$operator;                                                
                                                $state_info->search = (object)array( 'field' => $_POST['search_field'] , 'text' => $_POST['search_text'],'operator'=>$operator );
                                        }
                                }
                        break;

                        default:
                                $state_info = parent::getStateInfo();

                }

                return $state_info;
        }
        
        public function set_clone(){
            $this->unset_clone = false;
            return $this;
        }
        
        protected function getCloneUrl($primary_key = null)

	{

		if($primary_key === null)

			return $this->getListUrl().'/clone';

		else

			return $this->getListUrl().'/clone/'.$primary_key;

	}
        
        protected function set_ajax_list_queries($state_info = null)
        {
                if(!empty($state_info->per_page))
                {
                        if(empty($state_info->page) || !is_numeric($state_info->page) )
                                $this->limit($state_info->per_page);
                        else
                        {
                                $limit_page = ( ($state_info->page-1) * $state_info->per_page );
                                $this->limit($state_info->per_page, $limit_page);
                        }
                }

                if(!empty($state_info->order_by))
                {
                        $this->order_by($state_info->order_by[0],$state_info->order_by[1]);
                }

                if(!empty($state_info->search))
                {
                        if(!empty($this->relation))
                                foreach($this->relation as $relation_name => $relation_values)
                                        $temp_relation[$this->_unique_field_name($relation_name)] = $this->_get_field_names_to_search($relation_values);

                        if($state_info->search->field !== null)
                        {
                                if(!is_array($state_info->search->field) && isset($temp_relation[$state_info->search->field]))
                                {
                                        if(is_array($temp_relation[$state_info->search->field]))
                                                foreach($temp_relation[$state_info->search->field] as $search_field)
                                                        $this->or_like($search_field , $state_info->search->text);
                                        else
                                                $this->like($temp_relation[$state_info->search->field] , $state_info->search->text);
                                }
                                elseif(!is_array($state_info->search->field) && isset($this->relation_n_n[$state_info->search->field]))
                                {
                                        $escaped_text = $this->basic_model->escape_str($state_info->search->text);
                                        $this->having($state_info->search->field." LIKE '%".$escaped_text."%'");
                                }
                                else
                                {                                    
                                        if(!is_array($state_info->search->field)){
                                            $this->like($state_info->search->field , $state_info->search->text);
                                        }
                                        else{
                                            foreach($state_info->search->field as $n=>$f){
                                                
                                                $search_field = $state_info->search->field[$n];
                                                //Validate if the field is relation   
                                                //print_r($search_field);                                                
                                                if(!empty($temp_relation[$search_field]))
                                                    $search_field = $temp_relation[$search_field];                                                
                                                switch($state_info->search->operator){                                                    
                                                    case 'like': 
                                                        if(is_array($search_field) && !empty($state_info->search->text[$n])){
                                                            $str = '';
                                                            foreach($search_field as $s){
                                                                $str.= ' OR j'.substr($state_info->search->field[$n],1).'.'.$s.' LIKE "%'.$state_info->search->text[$n].'%"';                                                                
                                                            }
                                                            $str = substr($str,4);
                                                            $this->where($str,'ESCAPE',FALSE);
                                                        }
                                                        else{
                                                            $this->like($search_field, $state_info->search->text[$n]); 
                                                        }
                                                    break;
                                                    case 'or_like': 
                                                        if(is_array($search_field) && !empty($state_info->search->text[$n])){
                                                            foreach($search_field as $s){
                                                                $this->or_like('j'.substr($state_info->search->field[$n],1).'.'.$s , $state_info->search->text[$n]);
                                                            }
                                                        }
                                                        else{
                                                            $this->or_like($search_field, $state_info->search->text[$n]); 
                                                        }
                                                    break;
                                                    case 'where':
                                                        if(is_array($search_field) && !empty($state_info->search->text[$n])){
                                                            $str = '';
                                                            foreach($search_field as $s){
                                                                $str.= ' OR j'.substr($state_info->search->field[$n],1).'.'.$s.' = "'.$state_info->search->text[$n].'"';
                                                            }
                                                            $str = substr($str,4);
                                                            $this->where($str,'ESCAPE',FALSE);
                                                        }
                                                        else{
                                                            $this->where($search_field, $state_info->search->text[$n]); 
                                                        }
                                                    break;
                                                    case 'or_where':
                                                        if(is_array($search_field) && !empty($state_info->search->text[$n])){
                                                            foreach($search_field as $s){
                                                                $this->or_where('j'.substr($state_info->search->field[$n],1).'.'.$s , $state_info->search->text[$n]);
                                                            }
                                                        }
                                                        else{
                                                            $this->or_where($search_field, $state_info->search->text[$n]); 
                                                        }
                                                     break;
                                                }
                                                
                                            }
                                        }
                                }
                        }
                        else
                        {
                                $columns = $this->get_columns();

                                $search_text = $state_info->search->text;

                                if(!empty($this->where))
                                        foreach($this->where as $where)
                                                $this->basic_model->having($where[0],$where[1],$where[2]);

                                foreach($columns as $column)
                                {
                                        if(isset($temp_relation[$column->field_name]))
                                        {
                                                if(is_array($temp_relation[$column->field_name]))
                                                {
                                                        foreach($temp_relation[$column->field_name] as $search_field)
                                                        {
                                                                $this->or_like($search_field, $search_text);
                                                        }
                                                }
                                                else
                                                {
                                                        $this->or_like($temp_relation[$column->field_name], $search_text);
                                                }
                                        }
                                        elseif(isset($this->relation_n_n[$column->field_name]))
                                        {
                                                //@todo have a where for the relation_n_n statement
                                        }
                                        else
                                        {
                                                $this->or_like($column->field_name, $search_text);
                                        }
                                }
                        }

                }
        }



        protected function ajax_extension($state_info)
        {

                if(!isset($this->relation[$state_info->target_field_name]))
                        return false;

                list($field_name, $related_table, $related_field_title, $where_clause, $order_by)  = $this->relation[$state_info->target_field_name];


                $target_field_name = $state_info->target_field_name;

                $relation_field_on_source_table = $state_info->relation_field_on_source_table;

                $filter_value = $state_info->filter_value;

                if(is_int($filter_value)){

                        $final_filter_value = $filter_value;

                }else {

                                $decoded_filter_value = urldecode($filter_value);

                                $replaced_filter_value = str_replace($this->slash_replacement,'/',$decoded_filter_value);

                                if(strpos($replaced_filter_value,'/') !== false) {
                                        $final_filter_value = $this->_convert_date_to_sql_date($replaced_filter_value);

                                }else{
                                        $final_filter_value = $replaced_filter_value;
                                }
                }

                $target_field_relation = $this->relation[$target_field_name];

                $result = $this->get_dependency_relation_array($target_field_relation, $relation_field_on_source_table, $final_filter_value);

                return $result;
        }

        protected function get_dependency_relation_array($relation_info, $relation_key_field, $relation_key_value, $limit = null)
        {
                list($field_name , $related_table , $related_field_title, $where_clause, $order_by)  = $relation_info;

                $w = $where_clause;
                $where_clause = array($relation_key_field => $relation_key_value);
                if(!empty($w)){
                    $where_clause = array_merge($where_clause,$w);
                }

                if(empty($relation_key_value)){
                        $relation_array = array();
                }else{
                        $relation_array = $this->basic_model->get_relation_array($field_name , $related_table , $related_field_title, $where_clause, $order_by, $limit);
                }
                return $relation_array;
        }
        
        protected function get_hidden_input($field_info,$value)
        {
                if($field_info->extras !== null && $field_info->extras != false){
                        $value = $field_info->extras;
                }
                $input = "<input id='field-{$field_info->name}' type='hidden' name='{$field_info->name}' value='$value' />";
                return $input;
        }



        public function unset_ajax_extension()
        {
                $this->unset_ajax_extension = true;

                return $this;
        }


    //Overriden with the purpose of adding a third parameter, currently not calling parent. It should be changed in future if changes are made to parent.
        protected function get_state_info_from_url()
        {
                $ci = &get_instance();

                $segment_position = count($ci->uri->segments) + 1;
                $operation = 'list';

                $segements = $ci->uri->segments;
                foreach($segements as $num => $value)
                {
                        if($value != 'unknown' && in_array($value, $this->states))
                        {
                                $segment_position = (int)$num;
                                $operation = $value; //I don't have a "break" here because I want to ensure that is the LAST segment with name that is in the array.
                        }
                }

                $function_name = $this->get_method_name();

                if($function_name == 'index' && !in_array('index',$ci->uri->segments))
                        $segment_position++;

                $first_parameter = isset($segements[$segment_position+1]) ? $segements[$segment_position+1] : null;
                $second_parameter = isset($segements[$segment_position+2]) ? $segements[$segment_position+2] : null;
                $third_parameter = isset($segements[$segment_position+3]) ? $segements[$segment_position+3] : null;

                return (object)array('segment_position' => $segment_position, 'operation' => $operation, 'first_parameter' => $first_parameter, 'second_parameter' => $second_parameter, 'third_parameter' => $third_parameter);
        }

        protected function get_field_input($field_info, $value = null,$primary_key='')
        {
                        $real_type = $field_info->crud_type;

                        $types_array = array(
                                        'integer',
                                        'text',
                                        'true_false',
                                        'string',
                                        'date',
                                        'datetime',
                                        'enum',
                                        'set',
                                        'relation',
                                        'relation_readonly',
                                        'relation_n_n',
                                        'upload_file',
                                        'upload_file_readonly',
                                        'hidden',
                                        'password',
                                        'readonly',
                                        'dropdown',
                                        'multiselect',
                                        'tags',
                                        'map',
                                        'editor',
                                        'products',
                                        'image',
                                        'checkbox'
                        );
                        if (in_array($real_type,$types_array)) {
                                /* A quick way to go to an internal method of type $this->get_{type}_input .
                                 * For example if the real type is integer then we will use the method
                                 * $this->get_integer_input
                                 *  */
                                $field_info->input = $this->{"get_".$real_type."_input"}($field_info,$value,$primary_key);                                
                        }
                        else
                        {
                                $field_info->input = $this->get_string_input($field_info,$value,$primary_key);
                        }                
                return $field_info;
        }
        
        protected function get_password_input($field_info,$value)
        {
                $value = !is_string($value) ? '' : str_replace('"',"&quot;",$value);
                $value = empty($value)?$field_info->extras:$value;
                $extra_attributes = '';
                if(!empty($field_info->db_max_length))
                        $extra_attributes .= "maxlength='{$field_info->db_max_length}'";
                $input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='password' value='$value' $extra_attributes />";
                return $input;
        }
        
        protected function get_string_input($field_info,$value)
        {
                $value = !is_string($value) ? '' : str_replace('"',"&quot;",$value);
                $value = empty($value)?$field_info->extras:$value;
                $extra_attributes = '';
                if(!empty($field_info->db_max_length))
                        $extra_attributes .= "maxlength='{$field_info->db_max_length}'";
                $input = "<input id='field-{$field_info->name}' name='{$field_info->name}' class='form-control {$field_info->name}' type='text' value=\"$value\" $extra_attributes />";
                return $input;
        }

        protected function get_edit_input_fields($field_values = null)
        {
                $fields = $this->get_edit_fields();
                $types = $this->get_field_types();

                $input_fields = array();
                
                foreach($fields as $field_num => $field)
                {
                        $field_info = $types[$field->field_name];

                        $field_value = !empty($field_values) && isset($field_values->{$field->field_name}) ? $field_values->{$field->field_name} : null;
                        if(!isset($this->callback_edit_field[$field->field_name]))
                        {
                                $field_input = $this->get_field_input($field_info, $field_value,$this->getStateInfo()->primary_key);                                                                
                        }
                        else
                        {
                                $primary_key = $this->getStateInfo()->primary_key;
                                $field_input = $field_info;
                                $field_input->input = call_user_func($this->callback_edit_field[$field->field_name], $field_value, $primary_key, $field_info, $field_values);
                        }
                        switch ($field_info->crud_type) {
                                case 'invisible':
                                        unset($this->edit_fields[$field_num]);
                                        unset($fields[$field_num]);
                                        continue;
                                break;
                                case 'hidden':
                                        $this->edit_hidden_fields[] = $field_input;
                                        unset($this->edit_fields[$field_num]);
                                        unset($fields[$field_num]);
                                        continue;
                                break;
                        }

                        $input_fields[$field->field_name] = $field_input;
                }

                return $input_fields;
        }

        protected function _initialize_variables()
        {
                $ci = &get_instance();
                $ci->load->config('grocery_crud');

                $this->config = (object)array();

                /** Initialize all the config variables into this object */
                $this->config->default_language 	= $ci->config->item('grocery_crud_default_language');
                $this->config->date_format 			= $ci->config->item('grocery_crud_date_format');
                $this->config->default_per_page		= $ci->config->item('grocery_crud_default_per_page');
                $this->config->file_upload_allow_file_types	= $ci->config->item('grocery_crud_file_upload_allow_file_types');
                $this->config->file_upload_max_file_size	= $ci->config->item('grocery_crud_file_upload_max_file_size');
                $this->config->default_text_editor	= $ci->config->item('grocery_crud_default_text_editor');
                $this->config->text_editor_type		= $ci->config->item('grocery_crud_text_editor_type');
                $this->config->character_limiter	= $ci->config->item('grocery_crud_character_limiter');
                $this->config->dialog_forms			= $ci->config->item('grocery_crud_dialog_forms');
                $this->config->paging_options		= $ci->config->item('grocery_crud_paging_options');
                $this->config->map_lib = $ci->config->item('map_lib');
                $this->config->map_lat = $ci->config->item('map_lat');
                $this->config->map_lon = $ci->config->item('map_lon');
                /** Initialize default paths */
                $this->default_javascript_path				= $this->default_assets_path.'/js';
                $this->default_css_path						= $this->default_assets_path.'/css';
                $this->default_texteditor_path 				= $this->default_assets_path.'/texteditor';
                $this->default_theme_path					= $this->default_assets_path.'/themes';
                $this->operator_search_default = $ci->config->item('operator_search_default');
                $this->restrict_operator_search = $ci->config->item('restrict_operator_search');
                $this->character_limiter = $this->config->character_limiter;

                if($this->character_limiter === 0 || $this->character_limiter === '0')
                {
                        $this->character_limiter = 1000000; //a big number
                }
                elseif($this->character_limiter === null || $this->character_limiter === false)
                {
                        $this->character_limiter = 30; //is better to have the number 30 rather than the 0 value
                }
        }

        public function set_js_lib($js_file,$url=false)
        {
                $this->js_lib_files[sha1($js_file)] = !$url?base_url().$js_file:$js_file;
                $this->js_files[sha1($js_file)] = !$url?base_url().$js_file:$js_file;
        }

        protected function get_tags_input($field_info,$value)
        {		
                    //$this->set_js_lib($this->default_javascript_path.'/jquery_plugins/tags.js');
                    //$this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/tags.js');
                    $this->set_css($this->default_css_path.'/jquery_plugins/tags.css');

                    $str = '';
                    if(!empty($field_info->extras)){
                    foreach($field_info->extras as $e)
                    $str.= '"'.$e.'",';
                    }
                    $input =
                            '<script src="'.base_url($this->default_javascript_path.'/jquery_plugins/tags.js').'"></script>'.                             
                            form_input($field_info->name,$value,'id="field-'.$field_info->name.'" class="tags"').
                            '<script>$("#field-'.$field_info->name.'").tagbox({url:['.$str.']});</script>';
                    return $input;
        }

        protected function get_map_input($field_info,$value)
        {		
            $this->set_js_lib('//maps.googleapis.com/maps/api/js?key=AIzaSyBoW1mnkomGhsB2yL--AYoFdnE-jkgskSI&libraries=places',TRUE);
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/map.js');
            $extras = $field_info->extras;
            $width = !empty($extras['width'])?$extras['width']:'400px';
            $height = !empty($extras['height'])?$extras['height']:'400px';
            $lat = !empty($extras['lat'])?$extras['lat']:$this->config->map_lat;
            $lon = !empty($extras['lon'])?$extras['lon']:$this->config->map_lon;
            if(!empty($value)){
                $c = explode(',',$value);
                $lat = str_replace('(','',$c[0]);
                $lon = str_replace(')','',$c[1]);
            }
            return                    
                   '<input type="hidden" value="'.$value.'" name="'.$field_info->name.'" id="field-'.$field_info->name.'">'
                   .'<div id="map_'.$field_info->name.'" style="width:'.$width.'; height:'.$height.'"></div>'
                   .'<script>
                    var '.$field_info->name.' = new mapa(\'map_'.$field_info->name.'\',\''.$lat.'\',\''.$lon.'\');'.
                    $field_info->name.'.initialize();
                    google.maps.event.addListener('.$field_info->name.'.marker,\'dragend\',function(){$("#field-'.$field_info->name.'").val('.$field_info->name.'.marker.getPosition())});
                    </script>';
        }
        
        protected function get_checkbox_input($field_info,$value)
        {		                
                $value_is_null = !empty($value) && $value !== 0 ? 1 : 0;
                $checked = $value_is_null==1?'checked':'';
                $input = "";
                $input.= "<input type='checkbox' id='field-{$field_info->name}' name='{$field_info->name}' value='1' $checked>";
                $input .= "";

                return $input;
        }

        protected function get_editor_input($field_info,$value)
        {
            $extras = $field_info->extras;

                if($extras['type'] == 'text_editor')
                {
                        $editor = empty($extras['editor'])?$this->config->default_text_editor:$extras['editor'];
                        switch ($editor) {
                                case 'ckeditor':
                                        $this->set_js_lib($this->default_texteditor_path.'/ckeditor/ckeditor.js');
                                        $this->set_js_lib($this->default_texteditor_path.'/ckeditor/adapters/jquery.js');
                                        $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.ckeditor.config.js');
                                break;

                                case 'tinymce':
                                        $this->set_js_lib($this->default_texteditor_path.'/tiny_mce/jquery.tinymce.js');
                                        $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.tine_mce.config.js');
                                break;

                                case 'markitup':
                                        $this->set_css($this->default_texteditor_path.'/markitup/skins/markitup/style.css');
                                        $this->set_css($this->default_texteditor_path.'/markitup/sets/default/style.css');

                                        $this->set_js_lib($this->default_texteditor_path.'/markitup/jquery.markitup.js');
                                        $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.markitup.config.js');
                                break;
                        }

                        $class_name = $this->config->text_editor_type == 'minimal' ? 'mini-texteditor' : 'texteditor';
                        $value = strip_slashes($value);
                        $value = htmlspecialchars($value);
                        if(empty($value) && !empty($extras['default'])){
                            $value = strip_slashes($extras['default']);
                            $value = htmlspecialchars($value);
                        }
                        $input = "<textarea id='field-{$field_info->name}' name='{$field_info->name}' class='$class_name' >$value</textarea>";
                }
                else
                {
                        $input = "<textarea id='field-{$field_info->name}' name='{$field_info->name}'>$value</textarea>";
                }
                return $input;
        }
        
        protected function get_relation_input($field_info,$value)
        {
                $this->load_js_chosen();
                $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
                $ajax_limitation = 10000;
                $total_rows = $this->get_relation_total_rows($field_info->extras);
                //Check if we will use ajax for our queries or just clien-side javasript
                $using_ajax = $total_rows > $ajax_limitation ? true : false;
                //We will not use it for now. It is not ready yet. Probably we will have this functionality at version 1.4
                $using_ajax = false;
                //If total rows are more than the limitation, use the ajax plugin
                $ajax_or_not_class = $using_ajax ? 'chosen-select' : 'chosen-select';
                $this->_inline_js("var ajax_relation_url = '".$this->getAjaxRelationUrl()."';\n");
                $select_title = str_replace('{field_display_as}',$field_info->display_as,$this->l('set_relation_title'));
                $input = "<select id='field-{$field_info->name}'  name='{$field_info->name}' class='{$field_info->name} $ajax_or_not_class' data-placeholder='$select_title' style='width:100%; min-height:60px;'>";
                $input .= "<option value=''></option>";
                if(!$using_ajax)
                {
                        $options_array = $this->get_relation_array($field_info->extras);
                        foreach($options_array as $option_value => $option)
                        {
                                $selected = !empty($value) && $value == $option_value ? "selected='selected'" : '';
                                $input .= "<option value='$option_value' $selected >$option</option>";
                        }
                }
                elseif(!empty($value) || (is_numeric($value) && $value == '0') ) //If it's ajax then we only need the selected items and not all the items
                {
                        $selected_options_array = $this->get_relation_array($field_info->extras, $value);
                        foreach($selected_options_array as $option_value => $option)
                        {
                                $input .= "<option value='$option_value'selected='selected' >$option</option>";
                        }
                }
                $input .= "</select>";
                return $input;
        }
        
        protected function get_date_input($field_info,$value)
        {
                $this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
                $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
                if($this->language !== 'english')
                {
                        include($this->default_config_path.'/language_alias.php');
                        if(array_key_exists($this->language, $language_alias))
                        {
                                $i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-'.$language_alias[$this->language].'.js';
                                if(file_exists($i18n_date_js_file))
                                {
                                       $this->set_js_lib($i18n_date_js_file);
                                }
                        }
                }
                $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.datepicker.config.js');
                if(!empty($value) && $value != '0000-00-00' && $value != '1970-01-01')
                {
                        list($year,$month,$day) = explode('-',substr($value,0,10));
                        $date = date($this->php_date_format, mktime(0,0,0,$month,$day,$year));
                }
                else
                {
                        $date = '';
                }
                $input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='text' value='$date' maxlength='10' class='datepicker-input form-control' placeholder='{$this->ui_date_format}' />";
                return $input;
        }
        
        protected function get_datetime_input($field_info,$value)
        {
            $this->set_css($this->default_css_path.'/ui/simple/'.grocery_CRUD::JQUERY_UI_CSS);
            $this->set_css($this->default_css_path.'/jquery_plugins/jquery.ui.datetime.css');
            $this->set_css($this->default_css_path.'/jquery_plugins/jquery-ui-timepicker-addon.css');
            $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/ui/'.grocery_CRUD::JQUERY_UI_JS);
            $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery-ui-timepicker-addon.js');
            if($this->language !== 'english')
                {
                include($this->default_config_path.'/language_alias.php');
                if(array_key_exists($this->language, $language_alias))
                        {
                    $i18n_date_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/datepicker/jquery.ui.datepicker-'.$language_alias[$this->language].'.js';
                    if(file_exists($i18n_date_js_file))
                        {
                        $this->set_js_lib($i18n_date_js_file);
                        }
                        $i18n_datetime_js_file = $this->default_javascript_path.'/jquery_plugins/ui/i18n/timepicker/jquery-ui-timepicker-'.$language_alias[$this->language].'.js';
                        if(file_exists($i18n_datetime_js_file))
                            {
                            $this->set_js_lib($i18n_datetime_js_file);
                            }
                            }
                            }
                            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery-ui-timepicker-addon.config.js');
                            if(!empty($value) && $value != '0000-00-00 00:00:00' && $value != '1970-01-01 00:00:00'){
                                list($year,$month,$day) = explode('-',substr($value,0,10));
                                $date = date($this->php_date_format, mktime(0,0,0,$month,$day,$year));
                                $datetime = $date.substr($value,10);
                                }
                                else
                                    {
                                    $datetime = '';
                                    }
                                    $input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='text' value='$datetime' maxlength='19' class='datetime-input' placeholder='({$this->ui_date_format}) hh:mm:ss' />";
                                    return $input;

        }
        
        protected function get_integer_input($field_info,$value)
        {
            $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/jquery.numeric.min.js');
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.numeric.config.js');
            $extra_attributes = '';
            if(!empty($field_info->db_max_length)){
                $extra_attributes .= "maxlength='{$field_info->db_max_length}'";
            }
           $input = "<input id='field-{$field_info->name}' name='{$field_info->name}' type='number' value='$value' class='{$field_info->name} numeric form-control' $extra_attributes />";
           return $input;

        }
        //Products type
        protected function db_insert($state_info)
        {
                $validation_result = $this->db_insert_validation();

                if($validation_result->success)
                {
                        $post_data = $state_info->unwrapped_data;

                        $add_fields = $this->get_add_fields();

                        if($this->callback_insert === null)
                        {
                                if($this->callback_before_insert !== null)
                                {
                                        $callback_return = call_user_func($this->callback_before_insert, $post_data);

                                        if(!empty($callback_return) && is_array($callback_return))
                                                $post_data = $callback_return;
                                        elseif($callback_return === false)
                                                return false;
                                }

                                $insert_data = array();
                                $types = $this->get_field_types();
                                foreach($add_fields as $num_row => $field)
                                {
                                        /* If the multiselect or the set is empty then the browser doesn't send an empty array. Instead it sends nothing */
                                        if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect') && !isset($post_data[$field->field_name]))
                                        {
                                                $post_data[$field->field_name] = array();
                                        }

                                        /* If the value it's products */


                                        if(isset($post_data[$field->field_name]) && !isset($this->relation_n_n[$field->field_name]))
                                        {
                                                if(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && is_array($post_data[$field->field_name]) && empty($post_data[$field->field_name]))
                                                {
                                                        $insert_data[$field->field_name] = null;
                                                }
                                                elseif(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && $post_data[$field->field_name] === '')
                                                {
                                                        $insert_data[$field->field_name] = null;
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'date')
                                                {
                                                        $insert_data[$field->field_name] = $this->_convert_date_to_sql_date($post_data[$field->field_name]);
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'readonly')
                                                {
                                                        //This empty if statement is to make sure that a readonly field will never inserted/updated
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect'))
                                                {
                                                        $insert_data[$field->field_name] = !empty($post_data[$field->field_name]) ? implode(',',$post_data[$field->field_name]) : '';
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'datetime'){
                                                        $insert_data[$field->field_name] = $this->_convert_date_to_sql_date(substr($post_data[$field->field_name],0,10)).
                                                                                                                                                substr($post_data[$field->field_name],10);
                                                }
                                                else
                                                {
                                                        $insert_data[$field->field_name] = $post_data[$field->field_name];
                                                }
                                        }
                                }

                                $insert_result =  $this->basic_model->db_insert($insert_data);

                                if($insert_result !== false)
                                {
                                        $insert_primary_key = $insert_result;
                                }
                                else
                                {
                                        return false;
                                }

                                if(!empty($this->relation_n_n))
                                {
                                        foreach($this->relation_n_n as $field_name => $field_info)
                                        {
                                                $relation_data = isset( $post_data[$field_name] ) ? $post_data[$field_name] : array() ;
                                                $this->db_relation_n_n_update($field_info, $relation_data  ,$insert_primary_key);
                                        }
                                }

                                foreach($add_fields as $num_row => $field)
                                {					
                                    if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'products') && !isset($post_data[$field->field_name]))
                                    {      
                                       $f = '';
                                       $extras = $types[$field->field_name]->extras;
                                       //$table = get_instance()->db->query("SHOW COLUMNS FROM ".$extras['table']);                                       
                                       $table = get_instance()->db->field_data($extras['table']);        
                                       foreach($_POST as $p=>$v)
                                       {
                                           $x = explode("_",$p);                                           
                                           if($x[0]==$field->field_name)
                                           {                                               
                                               if($f!=$x[1]){
                                                   $f = $x[1];
                                                   $data = array();                                                                                                                  
                                                   foreach($table as $t){
                                                   $t->Field = $t->name;
                                                   if(isset($_POST[$x[0].'_'.$x[1].'_'.$t->Field]))$data[$t->Field] = $_POST[$x[0].'_'.$x[1].'_'.$t->Field];
                                                   }
                                                   $data[$extras['relation_field']] = $insert_primary_key;
                                                   get_instance()->db->insert($extras['table'],$data);
                                               }
                                           }
                                       }
                                    }
                                }

                                if($this->callback_after_insert !== null)
                                {
                                        $callback_return = call_user_func($this->callback_after_insert, $post_data, $insert_primary_key);

                                        if($callback_return === false)
                                        {
                                                return false;
                                        }

                                }
                        }else
                        {
                                        $callback_return = call_user_func($this->callback_insert, $post_data);

                                        if($callback_return === false)
                                        {
                                                return false;
                                        }
                        }

                        if(isset($insert_primary_key))
                                return $insert_primary_key;
                        else
                                return true;
                }

                return false;

        }

        protected function db_update($state_info)
        {
                $validation_result = $this->db_update_validation();
                $edit_fields = $this->get_edit_fields();
                if($validation_result->success)
                {
                        $post_data 		= $state_info->unwrapped_data;
                        $primary_key 	= $state_info->primary_key;
                        if($this->callback_update === null)
                        {
                                if($this->callback_before_update !== null)
                                {
                                        $callback_return = call_user_func($this->callback_before_update, $post_data, $primary_key);

                                        if(!empty($callback_return) && is_array($callback_return))
                                        {
                                                $post_data = $callback_return;
                                        }
                                        elseif($callback_return === false)
                                        {
                                                return false;
                                        }
                                }
                                $update_data = array();
                                $types = $this->get_field_types();
                                foreach($edit_fields as $num_row => $field)
                                {
                                        /* If the multiselect or the set is empty then the browser doesn't send an empty array. Instead it sends nothing */
                                        if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect') && !isset($post_data[$field->field_name]))
                                        {
                                                $post_data[$field->field_name] = array();
                                        }

                                        if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'products'))
                                        {      
                                           $f = '';                                                   
                                           $extras = $types[$field->field_name]->extras;
                                           get_instance()->db->where($extras['relation_field'],$primary_key);
                                           get_instance()->db->delete($extras['table']);
                                           //$table = get_instance()->db->query("SHOW COLUMNS FROM ".$extras['table']);                                       
                                           $table = get_instance()->db->field_data($extras['table']);

                                           foreach($_POST as $p=>$v)
                                           {
                                               $x = explode("_",$p);                                           
                                               if($x[0]==$field->field_name)
                                               {                                               
                                                   if($f!=$x[1]){
                                                       $f = $x[1];
                                                       $data = array();                                                                                                                  
                                                       foreach($table as $t){
                                                       $t->Field = $t->name;
                                                       if(isset($_POST[$x[0].'_'.$x[1].'_'.$t->Field]))$data[$t->Field] = $_POST[$x[0].'_'.$x[1].'_'.$t->Field];
                                                       }
                                                       $data[$extras['relation_field']] = $primary_key;
                                                       get_instance()->db->insert($extras['table'],$data);
                                                   }
                                               }
                                           }
                                        }
                                        if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'checkbox'))
                                        {
                                           $post_data[$field->field_name] = empty($post_data[$field->field_name])?0:$post_data[$field->field_name];
                                        }
                                        if(isset($post_data[$field->field_name]) && !isset($this->relation_n_n[$field->field_name]))
                                        {
                                                if(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && is_array($post_data[$field->field_name]) && empty($post_data[$field->field_name]))
                                                {
                                                        $update_data[$field->field_name] = null;
                                                }
                                                elseif(isset($types[$field->field_name]->db_null) && $types[$field->field_name]->db_null && $post_data[$field->field_name] === '')
                                                {
                                                        $update_data[$field->field_name] = null;
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'date')
                                                {
                                                        $update_data[$field->field_name] = $this->_convert_date_to_sql_date($post_data[$field->field_name]);
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'readonly')
                                                {
                                                        //This empty if statement is to make sure that a readonly field will never inserted/updated
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'set' || $types[$field->field_name]->crud_type == 'multiselect'))
                                                {
                                                        $update_data[$field->field_name] = !empty($post_data[$field->field_name]) ? implode(',',$post_data[$field->field_name]) : '';
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'datetime'){
                                                        $update_data[$field->field_name] = $this->_convert_date_to_sql_date(substr($post_data[$field->field_name],0,10)).
                                                                                                                                                substr($post_data[$field->field_name],10);
                                                }
                                                elseif(isset($types[$field->field_name]->crud_type) && $types[$field->field_name]->crud_type == 'checkbox'){
                                                        $update_data[$field->field_name] = empty($post_data[$field->field_name])?0:$post_data[$field->field_name];
                                                }
                                                else
                                                {
                                                        $update_data[$field->field_name] = $post_data[$field->field_name];
                                                }
                                        }
                                }
                                if($this->basic_model->db_update($update_data, $primary_key) === false)
                                {
                                        return false;
                                }
                                if(!empty($this->relation_n_n))
                                {
                                        foreach($this->relation_n_n as $field_name => $field_info)
                                        {
                                                if (   $this->unset_edit_fields !== null
                                                        && is_array($this->unset_edit_fields)
                                                        && in_array($field_name,$this->unset_edit_fields)
                                                ) {
                                                                continue;
                                                }

                                                $relation_data = isset( $post_data[$field_name] ) ? $post_data[$field_name] : array() ;
                                                $this->db_relation_n_n_update($field_info, $relation_data ,$primary_key);
                                        }
                                }

                                if($this->callback_after_update !== null)
                                {
                                        $callback_return = call_user_func($this->callback_after_update, $post_data, $primary_key);

                                        if($callback_return === false)
                                        {
                                                return false;
                                        }

                                }
                        }
                        else
                        {
                                $callback_return = call_user_func($this->callback_update, $post_data, $primary_key);

                                if($callback_return === false)
                                {
                                        return false;
                                }
                        }

                        return true;
                }
                else
                {
                        return false;
                }
        }

        protected function db_delete($state_info)
        {
                $primary_key 	= $state_info->primary_key;

                if($this->callback_delete === null)
                {
                        if($this->callback_before_delete !== null)
                        {
                                $callback_return = call_user_func($this->callback_before_delete, $primary_key);

                                if($callback_return === false)
                                {
                                        return false;
                                }

                        }

                        if(!empty($this->relation_n_n))
                        {
                                foreach($this->relation_n_n as $field_name => $field_info)
                                {
                                        $this->db_relation_n_n_delete( $field_info, $primary_key );
                                }
                        }

                        $types = $this->get_field_types();
                        $add_fields = $this->get_add_fields();                        
                        foreach($add_fields as $num_row => $field)
                        {					
                            if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'products') && !isset($post_data[$field->field_name]))
                            {      
                               $extras = $types[$field->field_name]->extras;
                               get_instance()->db->where($extras['relation_field'],$primary_key);
                               get_instance()->db->delete($extras['table']);
                            }

                            if(isset($types[$field->field_name]->crud_type) && ($types[$field->field_name]->crud_type == 'image') && !isset($post_data[$field->field_name]))
                            {      
                                $extras = $types[$field->field_name]->extras;                                
                                $delete_result = $this->basic_model->db_delete_cropper($primary_key,$field->field_name,$extras['path']);
                            }
                        }

                        $delete_result = $this->basic_model->db_delete($primary_key);

                        if($delete_result === false)
                        {
                                return false;
                        }

                        if($this->callback_after_delete !== null)
                        {
                                $callback_return = call_user_func($this->callback_after_delete, $primary_key);

                                if($callback_return === false)
                                {
                                        return false;
                                }

                        }                        
                }
                else
                {
                        $callback_return = call_user_func($this->callback_delete, $primary_key);

                        if($callback_return === false)
                        {
                                return false;
                        }
                }

                return true;
        }

        protected function get_products_input($field_info,$value,$primary_key)
        {

            //Para la parte de edicion, el campo primary_key va a traer el id del producto para hacer la consulta y renderizar todas las columnas
            $this->load_js_chosen();
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/jquery.chosen.config.js');
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/products.js');
            $extras = $field_info->extras;

            if(!empty($extras['source'])){
                $data = array(''=>'Seleccione '.$extras['dropdown']);
                if(!is_array($extras['source'])){
                $source = get_instance()->db->get($extras['source']);    //Se consulta el contenido del select                                            
                foreach($source->result() as $x)
                    $data[$x->id] = $x->{$extras['field_detalle']};
                }
                else
                {
                    foreach($extras['source'] as $x=>$y)
                    $data[$x] = $y;
                }
            }
            else $extras['dropdown'] = '';

            $inputs = '<div class="productitem" style="width:100%; text-align: left;">';      
            //$table = get_instance()->db->query("SHOW COLUMNS FROM `{$extras['table']}`");            
            if(!empty($extras['or_where']))
                {
                    foreach($extras['or_where'] as $o){
                        foreach($o as $p=>$z)
                            get_instance()->db->or_where($p,$z);
                    }
                }                
            $table = get_instance()->db->field_data($extras['table']);            
            foreach($table as $x){
                $x->Field = $x->name;
                $class = !empty($extras['class'][$x->Field])?'class="'.$extras['class'][$x->Field].'"':'';
                switch($x->Field){
                    case $extras['dropdown']: 
                        $select = '<select name="'.$field_info->name.'_x3x_'.$x->Field.'" id="field-'.$field_info->name.'_x3x_" data-placeholder="'.$field_info->name.'" '.$class.'>';                        
                        foreach($data as $x=>$y)
                            $select.='<option value="'.$x.'">'.$y.'</option>';
                        $select.= '</select>';
                        $inputs.= $select;
                    break;
                    case 'id':
                    case $extras['relation_field']:
                    case 'user': $inputs.=''; break;
                    default: 
                        $placeholder = !empty($extras['display_as'][$x->Field])?$extras['display_as'][$x->Field]:$x->Field;                        
                        $inputs.= form_input($field_info->name.'_x3x_'.$x->Field,'',$class.' id="field-'.$field_info->name.'_x3x_'.$x->Field.'" style="width:100px" placeholder="'.$placeholder.'"').' ';
                    break;
                }
            }                       
            $enlaces = '<a href="javascript:addFieldProduct(\\\'#'.$field_info->name.'_field_box\\\','.$field_info->name.'FieldProduct,\\\''.$field_info->name.'\\\')"><i class="fa fa-plus"></i></a> <a href="javascript:removeFieldProduct(\\\'#ax4x\\\')" id="ax4x" style="color:red"><i class="glyphicon glyphicon-remove"></i></a></div>';
            $inputs.= $enlaces;
            //Aqui viene el edit           
            if(!empty($primary_key))
            {
                $edits = get_instance()->db->get_where($extras['table'],array($extras['relation_field']=>$primary_key));
                $editfield = '';
                $y = 0;
                foreach($edits->result() as $e){
                $editfield.= '<div class="productitem" style="width:100%; text-align: left;">';
                foreach($table as $x){
                $x->Field = $x->name;
                $d = $edits->row($y);
                $class = !empty($extras['class'][$x->Field])?'class="'.$extras['class'][$x->Field].'"':'';
                switch($x->Field){
                    case $extras['dropdown']: 
                        $editfield.= form_dropdown($field_info->name.'_'.$y.'_'.$x->Field,$data,$d->{$x->Field},'id="field-'.$field_info->name.'_'.$y.'_" data-placeholder="'.$field_info->name.'" '.$class.'');                        
                    break;
                    case 'id':
                    case $extras['relation_field']:
                    case 'user': $inputs.=''; break;
                    $placeholder = !empty($extras['display_as'][$x->Field])?$extras['display_as'][$x->Field]:$x->Field;                    
                    default: $editfield.= form_input($field_info->name.'_'.$y.'_'.$x->Field,$d->{$x->Field},$class.' id="field-'.$field_info->name.'_'.$y.'_'.$x->Field.'" style="width:50px" placeholder="'.$placeholder.'"').' '; break;
                }                
                }
                $y++;
                $editfield.='<a href="javascript:addFieldProduct(\'#'.$field_info->name.'_field_box\','.$field_info->name.'FieldProduct,\''.$field_info->name.'\')"><i class="fa fa-plus"></i></a> <a href="javascript:removeFieldProduct(\'#aa'.$y.'\')" id="aa'.$y.'" style="color:red"><i class="glyphicon glyphicon-remove"></i></a></div>';
                }
            }

            //Salida
            $str =  '<script>'.$field_info->name.'FieldProduct = \''.$inputs.'\'; FieldProductId[\''.$field_info->name.'\'] = 0; console.log(FieldProductId); ';
            if(empty($primary_key) || $edits->num_rows()==0)$str.= 'addFieldProduct(\'#'.$field_info->name.'_field_box\','.$field_info->name.'FieldProduct,\''.$field_info->name.'\')';
            $str.= '</script>';            
            if(!empty($primary_key))$str.= $editfield.'<script>FieldProductId[\''.$field_info->name.'\'] = '.$y.'</script>';            
            return $str;
        }
        
        function clonerow($state_info){
            $id = $state_info->target_field_name;
            
            $this->basic_model->db_clone($id,$this->upload_fields);
        }

        function cropper($state_info)
        {
            switch($state_info->target_field_name)
            {
                case 'crop': 
                    if(!empty($_POST)){
                    $imgUrl = $_POST['imgUrl'];
                    $imgInitW = $_POST['imgInitW'];
                    $imgInitH = $_POST['imgInitH'];
                    $imgW = $_POST['imgW'];
                    $imgH = $_POST['imgH'];
                    $imgY1 = $_POST['imgY1'];
                    $imgX1 = $_POST['imgX1'];
                    $cropW = $_POST['cropW'];
                    $cropH = $_POST['cropH'];
                    $jpeg_quality = 100;
                    $name = explode("/",$imgUrl);
                    $name = $name[count($name)-1];
                    $output_filename = $_POST['url'].'/'.$name;
                    $output_filename = explode(".",$output_filename);
                    $output_filename = $output_filename[0].rand();
                    $what = getimagesize($imgUrl);
                    switch(strtolower($what['mime']))
                    {
                        case 'image/png':
                            $img_r = imagecreatefrompng($imgUrl);
                                    $source_image = imagecreatefrompng($imgUrl);
                                    $type = '.png';
                            break;
                        case 'image/jpeg':
                            $img_r = imagecreatefromjpeg($imgUrl);
                                    $source_image = imagecreatefromjpeg($imgUrl);
                                    $type = '.jpeg';
                            break;
                        case 'image/gif':
                            $img_r = imagecreatefromgif($imgUrl);
                                    $source_image = imagecreatefromgif($imgUrl);
                                    $type = '.gif';
                            break;
                        default: die('image type not supported');
                    }
                    $resizedImage = imagecreatetruecolor($imgW, $imgH);
                    imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW,$imgH, $imgInitW, $imgInitH);	
                    $dest_image = imagecreatetruecolor($cropW, $cropH);
                    imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW,$cropH, $cropW, $cropH);	
                    imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
                    $response = array("status" => 'success',"url" => base_url($output_filename.$type ));
                    unlink($_POST['url'].'/'.$name);
                     return $response;
                    }
                    else
                    return array('status'=>'Fail, Faltan datos');
                break;
                case 'save': 
                    $imagePath = $_POST['url'].'/';
                    $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
                    $temp = explode(".", $_FILES["img"]["name"]);
                    $extension = end($temp);
                    if ( in_array($extension, $allowedExts))
                      {
                      if ($_FILES["img"]["error"] > 0)
                            {
                                     $response = array(
                                            "status" => 'error',
                                            "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                                    );
                                    echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
                            }
                      else
                            {

                              $filename = $_FILES["img"]["tmp_name"];
                              list($width, $height) = getimagesize( $filename );

                              move_uploaded_file($filename,  $imagePath . $this->_unique_field_name($_FILES["img"]["name"]).'.'.$extension);

                              $response = array(
                                    "status" => 'success',
                                    "url" => base_url($imagePath.$this->_unique_field_name($_FILES["img"]["name"]).'.'.$extension),
                                    "width" => $width,
                                    "height" => $height
                              );

                            }
                      }
                    else
                      {
                       $response = array(
                                    "status" => 'error',
                                    "message" => 'something went wrong',
                            );
                      }

                      return $response;
                break;

                case 'delete_crop':
                    if(file_exists($_POST['path']."/".$_POST['url']))
                    {
                            if( unlink($_POST['path']."/".$_POST['url']) )
                            {
                                    $this->basic_model->db_file_delete($_POST['field'],$_POST['url']);

                                    return array('success'=>true);
                            }
                            else
                            {
                                    return array('success'=>false);
                            }
                    }
                    else
                    {
                            $this->basic_model->db_file_delete($_POST['field'],$_POST['url']);
                            return array('success'=>true);
                    }
                break;
            }
        }

        function get_image_input($field_info,$value,$primary_key)
        {
            $this->set_css($this->default_css_path.'/jquery_plugins/cropper/croppic.css');
            $this->set_js_lib($this->default_javascript_path.'/jquery_plugins/croppic.min.js');
            $this->set_js_config($this->default_javascript_path.'/jquery_plugins/config/cropper.js',true);
            $extras = $field_info->extras;
            $url = $extras['path'];
            $str = '<script>var url = \''.base_url().'\'; var crop_upload_url = \''.$url.'\'; croppers.push(\'crop-'.$field_info->name.'\');</script>';

            if(empty($primary_key))
            $input = '<input style="visibility:hidden" type="file" name="'.$field_info->name.'" id="'.'field-'.$field_info->name.'">';
            else
            $input = '<input style="visibility:hidden" type="input" value="'.$value.'" name="'.$field_info->name.'" id="'.'field-'.$field_info->name.'">';
            return '
                    <div class="crop" style="width:'.$extras['width'].'; height:'.$extras['height'].'; position:relative;" id="crop-'.$field_info->name.'">
                    '.$input.'
                    </div>
                '.$str;
        }

        public function required_fields_array($args = '')
        {
            if(!empty($args)){
                $this->required_fields = $args;                
            }
            else{
                //Validar si el driver trae si el campo acepta NULL o no, se bloquea si no lo acepta
                $fields = get_instance()->db->field_data($this->basic_db_table);
                $args = array();
                foreach($fields as $f){
                    $f = (array)$f;
                    if(array_key_exists('null',$f) && $f['null']=='NO' && !in_array($f['name'],$this->norequireds)){
                        $args[] = $f['name'];
                    }
                }
                $this->required_fields = $args;            
            }
            return $this;
        }

        public function getParameters($group = TRUE)
        {		
                $segment_object = $this->get_state_info_from_url();
                $operation = $segment_object->operation;
                if($group){
                    switch($segment_object->operation){
                        case 'list':
                        case 'ajax_list':
                        case 'ajax_list_info':
                        case 'success':
                            $operation = 'list';
                        break;
                        case 'add':
                        case 'insert':
                        case 'insert_validation':
                            $operation = 'add';
                        break;
                        case 'edit':
                        case 'update':
                        case 'update_validation':
                            $operation = 'edit';
                        break;

                    }
                }
                return $operation;
        }
         
        public function encodeFieldName($fieldname,$type = 'field'){
            
            return $type=='field'?
                    's'.substr(md5($fieldname),0,8) //This s is because is better for a string to begin with a letter and not with a number
                    :
                    'j'.substr(md5($fieldname),0,8); //This s is because is better for a string to begin with a letter and not with a number
                            
        }
        
        protected function change_list_add_actions($list)
	{
		if(empty($this->actions))
			return $list;

		$primary_key = $this->get_primary_key();

		foreach($list as $num_row => $row)
		{
			$actions_urls = array();
			foreach($this->actions as $unique_id => $action)
			{
                            $primary_key = empty($this->primary_key_list_actions)?$primary_key:$this->primary_key_list_actions;
				if(!empty($action->url_callback))
				{
					$actions_urls[$unique_id] = call_user_func($action->url_callback, $row->$primary_key, $row);
				}
				else
				{
					$actions_urls[$unique_id] =
						$action->url_has_http ?
							$action->link_url.$row->$primary_key :
							base_url($action->link_url.'/'.$row->$primary_key);
				}
			}
			$row->action_urls = $actions_urls;
		}

		return $list;
	}
        
        protected function showAddForm() {
            $this->set_js_lib($this->default_javascript_path . '/' . grocery_CRUD::JQUERY);
            $data = $this->get_common_data();
            $data->types = $this->get_field_types();
            $data->list_url = $this->getListUrl();
            $data->insert_url = $this->getInsertUrl();
            $data->validation_url = $this->getValidationInsertUrl();
            $data->input_fields = $this->get_add_input_fields();
            $data->fields = $this->get_add_fields();
            $data->hidden_fields = $this->get_add_hidden_fields();
            $data->unset_back_to_list = $this->unset_back_to_list;
            $data->unique_hash = $this->get_method_hash();
            $data->is_ajax = $this->_is_ajax();
            $data->table = $this->get_table();
            $this->_theme_view('add.php', $data);
            $this->_inline_js("var js_date_format = '" . $this->js_date_format . "';");
            $this->_get_ajax_results();
        }

        protected function showEditForm($state_info){
            $this->set_js_lib($this->default_javascript_path . '/' . grocery_CRUD::JQUERY);
            $data = $this->get_common_data();
            $data->types = $this->get_field_types();
            $data->field_values = $this->get_edit_values($state_info->primary_key);
            $data->add_url = $this->getAddUrl();
            $data->list_url = $this->getListUrl();
            $data->update_url = $this->getUpdateUrl($state_info);
            $data->delete_url = $this->getDeleteUrl($state_info);
            $data->read_url = $this->getReadUrl($state_info->primary_key);                                    
            $data->unique_hash = $this->get_method_hash();            
            $data->input_fields = '';
            $hidden = array();
            if(!is_array($data->field_values)){
                $data->input_fields = $this->get_edit_input_fields($data->field_values);                
            }else{
                foreach($data->field_values as $n=>$v){
                    $data->input_fields[] = json_encode($this->get_edit_input_fields($v));
                    $hidden[] = (object)array('field_name'=>'id','input'=>'<input type="hidden" name="id_'.$n.'_'.$this->get_table().'" value="'.$v->id.'">');
                }                
                if(!empty($data->input_fields)){
                    foreach($data->input_fields as $n=>$d){
                        $data->input_fields[$n] = json_decode($d);
                    }
                }else{
                    $data->input_fields = $this->get_add_input_fields();
                    $data->fields = $this->get_add_fields();
                    $data->hidden_fields = $this->get_add_hidden_fields();
                }
            }
            $data->fields = $this->get_edit_fields();
            if(empty($data->hidden_fields)){
                $data->hidden_fields = $this->get_edit_hidden_fields();
                foreach($hidden as $h){
                    $data->hidden_fields[] = $h;
                }
            }
            $data->unset_back_to_list = $this->unset_back_to_list;
            $data->validation_url = $this->getValidationUpdateUrl($state_info->primary_key);
            $data->is_ajax = $this->_is_ajax();
            $data->table = $this->get_table();
            $this->_theme_view('edit.php', $data);
            $this->_inline_js("var js_date_format = '" . $this->js_date_format . "';");
            $this->_get_ajax_results();
        }
        
        protected function insert_layout($insert_result = false)
        {		
                
                if($insert_result === false)
                {
                        echo json_encode(array('success' => false));
                         $this->set_echo_and_die();
                }
                else
                {                    
                        $success_message = '<p>'.$this->l('insert_success_message');
                        if(!$this->unset_back_to_list && !empty($insert_result) && !$this->unset_edit)
                        {
                                $success_message .= " <a class='go-to-edit-form' href='".$this->getEditUrl($insert_result)."'>".$this->l('form_edit')." {$this->subject}</a> ";
                                if (!$this->_is_ajax()) {
                                        $success_message .= $this->l('form_or');
                                }
                        }
                        if(!$this->unset_back_to_list && !$this->_is_ajax())
                        {
                                $success_message .= " <a href='".$this->getListUrl()."'>".$this->l('form_go_back_to_list')."</a>";
                        }
                        $success_message .= '</p>';                        
                        $salida = "<textarea>".json_encode(array(

                                        'success' => true ,

                                        'insert_primary_key' => $insert_result,

                                        'success_message' => $success_message,

                                        'success_list_url'	=> $this->getListSuccessUrl($insert_result)

                        ))."</textarea>";
                        echo $salida;
                        if(empty($this->display_and_exit) || $this->display_and_exit == 'activo'){                            
                            $this->set_echo_and_die();
                            die();
                        }
                }		

        }
        
        protected function update_layout($update_result = false, $state_info = null) {
            @ob_end_clean();
            if ($update_result === false) {
                echo json_encode(array('success' => $update_result));
            } else {
                $success_message = '<p>' . $this->l('update_success_message');
                if (!$this->unset_back_to_list && !$this->_is_ajax()) {
                    $success_message .= " <a href='" . $this->getListUrl() . "'>" . $this->l('form_go_back_to_list') . "</a>";
                }
                $success_message .= '</p>';
                /* The textarea is only because of a BUG of the jquery form plugin with the combination of multipart forms */
                echo "<textarea>" . json_encode(array(
                    'success' => true,
                    'insert_primary_key' => $update_result,
                    'success_message' => $success_message,
                    'success_list_url' => $this->getListSuccessUrl($state_info->primary_key)
                )) . "</textarea>";
            }
            if(empty($this->display_and_exit) || $this->display_and_exit == 'activo'){                            
                $this->set_echo_and_die();
            }
    }

    protected function validation_layout($validation_result) {
        echo "<textarea>" . json_encode($validation_result) . "</textarea>";        
        if (empty($this->display_and_exit) || $this->display_and_exit == 'activo' || !$validation_result->success) {
            $this->set_echo_and_die();
        }
    }
    
    public function group_by($key)
        {
                $this->group_by[] = array($key);

                return $this;
        }
}