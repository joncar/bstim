<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="ISO-8859-1">
        <meta name="Viewport" content="width=device-width, initial-scale=1.0">
        <style type="text/css" id="Mail Designer General Style Sheet">
            a { word-break: break-word; }
            a img { border:none; }
            img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
            body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family:'Arial' !important;}
            .ExternalClass { width: 100%; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            #page-wrap { margin: 0; padding: 0; width: 100% !important;}
            #outlook a { padding: 0; }
            .preheader { display:none !important; }
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: 'Arial' !important; font-weight: inherit !important; line-height: inherit !important; }
        </style>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        
    </head>
    <body style="margin-top: 0px; margin-right: 0px; margin-bottom: 50px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 50px; padding-left: 0px; " eqid="EQMST-5B629705-2068-4643-A1B4-1BA752D88F04" background="<?= base_url('images/boletines')?>/page-bg.jpg">
        <center><a href="<?= base_url('boletines/frontend/ver/'.$detail->id) ?>" style="color:#333">Si no veus bé aquest email fes clik aquí</a></center>
        <!--[if gte mso 9]>
        <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="<?= base_url('images/boletines')?>/page-bg.jpg" />
        </v:background>
        <![endif]-->
        <table id="page-wrap" width="100%" align="center" cellspacing="0" cellpadding="0" background="<?= base_url('images/boletines')?>/page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" id="email-body" width="726" align="center" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Left page bg show-thru --></td>
                                    <td id="page-body" eqid="EQMST-E42EE1D2-FD64-4CC3-81A8-CEDDEEA78958" width="666" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/background.jpg">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer" style="height: auto; ">
                                            
                                            
                                            <div eqid="EQMST-E7DD055F-9768-41B1-B11F-7203C5F91402" width="100%" class="desktop-only">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        
                                                                                    <a href="http://www.bstim.cat">
                                                                                        <img alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines')?>/image-3.png" style="width: 666px; height: 131px; display: block; " width="666" height="131" border="0">
                                                                                    </a>
                                                                                    
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-188CA4C8-0993-41A1-96D4-3F44AF105B12" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        <a href="<?= $detail->link_banner ?>">
                                                                                            <img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/'.$detail->banner)?>" border="0" style="display: block; width: 666px; height: 394px;">
                                                                                        </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <?php foreach($detail->cuerpo->result() as $c): ?>
                                            <!--------- Comienza contenido ---------->
                                            <div eqid="EQMST-F80758DE-9EEF-47C7-AC06-93EA4A39DE2D" width="100%" style="margin-top:15px; margin-bottom:15px;">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-23860111-7423-4AD5-9AB8-FB4626A266B0" class="desktop-only" width="100%">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-content-cell" eqid="EQMST-A92510C3-ACA8-41FD-AEAB-8686AB13634B" style="min-width: 666px; " width="666" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-1.jpg">
                                                                <table class="mso-fixed-width layout-block-content-cell" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="layout-block-content-cell" style="min-width: 666px; " width="666" align="left"><!--[if mso]><table width="666" border="0" cellpadding="0" cellspacing="0" align="center"><tr><![endif]-->
                                                                                    <!--[if mso]><td width="241"><![endif]-->
                                                                                <table class="layout-block-full-width" style="min-width: 241px; " width="241" align="left" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 241px; " width="241" valign="top" align="left">
                                                                                                <div class="layout-block-image">
                                                                                                        <!--[if gte mso 9]>
                                                                                                        <img width="241" alt="" eqid="EQMST-0EBC43D5-3A59-4C1A-B28F-C1C606F72CFD" src="<?= base_url('images/boletines/'.$c->foto)?>" border="0" eqoriginalbackground="<?= base_url('images/boletines')?>/image-6.png" style="display: block; width: 241px; height: auto">
                                                                                                        <div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]-->
                                                                                                    <img alt="" eqid="EQMST-0EBC43D5-3A59-4C1A-B28F-C1C606F72CFD" src="<?= base_url('images/boletines/'.$c->foto)?>" style="width: 241px; display: block;" width="241" border="0">
                                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                    <!--[if mso]></td><td width="425"><![endif]-->
                                                                                    <table class="layout-block-full-width" style="min-width: 425px; " width="425" align="left" cellspacing="0" cellpadding="0">
                                                                                    <!--[if !mso]><!-->
                                                                                    <tbody>
                                                                                        
                                                                                        <!--<![endif]-->
                                                                                        <tr>
                                                                                            <td class="layout-block-padding-left" style="min-width: 33px; " width="33">&nbsp;</td>
                                                                                            <td class="layout-block-content-cell" style="min-width: 359px; " width="359" valign="top" align="left">
                                                                                                <div class="text" eqid="EQMST-57A7DC5C-6959-48D7-8BF0-AF913A625979" style="font-size: 16px; font-family: 'Lucida Grande', 'Arial'; ">
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_ingles ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/en').'?url='.$c->url_ingles ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Read More</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo_catalan ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/ca').'?url='.$c->url_catalan ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Llegir més</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(177, 0, 26); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 20px; font-weight: bold; line-height: 1.2; text-align: left; ">
                                                                                                        <span style="color: rgb(0, 0, 0); "><?= $c->titulo ?></span>
                                                                                                    </div>
                                                                                                    <a href="<?= base_url('main/traduccion/es').'?url='.$c->url ?>" style="color: rgb(126, 221, 56); font-family: 'Open Sans', 'Arial', 'Helvetica Neue', 'Arial', sans-serif; font-size: 12px; font-style: normal; font-weight: 400; line-height: 27px; text-align: left; text-decoration: none; ">
                                                                                                        <font color="#ae1916">Leer más</font>
                                                                                                    </a>
                                                                                                    <div style="color: rgb(0, 0, 0); font-family: 'Open Sans', 'Arial', 'Helvetica Neue'; font-size: 32px; font-style: normal; font-weight: normal; line-height: 1.5; text-align: left; text-decoration: none; ">
                                                                                                    </div>                                                                                                        
                                                                                                </div>
                                                                                            </td>
                                                                                            <td class="layout-block-padding-right" style="min-width: 33px; " width="33">&nbsp;</td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                                <!--[if mso]></tr></table><![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-A8B3B39F-10E0-40EA-B9F9-9E132D0DFF9C" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody><tr>
                                                            <td class="layout-block-padding-left" style="min-width: 33px; " width="33">&nbsp;</td>
                                                            <td class="layout-block-content-cell" eqid="EQMST-25FE1786-EE7C-4027-A95E-C87018BCF30B" style="min-width: 600px; " width="600" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-3.jpg">
                                                                <table style="padding-left: 10px; padding-right: 10px; min-width: 600px; " class="layout-block-box-padding" width="600" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="layout-block-padded-column" style="min-width: 580px; " width="580" valign="top" align="center">
                                                                                <div class="layout-block-image">
                                                                                        <!--[if gte mso 9]><img width="580" height="1" alt="" eqid="EQMST-5F31893F-EC2A-40F3-9019-15C7DF074171" src="<?= base_url('images/boletines')?>/image-9.png" border="0" eqoriginalbackground="<?= base_url('images/boletines')?>/image-8.png" style="display: block; width: 580px; height: 1px;"><div style="width:0px; height:0px; max-height:0; max-width:0; overflow:hidden; display:none; visibility:hidden; mso-hide:all;"><![endif]-->
                                                                                    <img alt="" eqid="EQMST-5F31893F-EC2A-40F3-9019-15C7DF074171" src="<?= base_url('images/boletines')?>/image-9.png" style="width: 580px; height: 1px; display: block; " width="580" height="1" border="0">
                                                                                    <!--[if gte mso 9]></div><![endif]-->
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 33px; " width="33">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            <?php endforeach ?>
                                            <!---------- FIN CONTENIDO ------------>
                                            <div eqid="EQMST-66A68E4D-7B41-487F-833C-BCCA15FB7D50" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 260px;text-align:center" width="260">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>"><img src="<?= base_url('images/boletines/logo.png' ) ?>"style="margin-top: 30px " ></a>
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 25px; " width="25">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 616px; " width="616" valign="top" align="left">
                                                                <table class="layout-block-column" style="min-width: 616px; " width="616" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: 10px; padding-right: 10px; min-width: 596px; " eqid="EQMST-6F1F647C-304E-4260-A64E-BFC53CA5C677" width="596" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-2.jpg">
                                                                                <table class="layout-block-box-padding" style="min-width: 596px; " width="596" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 596px; " width="596" align="left">
                                                                                                <div class="heading" eqid="EQMST-3B228E9E-2621-4001-908E-74A5025C2D05" style="font-size: 16px; font-family:'Arial'; text-align: center; ">
                                                                                                    <div style=" margin-left: px; margin-top: 20px;padding-bottom: 30px; margin-bottom: 20px">
                                                                                                        <a href="https://www.facebook.com/bstimfair/">
                                                                                                            <img src="<?= base_url('images/boletines/face.png') ?>" style="">
                                                                                                        </a>
                                                                                                        <a href="https://twitter.com/bstim_fair">
                                                                                                            <img src="<?= base_url('images/boletines/twit.png') ?>" style=" padding-top: 8px;">
                                                                                                        </a>
                                                                                                    </div>                                                                                                    
                                                                                                    <div style="color:rgb(94,94,94);font-family:'Arial','Helvetica Neue';font-size:11px" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.bstim.cat/blog/main/unsubscribe&amp;source=gmail&amp;ust=1482428959994000&amp;usg=AFQjCNETdFS7_JoTq9RFSVY2L0uZ6exhlQ">    
                                                                                                        En compliment de la LOPD 15/99 i LSSI 34/2002, i altres disposicions legals, l'informem que les seves dades de caràcter personal passaran a formar 
                                                                                                        part d'un fitxer automatitzat de caràcter  personal el Responsable del qual és FIRA D'IGUALADA. Autoritza la utilització dels mateixos per a les 
                                                                                                        comunicacions, incloent les realitzades via correu electrònic, que FIRA D'IGUALADA realitzi amb finalitats promocionals o informatives de les activitats 
                                                                                                        que organitza i/o dóna suport amb la seva logística. En cas que contracti algun servei amb FIRA D'IGUALADA, queda informat que les seves dades podran ser 
                                                                                                        comunicades, amb obligació de confidencialitat, a les empreses col·laboradores de FIRA D'IGUALADA, sempre que això sigui necessari a les fins que aquestes 
                                                                                                        realitzin el servei contractat. Queda igualment informat de la possibilitat d'exercitar sobre les dades els drets d'accés, rectificació, cancel·lació i oposició, 
                                                                                                        per la qual cosa haurà de dirigir-se per e-mail  

                                                                                                        <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 10px; color:blue; padding-top: 8px; ">    
                                                                                                            info@firaigualada.org.<br><br><br>
                                                                                                        </a>                                                                                                    
                                                                                                        <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px;">
                                                                                                            Avis Legal  
                                                                                                        </a>| 
                                                                                                        <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px; ">    
                                                                                                            Donar-se de baixa <br><br><br>
                                                                                                        </a>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 25px; " width="25">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" style="background-image:url(background-3.jpg); background-position: 0% 0%; " class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('images/boletines')?>/background-3.jpg">
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        <!--End of layout container -->

                                    </td>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                            </tbody>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>