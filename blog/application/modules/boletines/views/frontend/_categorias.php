<aside class="col-md-3 text-uppercase sidebar"> 
    <div class="regions-sidebar text-uppercase animated slideInLeft">                                 
        <h6>
            Categories 
            <span><?= $categorias->num_rows() ?></span>
        </h6> 
        <ul class="list-unstyled"> 
            <?php if($categorias->num_rows()==0): ?>
            <li>
                <a href="#">Sin categorías</a>
            </li>
            <?php endif ?>
            <?php foreach($categorias->result() as $c): ?>
            <li> 
                <a href="javascript:changeCategoria(<?= $c->id ?>)"> 
                    <span><?= $c->categorias_hoteles_nombre ?></span>
                    <b class="pull-right"><?= $c->cantidad ?></b> 
                </a>
            </li> 
            <?php endforeach ?>
        </ul> 
    </div>
    <div class="contact-us-small wow slideInLeft" style="visibility: visible; animation-name: slideInLeft;"> 
        <h5 class="text-uppercase">need help ?</h5> 
        <p style="margin: 0px;">
            Our most experienced guides about all advenure types
        </p>
        <a class="btn text-uppercase" href="#">Contact us</a>
    </div>
    <form id="searchForm" data-wow-delay="0.2s" class="wow slideInLeft" style="animation-delay: 0.2s; animation-name: none;" action="<?= base_url('hoteles') ?>"> 
        <!--<input type="search" placeholder="Search..." name="descripcion"> -->
        <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
        <input type="hidden" id="categorias_hoteles_id" name="categorias_hoteles_id" value="<?= !empty($_GET['categorias_hoteles_id'])?$_GET['categorias_hoteles_id']:'' ?>">
        <!--<button type="submit"> <i class="fa fa-search"></i> </button> -->
    </form>
</aside> <!-- /.sidebar --> <!-- blog posts intro list --> 
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#categorias_hoteles_id").val(id);
        $("#searchForm").submit();
    }
</script>