<?php echo $output ?>
<script src='<?= base_url('js/jquery.mask.js'); ?>'></script>
<script>
    function add(obj){
        obj = $(obj).parents('.programacionMateriasPlan');
        var element = obj.clone();
        obj.after(element);
        $(".materiaField").removeClass('chzn-done');
        $(".programacionMateriasPlan .chzn-container").remove();
        var id = 0;
        var fields = $(".materiaField").length;
        $(".materiaField").each(function(){
            $(this).removeAttr('id');
            $(this).attr('id',id+'_chzn');
            p = $(this).parents('.programacionMateriasPlan').find('.filaNumber');
            p.html(id);
            id++;
            if(fields===id){
                $(".materiaField").chosen({allow_single_deselect:true});
                $(".chzn-container").css('width','100%');                
            }
        });
    }
    function remove(obj){
        $(obj).parents('.programacionMateriasPlan').remove();
    }
    $(document).on('click','.remove',function(e){
        e.preventDefault();
        remove($(this));
    });
    $(document).on('click','.add',function(e){
        e.preventDefault();
        add($(this));
    });
    $(document).on('change','#field-programacion_materias_plan_id',function(){
        if($(this).val()!==''){
            $.get('<?php echo base_url('procesosacademicos/procesosacademicosJson/actas_inscritos/') ?>/'+$(this).val()+'/'+$("#field-anho_lectivo").val()<?php if(!empty($acta_final))echo '+"/'.$acta_final->id.'"'; ?>,{},function(data){
                data = JSON.parse(data);
                $("#field-cant_alum_inscriptos").val(data['total']);
                $(".extrafields").replaceWith(data['extrafields']);                
                $("select").removeClass('chzn-done');
                $(".chzn-container").remove();
                $(".chosen-select,.chosen-multiple-select").chosen({allow_single_deselect:true});
                $(".chzn-container").css('width','100%');
            });
        }
    });
    
     $(document).on('change','#field-plan_estudio_id',function(e) {
            if($(this).val()!==''){
                e.stopPropagation();
                var selectedValue = $('#field-plan_estudio_id').val();
                var old = $('#field-programacion_materias_plan_id').val();
                $.post('ajax_extension/programacion_materias_plan_id/', {plan_estudio:selectedValue,anho_lectivo:$("#field-anho_lectivo").val()}, function(data) {					
                var $el = $('#field-programacion_materias_plan_id');
                          var newOptions = data;
                          $el.empty(); // remove old options
                          $el.append($('<option></option>').attr('value', '').text(''));
                          $.each(newOptions, function(key, value) {
                            $el.append($('<option></option>')
                               .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                if(key==old){
                                    $el.val(old);
                                }
                            });
                          //$el.attr('selectedIndex', '-1');
                          $el.chosen().trigger('liszt:updated');
                          $(".chzn-container").css('width','100%');

        },'json');
        $('#field-programacion_materias_plan_id').change();
        }
    });
    
    $(document).on('change','.materiaField',function(){
         var val = $(this).val();
         var tot = $(".materiaField").length;
         var ind = 0;
         var ocu = 0;
        $(".materiaField").each(function(){            
            if(val===$(this).val()){
                ocu++;
            }
            ind++;
            if(ind===tot && ocu>1){
                alert('este estudiante ya fue asignado a la lista');
            }
        });
    });
        
    $(document).on('ready',function(){
        $("#estado_acta_id_field_box").after('<div class="extrafields"></div>');
        $(".chzn-container").css('width','100%');
        <?php if(!empty($acta_final)){
            echo '$("#field-plan_estudio_id").trigger("change"); ';
        }
        ?>
    });
</script>