/*
 * Initializing the plugins we use and our little functions ;-)
 * 1. Select list dropdown,
 * 2. Animation on scroll - wow.js,
 * 3. Page loading function (Preloader),
 * 4. Function for adding sections background from html,
 * 5. Function for moving about section to watch video presentation,
 * 6. Arrow up && down functions,
 * 7. Tour page top EYE hover functions
 * 8. Gallery lightbox - magnific-popup
 * 9. Second navigation smooth scrool to div 
 * 10. Navbar fixed position
 * 11. Play youtube video
 */ 

"use strict";
var navbar = $(".navbar-default");
var arrowUp = $('.goUp-btn');
var arrowDown = $('.scroll-down');
var interval = 0;
var timeOut = setInterval(function(){preLoader();}, 2000);


// 1. Initializing select list 
  
(function selectList() {
	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
		new SelectFx(el);
	} );
})();


// 2. Initializing wow animation on scroll
	var wow = new WOW(
		{
			boxClass:     'wow',     
			animateClass: 'animated',
			offset:       30,         
			mobile:       false,      
			live:         true,      
		}
	)

// 3. page loading function
function preLoader(){
			
	if (interval == 1) {

		clearInterval(timeOut);
	
		$('.page_load').fadeOut(1000, function(){
			if ($('#map').length > 0){initMap();}
			if ($('#destinations-map').length > 0){initMap2();}
			if ($('#road-map').length > 0){
			 initMap3();
			}
			
			wow.init();
		});

		$('#page-content').addClass('loaded');
	}
}

$(document).ready(function (){
	//overlay interval
 	interval = 1;    //

	
	
    // 4. dinamically add background from html
	$.each($('[data-bg]'), function(){
	    if ($(this).attr('data-bg').length > 0){
	      $(this).css('background-image', 'url('+ $(this).attr('data-bg') +')');
	    }
	});

	// 5. move about block
	$('.play-btn').on('click', function(){
		var $this = $(this);
		$('.about-description, .about-foundation').toggleClass('move');
		$('.about-video-bg').toggleClass('video-play');
		$('#about-play').toggleClass('active');
		$('#about-stop').toggleClass('active');
	});

	// 6.arrow up && down functions
	$(window).on('scroll', function() {    
	    var scroll = $(window).scrollTop() + $(window).height();
	    var header = $('header').offset().top;
	    
	    if (scroll >= 1000) {
	        arrowUp.addClass("visible");
	    }else{
	    	arrowUp.removeClass("visible");
	    };
	    if ($('section.about').length > 0){
		    if (scroll >= $('section.about').offset().top && scroll <= $('section.about').offset().top + $('section.about').height()){
		    	arrowUp.addClass('white');
		    } else{
		    	arrowUp.removeClass('white');
		    }
		}
	    
	});
	arrowUp.on('click', function(){
	    $("html, body").animate({ scrollTop: 0 }, 600);
	    return false;
	});
	arrowDown.on('click', function() {
	    $('html, body').animate({
	        scrollTop: $(".top-destination").offset().top
	    }, 600);
	});

	// 7. Tour page top EYE hover functions

	$('#hoverEye').on('mouseenter', function(){
		$('#tour-header').addClass('hovered');
	}).on('mouseleave', function(){
		$('#tour-header').removeClass('hovered');
	});

	//  8. Gallery lightbox - magnific-popup
	$('.gallery-photos').magnificPopup({
	  delegate: 'a', 
	  type: 'image',
	  gallery:{
	    enabled:true
	  },
	  arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
	  tPrev: '<i class="fa fa-angle-left"></i>', // title for left button
  	  tNext: '<i class="fa fa-angle-right"></i>', // title for right button

	});

	// 9. Second navigation smooth scrool to div 
	$('#second-nav a[href*=#]').on('click', function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
	    && location.hostname == this.hostname) {
	      var $target = $(this.hash);
	      $target = $target.length && $target
	      || $('[name=' + this.hash.slice(1) +']');
	      if ($target.length) {
	        var targetOffset = $target.offset().top - 80;
	        $('html,body')
	        .animate({scrollTop: targetOffset}, 1000);
	       return false;
	      }
	    }
	});

	// 10. Navbar fixed position
	$(window).on('scroll', function() {    
    		// fixed to top navbar
            var scroll = $(window).scrollTop();
    	    if (scroll >= 50) {
    	       	navbar.addClass("navbar-fixed-top");
    	    }else{
    	    	navbar.removeClass("navbar-fixed-top");
    	    }
	});

	$('[href="#day2-tab"]').on('click', function(){
		if (typeof map2 == "undefined") {
			setTimeout(function(){
				initMap4();	
			},10);
		}
	});
	$('[href="#day3-tab"]').on('click', function(){
		if (typeof map3 == "undefined") {
			setTimeout(function(){
				initMap5();	
			},10);
		}
	});
var player;
	//  11. Play youtube video
	$('#play-video').on('click', function(ev) {
 		$('.video-overlay').fadeOut('fast');
	    $("#video-post")[0].src += "&autoplay=1";
	    ev.preventDefault();
	});
	if($('.frow,.lefty,.mydiv').length >0) {
        $.each( $('.navbar-toggle >span'),function(){
         	if(!$(this).hasClass("sr-only") &&!$(this).hasClass("icon-bar")) {$(this).remove();}
        });
        $('#page-content').parent().find("p").css('margin', '0');     
    }

});

// GOOGLE MAPS
 // 1. First Page
    function initMap() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 3,
            scrollwheel: false,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(38.119962, -8.474970), // map center
            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles:
            [
              {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#ededed"
                    },
                    {
                    "lightness": 17
                    }
                  ]
              },
              {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#f7f7f7"
                    },
                    {
                    "lightness": 20
                    }
                  ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                    "color": "#ffffff"
                    },
                    {
                    "lightness": 17
                    }
                  ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                    "color": "#ffffff"
                    },
                    {
                    "lightness": 29
                    },
                    {
                    "weight": 0.2
                    }
                  ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#ffffff"
                    },
                    {
                    "lightness": 18
                    }
                  ]
              },
              {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#ffffff"
                    },
                    {
                    "lightness": 16
                    }
                  ]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#f5f5f5"
                    },
                    {
                    "lightness": 21
                    }
                  ]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#dedede"
                    },
                    {
                    "lightness": 21
                    }
                  ]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                    "visibility": "on"
                    },
                    {
                    "color": "#ffffff"
                    },
                    {
                    "lightness": 16
                    }
                  ]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                    "saturation": 36
                    },
                    {
                    "color": "#333333"
                    },
                    {
                    "lightness": 40
                    }
                  ]
              },
              {
                "elementType": "labels.icon",
                "stylers": [
                    {
                    "visibility": "off"
                    }
                  ]
                },
              {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                    "color": "#f2f2f2"
                    },
                    {
                    "lightness": 19
                    }
                  ]
              },
              {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                    "color": "#fefefe"
                    },
                    {
                    "lightness": 20
                    }
                  ]
              },
              {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                    "color": "#fefefe"
                    },
                    {
                    "lightness": 17
                    },
                    {
                    "weight": 1.2
                    }
                  ]
              }
            ]
          };
          // Get the HTML DOM element that will contain your map
          // We are using a div with id="map" seen below in the <body>
          var mapElement = document.getElementById('map');
          // Create the Google Map using our element and options defined above
          var map = new google.maps.Map(mapElement, mapOptions);
          // Let's also add a marker while we're at it
          var image = 'images/map-locator.png';
          var marker = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(54.294999, -114.003350),
            map: map,
            title: 'Snazzy!'
          });
          var marker2 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(48.701743, -80.028421),
            map: map,
            title: 'Snazzy!'
          });
          var marker3 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(2.204706, -70.972128),
            map: map,
            title: 'Snazzy!'
          });
          var marker4 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(59.394203, 38.281068),
            map: map,
            title: 'Snazzy!'
          });
    };

 // 2. Destinations.html
    function initMap2() {
          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
          // How zoomed in you want the map to start at (always required)
          zoom: 3,
          scrollwheel: false,
          zoomControl: true,
          mapTypeControl: false,
          scaleControl: false,
          streetViewControl: false,
          // The latitude and longitude to center the map (always required)
          center: new google.maps.LatLng(38.119962, -8.474970), // map center
          // How you would like to style the map.
          // This is where you would paste any style found on Snazzy Maps.
          styles:
          [
	            {
	            "featureType": "water",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#ededed"
	                },
	                {
	                "lightness": 17
	                }
	              ]
	            },
	            {
	            "featureType": "landscape",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#f7f7f7"
	                },
	                {
	                "lightness": 20
	                }
	              ]
	            },
	            {
	            "featureType": "road.highway",
	            "elementType": "geometry.fill",
	            "stylers": [
	                {
	                "color": "#ffffff"
	                },
	                {
	                "lightness": 17
	                }
	              ]
	            },
	            {
	            "featureType": "road.highway",
	            "elementType": "geometry.stroke",
	            "stylers": [
	                {
	                "color": "#ffffff"
	                },
	                {
	                "lightness": 29
	                },
	                {
	                "weight": 0.2
	                }
	              ]
	            },
	            {
	            "featureType": "road.arterial",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#ffffff"
	                },
	                {
	                "lightness": 18
	                }
	              ]
	            },
	            {
	            "featureType": "road.local",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#ffffff"
	                },
	                {
	                "lightness": 16
	                }
	              ]
	            },
	            {
	            "featureType": "poi",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#f5f5f5"
	                },
	                {
	                "lightness": 21
	                }
	              ]
	            },
	            {
	            "featureType": "poi.park",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#dedede"
	                },
	                {
	                "lightness": 21
	                }
	              ]
	            },
	            {
	            "elementType": "labels.text.stroke",
	            "stylers": [
	                {
	                "visibility": "on"
	                },
	                {
	                "color": "#ffffff"
	                },
	                {
	                "lightness": 16
	                }
	              ]
	            },
	            {
	            "elementType": "labels.text.fill",
	            "stylers": [
	                {
	                "saturation": 36
	                },
	                {
	                "color": "#333333"
	                },
	                {
	                "lightness": 40
	                }
	              ]
	            },
	            {
	            "elementType": "labels.icon",
	            "stylers": [
	                {
	                "visibility": "off"
	                }
	              ]
	            },
	            {
	            "featureType": "transit",
	            "elementType": "geometry",
	            "stylers": [
	                {
	                "color": "#f2f2f2"
	                },
	                {
	                "lightness": 19
	                }
	              ]
	            },
	            {
	            "featureType": "administrative",
	            "elementType": "geometry.fill",
	            "stylers": [
	                {
	                "color": "#fefefe"
	                },
	                {
	                "lightness": 20
	                }
	              ]
	            },
	            {
	            "featureType": "administrative",
	            "elementType": "geometry.stroke",
	            "stylers": [
	                {
	                "color": "#fefefe"
	                },
	                {
	                "lightness": 17
	                },
	                {
	                "weight": 1.2
	                }
	              ]
	            }
          ]
    };
       
    	var mapElement = document.getElementById('destinations-map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var image = 'images/map-locator.png';
        var marker = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(47.014646, 28.874796),
            map: map,
            title: 'My Location!'
		});
		var marker2 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(38.798619, -3.345152),
            map: map,
            title: 'My Location!'
        });
        var marker3 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(27.344445, -2.466246),
            map: map,
            title: 'My Location!'
        });
        var marker4 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(59.518716, 8.432191),
            map: map,
            title: 'My Location!'
        });
        var marker5 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(35.435609, -76.821709),
            map: map,
            title: 'My Location!'
        });
        var marker6 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(-2.842097, -67.329522),
            map: map,
            title: 'My Location!'
        });
        var marker7 = new google.maps.Marker({
            icon: image,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(22.517969, -80.682138),
            map: map,
            title: 'My Location!'
        });
    };
  
  // 3. Tour-page.html
