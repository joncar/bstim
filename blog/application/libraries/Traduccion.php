<?php

class Traduccion{
    public $es = array(
        "Notícies"=>"Noticias",
        "Estigues al dia, informa't de totes les novetats de BSTIM."=>"Noticias del dia",        
    );
    
    public $en = array(
        "Notícies"=>"News",
        "Estigues al dia, informa't de totes les novetats de BSTIM."=>"News of day",
    );

    public function traducir($view,$idioma = ''){
        if($idioma!='ca'){
            foreach($this->$idioma as $n=>$v){
                $view = str_replace($n,$v,$view);
            }
            return $view;
        }else{
            return $view;
        }
    }
}    
