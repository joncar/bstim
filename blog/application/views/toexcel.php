<div class="blog-page loaded" id="page-content"> <!-- blog page header with video bg --> 
        <header data-bg="<?= base_url() ?>images/blog-page-bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/blog-page-bg.jpg);">
           
            <div class="header-center-content"> 
                <div class="container text-center"> 
                    <div class="row section-intro animated fadeInUpBig"> 
                        <div class="col-md-12"> 
                            
                            <h1 class="text-uppercase" <p style="letter-spacing: 10px; ">EXCELS</h1> 
                            <p style="margin: 0px; "> 
                                Estigues al dia, informa't de totes les novetats de BSTIM.
                            </p>
                        </div> 
                    </div> 
                </div> 
            </div> 
        </header> <!-- /.blog page header with video bg --> <!-- main content --> 
        <main> 
        <div class="blog-page-wrapp"> 
            <div class="container"> 
                <a href="<?= base_url('main/toExcel/Estudiantes') ?>"> Exportar Estudiantes </a>
                <a href="<?= base_url('main/toExcel/Empresa') ?>"> Exportar Empresas </a>
                <a href="<?= base_url('main/toExcel/Expositors') ?>"> Exportar Expositors </a>
            </div> 
        </div>       
        </main>
</div>