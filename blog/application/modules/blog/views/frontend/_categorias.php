<aside class="col-md-3 text-uppercase sidebar"> 
    <div class="categories animated slideInLeft">                                 
        <h6>
            Categories 
            <span><?= $categorias->num_rows() ?></span>
        </h6> 
        <ul class="list-unstyled"> 
            <?php if($categorias->num_rows()==0): ?>
            <li>
                <a href="#">Sin categorías</a>
            </li>
            <?php endif ?>
            <?php foreach($categorias->result() as $c): ?>
            <li> 
                <a href="javascript:changeCategoria(<?= $c->id ?>)"> 
                    <span><?= $c->blog_categorias_nombre ?></span>
                    <b class="pull-right"><?= $c->cantidad ?></b> 
                </a>
            </li> 
            <?php endforeach ?>
        </ul> 
    </div>
    <form id="searchForm" data-wow-delay="0.2s" action="<?= base_url() ?>"> 
        <input type="search" placeholder="Search..." name="direccion"> 
        <input type="hidden" id="page" name="page" value="<?= !empty($_GET['page'])?$_GET['page']:'1' ?>">
        <input type="hidden" id="blog_categorias_id" name="blog_categorias_id" value="<?= !empty($_GET['blog_categorias_id'])?$_GET['blog_categorias_id']:'' ?>">
        <button type="submit"> <i class="fa fa-search"></i> </button> 
    </form>
    <div class="sidebar-tags wow slideInLeft"> 
        <h6>Tags</h6> 
        <ul class="list-inline">
            <?php if(!empty($detail->tags)): ?>
                <?php foreach(explode(',',$detail->tags) as $t): ?>
                    <li>
                            <a href="<?= base_url().'?direccion='.$t ?>"><?= $t ?></a>
                    </li>
                <?php endforeach ?>            
            <?php endif ?>
        </ul>
    </div>
</aside> <!-- /.sidebar --> <!-- blog posts intro list --> 
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchForm").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchForm").submit();
    }
</script>