<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function paginas(){
            $crud = $this->crud_function('','');
            $crud->columns('titulo');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function banner(){
            $crud = $this->crud_function('','');            
            $crud->columns('foto');
            if($crud->getParameters()!='list'){
                $crud->field_type('foto','image',array('width'=>'600px','height'=>'300px','path'=>'img/banner'));
            }else{
                $crud->set_field_upload('foto','img/banner');
            }
            $crud->field_type('posicion','dropdown',
                    array(
                        'top:50px; left:0px;'=>'Izquierda - Arriba',
                        'top:160px; left:0px;'=>'Izquierda - Centro',
                        'bottom:0px; left:0px;'=>'Izquierda - Abajo',                        
                        'top:50px; left:30%;'=>'Centro - Arriba',
                        'top:160px; left:30%;'=>'Centro - Centro',
                        'bottom:0px; left:30%;'=>'Centro - Abajo',                        
                        'top:50px; right:0; left:initial;'=>'Derecha - Arriba',
                        'top:160px; right:0; left:initial;'=>'Derecha - Centro',
                        'bottom:0px; right:0; left:initial;'=>'Derecha - Abajo'
                    ));
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
