<li data-wow-duration="1s" data-wow-delay="0.1s" class="animated fadeInUp"> 
    <a href="<?= site_url('hoteles/'.toURL($detail->id.'-'.$detail->nombre)) ?>"> 
        <img class="img-responsive" alt="adventure-image" src="<?= base_url('images/hoteles/'.$detail->portada) ?>" style="max-width:270px; max-height:215px;"> 
        <div class="overlay-lnk text-uppercase text-center"> 
            <i class="icon icon-wine"></i> <h5><?= $detail->nombre ?></h5>
        </div> 
    </a>
    <p class="lefty" style="margin: 0px;">dissonancy</p>
    <span class="libeled">interantagonism bathymetry</span> 
</li>