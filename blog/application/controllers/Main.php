<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('traduccion');
        date_default_timezone_set('America/Asuncion');
        if(!isset($_SESSION['lang'])){
            $_SESSION['lang'] = 'ca';
        }
    }
    
    function get_categorias(){
        $this->db->limit('8');
        $categorias = $this->db->get_where('blog_categorias',array('idioma'=>$_SESSION['lang']));
        foreach($categorias->result() as $n=>$c){
            $categorias->row($n)->cantidad = $this->db->get_where('blog',array('blog_categorias_id'=>$c->id))->num_rows();
        }
        return $categorias;
    }

    public function index() {
        $blog = new Bdsource();        
        $blog->order_by = array('fecha','DESC');
        if(!empty($_GET['direccion'])){
            $blog->where('titulo like "%'.trim($_GET['direccion']).'%" OR texto like "%'.trim($_GET['direccion']).'%"','',FALSE);
            //$blog->like('titulo',$_GET['direccion']);
        }
        if(!empty($_GET['blog_categorias_id'])){
            $blog->where('blog_categorias_id',$_GET['blog_categorias_id']);
        }
        $blog->where('idioma',$_SESSION['lang']);
        if(!empty($_GET['page'])){
            $blog->limit = array('12',($_GET['page']-1)*12);
        }else{
            $blog->limit = array(12,0);
            $_GET['page'] = 1;
        }
        $blog->init('blog');
        $registros = $this->db->get_where('blog',array('idioma'=>$_SESSION['lang']))->num_rows();        
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows;                
        }
        if($this->blog->num_rows>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
        $paginacion = array(
            'actual'=>$_GET['page'],
            'total'=>$registros,
            'pages'=>2,
            'url'=>'javascript:changePage(\'{{page}}\')',
            'rows'=>12
        );
        $this->loadView(
                array(
                    'view'=>'main',
                    'detail'=>$this->blog,
                    'title'=>'Blog',
                    'categorias'=>$this->get_categorias(),
                    'paginacion'=>$paginacion
                ));
    }
    
    public function traduccion($idioma = 'ca'){
        $_SESSION['lang'] = $idioma;
        if(empty($_GET['url'])){
            redirect(base_url());
        }else{
            redirect($_GET['url']);
        }
    }
    
    function unsubscribe(){
        if(empty($_POST)){
            $this->loadView('frontend/unsubscribe');
        }else{
            $emails = $this->db->get_where('emails',array('email'=>$_POST['email']));
            $success = $emails->num_rows()>0?TRUE:FALSE;
            if($success){
                $this->db->delete('emails',array('email'=>$_POST['email']));
                $this->db->insert('emails_eliminados',array('email'=>$emails->row()->email,'nombre'=>$emails->row()->nombre));
            }            
            $this->loadView(array('view'=>'frontend/unsubscribe','success'=>$success));
        }
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url());
        } else
            header("Location:" . base_url());
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if(is_string($param)){
            $param = array('view'=>$param);
        }
        $view = $this->load->view('template',$param,TRUE);            
        echo $this->traduccion->traducir($view,$_SESSION['lang']);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        
        $post = json_decode($_POST['json'],TRUE);
        if($this->db->get_where('acreditaciones',array('email'=>$post['Email'],'tipo'=>$_POST['tipo']))->num_rows()==0){
            $this->db->insert('acreditaciones',array('email'=>$post['Email'],'json'=>$_POST['json'],'tipo'=>$_POST['tipo']));
        }
            
        $notificacion = $this->db->get_where('notificaciones',array('id'=>$_POST['id']));
        if($notificacion->num_rows()>0){
            $post = json_decode($_POST['json'],TRUE);
            $titulo = $notificacion->row()->titulo;
            $texto = $notificacion->row()->texto;
            foreach($post as $n=>$v){
                $texto = str_replace('{'.$n.'}',$v,$texto);
                $titulo = str_replace('{'.$n.'}',$v,$titulo);
            }            
            $this->mailer->mail($post['Email'],$titulo,$texto);
        }        
    }
    
    function toExcel($tipo = 'Todos'){
        if(!empty($tipo)){
            $labels = array(
                "custom_U87734"=>"Nombre",
                "custom_U50857"=>"Nombre",
                "custom_U5497"=>"Nombre",
                "custom_U86869"=>"Nombre",
                "custom_U87749"=>"Telefono",
                "custom_U86873"=>"Telefono",
                "custom_U50872"=>"Telefono",
                "custom_U5734"=>"Telefono",
                "Email"=>"Email",
            );
            $string_to_export = "";
            $correos = array();
            if($tipo!='Todos'){
                $this->db->where('tipo',$tipo);
            }
            //$this->db->limit(1);
            foreach($this->db->get_where('acreditaciones')->result() as $e){
                $var = json_decode($e->json,TRUE);                
                $var['Tipo'] = $e->tipo;
                $correos[] = $var;
            }                
            //Cabecera
            if(count($correos)>0){                
                foreach($correos[0] as $n=>$c){
                    if(isset($labels[$n])){
                        $string_to_export .= $labels[$n]."\t";
                        //$string_to_export .= $labels[$n];
                    }else{
                        $string_to_export .= $n."\t";
                    }
                }
                $string_to_export .= "Tipo\t";
                $string_to_export .= "\n";

                foreach($correos as $c){                    
                    foreach($c as $n=>$c1){
                        //if(isset($labels[$n])){
                            $string_to_export .= $c1."\t";
                            //$string_to_export .= $labels[$n];
                        //}
                    }
                    $string_to_export .= $c['Tipo']."\t";
                    $string_to_export .= "\n";
                }
            }
            //echo $string_to_export;

            // Convert to UTF-16LE and Prepend BOM
            $string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-8', 'UTF-8');
            $filename = "export-".date("Y-m-d_H:i:s").".xls";
            header('Content-type: application/vnd.ms-excel;charset=UTF-8');
            header('Content-Disposition: attachment; filename='.$filename);
            header("Cache-Control: no-cache");
            echo $string_to_export;
            die();
        }else{
            $this->loadView('toexcel');
        }
    }

}
