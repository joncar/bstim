<div class="contact-page loaded" id="page-content"> 
    <header class="overlay"> <!-- navigation / main menu --> 
        <?= $this->load->view('includes/template/menu2') ?>
    </header> <!-- main content --> 
    <main> 
        <section data-bg="<?= base_url() ?>images/mountains_bg.jpg" class="contacts-section" style="background-image: url(<?= base_url() ?>images/mountains_bg.jpg);"> 
            <div class="container"> 
                <div class="row text-center"> 
                    <div class="col-md-12"> 
                        <div data-wow-duration="1s" data-wow-delay="0.1s" class="user-avatar animated slideInDown"> 
                            <img class="img-responsive" alt="user-avatar" src="<?= base_url() ?>images/avatar.jpeg"> 
                        </div> 
                        <h1 class="text-uppercase">HABLEMOS!
                        </h1> 
                    </div> 
                </div> 
                <div class="row text-center"> 
                    <div class="col-md-12"> 
                        <p style="margin: 0px;"> 
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                        </p>
                        <p class="divc" style="margin: 0px;">agitable</p>
                        <span class="lehi">noninflammable preeliminate</span>
                        <span class="sown">megalomania</span>
                    </div> 
                </div> 
                <div class="row text-center">
                    <ul class="col-md-12 text-uppercase list-inline contacts-list"> 
                        <li data-wow-duration="1s" data-wow-delay="0.1s" class="animated slideInUp"> 
                            <a href="#"> <i class="icon icon-facebook"></i> 
                                <span>Facebook</span>
                                <span class="coma">inimical</span>
                            </a>
                            <p class="mydiv" style="margin: 0px;">remodifying</p>
                            <span class="classy">hyperelegance americium</span> 
                        </li> 
                        <li data-wow-duration="1s" data-wow-delay="0.3s" class="animated slideInUp"> 
                            <a href="tel:123-456-003"> <i class="icon icon-phone"></i> <span>+123 345908</span>
                                <span class="mydiv">autointoxication</span> 
                            </a>
                            <p class="lehi" style="margin: 0px;">conclusiveness</p>
                            <span class="frow">nonallegation undermeasured</span> 
                        </li> 
                        <li data-wow-duration="1s" data-wow-delay="0.5s" class="animated slideInUp"> 
                            <a href="mailto:hi@themesdojo.com"> <i class="icon icon-envelope"></i> 
                                <span>info@miex.me</span><span class="vice">philadelphia</span> 
                            </a>
                            <p class="libeled" style="margin: 0px;">reapplying</p>
                            <span class="kiwi">apsarases hypognathism</span> 
                        </li> 
                    </ul> 
                </div> <!-- section-intro --> 
                <div class="row text-center section-intro"> 
                    <div class="col-md-offset-2 col-md-8"> 
                        <i data-wow-delay="0.2s" class="icon icon-envelope animated fadeInUp"></i>
                        <h1 data-wow-delay="0.4s" class="text-uppercase animated fadeInUp">FORMULARIO DE CONTACTO</h1> 
                    </div> 
                </div> <!-- /.section-intro --> 
                <div class="row"> 
                    <form class="contact-form"> 
                        <div class="col-md-4"> <div class="form-group"> 
                                <input type="text" placeholder="Nombre" id="userName" class="form-control"> 
                            </div> 
                            <div class="form-group"> 
                                <input type="email" placeholder="Email" id="userEmail" class="form-control"> 
                            </div> 
                            <div class="form-group"> 
                                <input type="text" placeholder="Teléfono" id="userPhone" class="form-control"> 
                            </div> 
                        </div> 
                        <div class="col-md-8">
                            <div class="form-group"> 
                                        <textarea class="form-control" placeholder="Tu mensaje" id="userMessage"></textarea> 
                            </div> 
                            <button class="text-uppercase" type="submit">ENVIAR MENSAJE</button> 
                        </div> 
                    </form> 
                </div> 
            </div> 
        </section> <!-- go up arrow --> 
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> <span>Go Up</span><span class="mydiv">variolitic</span> 
        </button> <!-- /.go up arrow --> 

    </main>
    <?= $this->load->view('includes/template/footer'); ?>
</div>