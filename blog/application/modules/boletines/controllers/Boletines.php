<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Boletines extends Panel{
        function __construct() {
            parent::__construct();

        }
        function boletin(){
            $this->norequireds = array('emails');
            $crud = $this->crud_function('','');
            $emails = array();
            foreach($this->db->get('emails')->result() as $e){
                $emails[] = $e->email;
            }
            
            $crud->set_field_upload('banner','images/boletines');
            $crud->unset_columns('contenido');
            $crud->field_type('emails','set',$emails);
            $crud->add_action('<i class="fa fa-envelope"></i> Añadir Cuerpo','',base_url('boletines/boletin_detalles').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Ver Boletín','',base_url('boletines/ver').'/');
            $crud->add_action('<i class="fa fa-envelope"></i> Enviar Boletín','',base_url('boletines/send').'/');
            $action = $crud->getParameters();            
            $crud = $crud->render();
                        
            $this->loadView($crud);
        }   
        
        function excel($x = '',$y = ''){
            if($x=='procesar'){
                $file = $this->db->get_where('excel',array('id'=>$y))->row()->fichero;
                require_once 'application/libraries/Excel/SpreadsheetReader.php';
                $excel = new SpreadsheetReader('files/'.$file);
                $data = array();
                foreach ($excel as $Row)
                {
                    $data[] = $Row;
                }
                //Empresa = 0, email = 4
                //ob_start();
                if(count($data)>2){
                    //$this->db->truncate('emails');
                    $this->db->query('truncate emails');
                    for($i=2;$i<count($data);$i++){
                        if(!empty($data[$i][0])){
                            $this->db->insert('emails',array('nombre'=>$data[$i][0],'email'=>$data[$i][0]));
                            echo 'Incluido '.$data[$i][0].'<br/>';
                            ob_flush();
                        }
                    }
                }else{
                    echo "Formato incorrecto";
                }
            }else{
                $crud = $this->crud_function('','');
                $crud->set_field_upload('fichero','files');
                $crud->add_action('<i class="fa fa-refresh"></i> Procesar','',base_url('boletines/excel/procesar').'/');
                $crud = $crud->render();

                $this->loadView($crud);
            }
        }   
        
        function boletin_detalles($x = ''){
            $crud = $this->crud_function('','');
            if(is_numeric($x)){
                $crud->field_type('boletin_id','hidden',$x);
                $crud->unset_columns('boletin_id','texto');
            }
            $crud->set_field_upload('foto','images/boletines');
            $crud = $crud->render();                        
            $this->loadView($crud);
        }
        
        function emails(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();            
            $this->loadView($crud);
        } 
        
        function emails_eliminados(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();            
            $this->loadView($crud);
        } 
        
        function ver($id = ''){
            if(is_numeric($id)){
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = empty($detail->contenido)?'boletin':'boletinr';
                $this->load->view('frontend/'.$view,array('detail'=>$detail));                
            }                        
        }
        
        function send($id = ''){
            if(is_numeric($id)){
                $this->db->query("SET NAMES 'utf8'");
                $detail = $this->db->get_where('boletin',array('id'=>$id))->row();
                $detail->cuerpo = $this->db->get_where('boletin_detalles',array('boletin_id'=>$detail->id));
                $view = empty($detail->contenido)?'boletin':'boletinr';
                $correo = $this->load->view('frontend/'.$view,array('detail'=>$detail),TRUE);
                //$correo = $correo;
                //echo $correo;       
                ob_end_flush();
                ob_start();
                set_time_limit(0);
                if(empty($detail->emails)){
                    foreach($this->db->get('emails')->result() as $e){
                        correo($e->email,$detail->titulo,  $correo);
                        echo "Enviado a ".$e->email.'<br/>';
                        ob_flush();
                        flush();
                    }
                }else{
                    foreach(explode(',',$detail->emails) as $e){
                        correo($e,$detail->titulo,$correo);
                    }
                }
                echo "Correo enviado a todos los contactos registrados en la tabla emails <a href='".base_url('boletines/boletin')."'>Volver a la lista</a>";
            }                        
        }
        
        function getLastInput(){
            $this->db->order_by('id','DESC');
            $this->db->limit('1');
            $last = $this->db->get_where('boletin');
            if($last->num_rows()>0){
                echo $last->row()->texto;
            }
        }
        
        function notificaciones(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();            
            $this->loadView($crud);
        }
    }
?>
