<footer class="footer">
    <div class="container">
        <div class="row overflow">
            <div style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeInUp;" class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1s">
                <h4 class="text-capitalize footer-col-title">Últimos artículos</h4>
                <ul class="list-unstyled text-uppercase footer-blog-intro">
                    <li><a href="#">How to get used to higher altitudes</a>
                        <p style="margin: 0px;" class="coma">matteotti</p>
                        <span class="mydiv">biedermeier recancellation</span></li>
                    <li><a href="#">finding perfect spots where you don’t need a fortune to get there</a>
                        <p style="margin: 0px;" class="lefty">megalomania</p>
                        <span class="lehi">matteotti hypognathism</span></li>
                    <li><a href="#">Lorem ipsum dolor sit amet</a>
                        <p style="margin: 0px;" class="kiwi">pseudopoetical</p>
                        <span class="lefty">americium bathymetry</span></li>
                </ul>
            </div>
            <div style="visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: fadeInUp;" class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s" data-wow-duration="1s">
                <h4 class="text-capitalize footer-col-title">Newsletter</h4>
                <p style="margin: 0px;" class="subscribe-text">Sea uno de los primeros en enterarte de nuestro viaje impresionante</p>
                <p style="margin: 0px;" class="bura">nonglobular</p>
                <span class="poll">unengineered zygotically</span> <span class="kiwi">vectorially</span><form id="subscribeForm" class="form"><input type="email" name="emailInput" placeholder="Tu email..."> <input type="submit" name="submit" value="Subcribete"></form>
                <ul class="list-inline social-links">
                    <li><a href="#" class="fb-lnk"> <i class="fa fa-facebook-official"></i> </a>
                        <p style="margin: 0px;" class="sown">nonunanimous</p>
                        <span class="sown"> isohyetal autointoxication </span></li>
                    <li><a href="#" class="tw-lnk"> <i class="fa fa-twitter-square"></i> </a>
                        <p style="margin: 0px;" class="sown">endoplasm</p>
                        <span class="frow"> mischaracterize estimably </span></li>
                    <li><a href="#" class="inst-lnk"> <i class="fa fa-instagram"></i> </a>
                        <p style="margin: 0px;" class="vice">rationalizing</p>
                        <span class="poll"> electively bathymetry </span></li>
                    <li><a href="#" class="vim-lnk"> <i class="fa fa-vimeo-square"></i> </a>
                        <p style="margin: 0px;" class="lehi">bathymetry</p>
                        <span class="fitz">zygotically ariminum</span></li>
                    <li><a href="#" class="tub-lnk"> <i class="fa fa-youtube-square"></i> </a>
                        <p style="margin: 0px;" class="kiwi">pyophthalmitis</p>
                        <span class="lehi">monotrichic unmutated</span></li>
                </ul>
            </div>
            <div style="visibility: visible; animation-duration: 1s; animation-delay: 0.4s; animation-name: fadeInUp;" class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="1s">
                <h4 class="text footer-col-title">TELEFONO EXCLUSIVO PADRES</h4>
                <div class="footer-about">
                    <p style="margin: 0px;"> Horario de Atención al Cliente: </br> lunes a viernes de 10:00 a 13:30:00h. </br>y de 17:00 a 20:00 horas.</p>
                    <p style="margin: 0px;" class="classy">detribalise</p>
                    <span class="libeled">hospitaler overspicing</span> <span class="frow">annexationism</span>
                    <h4 class="text footer-col-title">DIRECCION</h4>
C/ Condes de Andrade nº 1 local 1</br>(entrada por el nº100 de Avda. de Vilaboa), CULLEREDO, A Coruña. C.P.15174</p>
                    <p style="margin: 0px;" class="classy">exegetics</p>
                    <span class="vice">nonallegation admiringly</span> <span class="sown">unfugitive</span></div>
            </div>
            <div style="visibility: visible; animation-duration: 1s; animation-delay: 0.5s; animation-name: fadeInUp;" class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1s">
                <h4 class="text-capitalize footer-col-title">links</h4>
                <ul class="list-unstyled text-uppercase footer-links">
                    <li><a href="#">quienes somos</a>
                        <p style="margin: 0px;" class="libeled">aluminosilicate</p>
                        <span class="divc">aluminosilicate mischaracterize</span></li>
                    <li><a href="#">Hoteles</a>
                        <p style="margin: 0px;" class="bura">aetiology</p>
                        <span class="poll">unexcogitative superrefined</span></li>
                    <li><a href="#">Sobre MIEX</a>
                        <p style="margin: 0px;" class="poll">annotator</p>
                        <span class="sown">americium unsegregated</span></li>
                    <li><a href="#">Blog</a>
                        <p style="margin: 0px;" class="lefty">stupefacient</p>
                        <span class="bura">semipractical exudative</span></li>
                    <li><a href="#">Contacto</a>
                        <p style="margin: 0px;" class="vice">overdiscouraging</p>
                        <span class="kiwi">noninflammable monogyny</span></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center text-uppercase">
                <div class="copyright"><small><small>Made in BCN with love <a href="http://" target="_blank"></a></small></small>
                    <p style="margin: 0px;" class="classy">superinfirmity</p>
                    <small><span class="mydiv">ornamental nontheocratical</span> </small></div>
            </div>
        </div>
    </div>
</footer>
<!-- plugins we use --> 
<script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflh4Q3pb/www-widgetapi.js" async=""></script>
<script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflwK39-Z/www-widgetapi.js" async=""></script>
<script src="https://www.youtube.com/iframe_api"></script>
<script src="<?= base_url() ?>js/functions.js"></script> <!-- snazzy maps -->
   <script> 
       var tag = document.createElement('script'); 
       tag.src = "https://www.youtube.com/iframe_api"; 
       var firstScriptTag = document.getElementsByTagName('script')[0]; 
       firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
       var player; 
       function onYouTubeIframeAPIReady() { 
           player = new YT.Player('player', 
           { height: '390', 
               width: '640', 
               videoId: 'f64cz6C4N74',
               rel: 0, 
               events: { 'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange } 
           }
                   ); 
       } 
       function onPlayerReady(event) { 
           event.target.playVideo();
       } 
       var done = false; 
       function onPlayerStateChange(event) { 
           if (event.data == YT.PlayerState.PLAYING && !done) {
               setTimeout(stopVideo, 0); 
               done = true; 
          }
       }
       function
       stopVideo() 
       { 
           player.stopVideo(); 
       } 
       $('#about-play').on('click', function(){ 
           player.playVideo(); 
           player.setPlaybackQuality('hd1080'); 
       }); 
       $('#about-stop').on('click', function(){ 
           player.pauseVideo() 
       }); 
    </script> <!-- initializing functions --> 
<script src="<?= base_url() ?>js/main.js"></script>