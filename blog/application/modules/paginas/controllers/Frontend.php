<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }        
        
        function read($url){            
            $this->loadView(array('view'=>'read','page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$url))));
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
    }
?>
