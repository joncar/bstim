<header class="overlay"> <!-- navigation / main menu --> 
	    <?= $this->load->view('includes/template/menu'); ?>
	    <div class="video_bg"> 
	        <video preload="metadata" id="my-video" poster="images/bg1.jpg" muted="" loop="" autoplay=""> <!-- video source --> 
	            <source type="video/mp4" src="video/video_bg.mp4"></source> 
	            <source type="video/ogv" src="video/video_bg.ogv"></source> 
	        </video> 
	    </div>
	    <div class="header-center-content"> 
	        <div class="container text-center"> 
	            <div class="row"> 
	                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
	                    <span class="logo text-uppercase">estd 
	                        <i class="icon icon-hotairballoon"></i> 1987
	                    </span>
	                    <span class="lefty">posthemiplegic</span> 
	                    <h1 class="text-uppercase">Explore adventures</h1> 
	                    <h4>Select you type of adventure and an exotic destination. And then - let’s go for it!</h4> 
	                </div> 
	            </div> 
	            <div class="row"> 
	                <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 select-options">   
	                    <button class="btn search-btn animated fadeInUp"> <i class="fa fa-search"></i> </button>
	                    <select class="cs-select cs-skin-elastic experience-select animated fadeInUp" id="experience"> 
	                        <option selected="" disabled="" value="0">Ubicación</option> 
                                <?php foreach($this->db->get_where('categorias_hoteles')->result() as $c): ?>
                                    <option value="<?= $c->id ?>"><?= $c->categorias_hoteles_nombre ?></option>
                                <?php endforeach ?>
	                    </select>
	                    <select class="cs-select cs-skin-elastic destination-select animated fadeInUp" id="destionation"> 
	                        <option selected="" disabled="" value="0">Destination</option> 
                                    <option value="1">Option 1</option> 
                                    <option value="2">Option 2</option> 
                                    <option value="3">Option 3</option> 
                                    <option value="4">Option 4</option> 
                                </select>
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <button class="btn scroll-down animated fadeInDown"><i aria-hidden="true" class="fa fa-arrow-circle-down"></i></button> 
            </header> <!-- /.first page header with video bg --> <!-- main content --> 