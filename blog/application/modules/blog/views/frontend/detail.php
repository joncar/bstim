<div class="blog-page loaded" id="page-content"> <!-- blog page header with video bg --> 
    <header data-bg="<?= base_url('img/fotos/'.$detail->foto) ?>" class="overlay" style="background-image: url(<?= base_url('img/fotos/'.$detail->foto) ?>);"> 
        
        <div class="header-center-content"> 
            <div class="container text-center"> 
                <div class="row section-intro animated fadeInUpBig"> 
                    <div class="col-md-12">
                        <h1 class="text-uppercase"><?= $detail->titulo ?></h1>
                        <span class="post-author text-uppercase">por <a href="#"><?= $detail->user ?></a>
                             el <?= date("d-m-Y",strtotime($detail->fecha)) ?>
                        </span> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </header> <!-- /.blog page header with video bg --> <!-- main content --> 
    <main> 
        <div class="blog-post-wrapp"> 
            <div class="container"> 
                <div class="row"> <!-- sidebar --> 
                    <?php $this->load->view('_categorias'); ?>
                    <!-- /.sidebar --> <!-- blog posts intro list --> 
                    <div class="col-md-9"> 
                        <div class="blog-post-content"> 
                            <?= $detail->texto ?>
                        </div>
                        <nav> 
                            <ul class="pagination posts-pagination text-uppercase"> 
                                <?php 
                                    $prev = $this->db->get_where('blog',array('idioma'=>$_SESSION['lang']));
                                    $ide = -1;
                                    $blogs = array();
                                    foreach($prev->result() as $n=>$v){
                                        $blogs[] = $v;
                                        if($v->id==$detail->id){
                                            $ide = $n;
                                        }
                                    }
                                ?>
                                <li class="prev-post pull-left">
                                    <?php if(isset($blogs[($ide-1)])): ?>
                                    <a href="<?= site_url('blog/'.toURL($blogs[$ide-1]->id.'-'.$blogs[$ide-1]->titulo)) ?>" title="<?= $blogs[$ide-1]->titulo ?>"><?= substr($blogs[$ide-1]->titulo,0,30) ?></a>
                                    <?php endif ?>
                                </li>
                                <li class="next-post pull-right">
                                    <?php if(isset($blogs[($ide+1)])): ?>
                                    <a href="<?= site_url('blog/'.toURL($blogs[$ide+1]->id.'-'.$blogs[$ide+1]->titulo)) ?>" title="<?= $blogs[$ide+1]->titulo ?>"><?= substr($blogs[$ide+1]->titulo,0,30) ?></a>
                                    <?php endif ?>
                                </li>
                            </ul>
                        </nav>
                    </div> <!-- /.blog posts intro list --> 
                </div> 
            </div> 
        </div> 
        
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> 
            <span>Go Up</span>
            <span class="fitz">noninfantry</span> 
        </button> <!-- /.go up arrow -->
    </main> <!-- /.main content --> <!-- footer content --> 
</div> 