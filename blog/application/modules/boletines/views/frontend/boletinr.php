<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="ISO-8859-1">
        <style type="text/css" id="Mail Designer General Style Sheet">
            a { word-break: break-word; }
            a img { border:none; }
            img { outline:none; text-decoration:none; -ms-interpolation-mode: bicubic; }
            body { width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family:'Arial' !important;}
            .ExternalClass { width: 100%; }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
            #page-wrap { margin: 0; padding: 0; width: 100% !important;}
            #outlook a { padding: 0; }
            .preheader { display:none !important; }
            a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: 'Arial' !important; font-weight: inherit !important; line-height: inherit !important; }
        </style>
        <style type="text/css" id="Mail Designer Mobile Style Sheet">  
            
        </style>
        <!--[if gte mso 9]>
        <style type="text/css" id="Mail Designer Outlook Style Sheet">
                table.layout-block-horizontal-spacer {
                    display: none !important;
                }
                table {
                    border-collapse:collapse;
                    mso-table-lspace:0pt;
                    mso-table-rspace:0pt;
                    mso-table-bspace:0pt;
                    mso-table-tspace:0pt;
                    mso-padding-alt:0;
                    mso-table-top:0;
                    mso-table-wrap:around;
                }
                td {
                    border-collapse:collapse;
                    mso-cellspacing:0;
                }
        </style>
        <![endif]-->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:700,regular" rel="stylesheet" type="text/css" class="EQWebFont">
        
    </head>
    <body style="margin-top: 10px; margin-right: 0px; margin-bottom: 80px; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 50px; padding-left: 0px; " eqid="EQMST-5B629705-2068-4643-A1B4-1BA752D88F04" background="<?= base_url('images/boletines')?>/page-bg.jpg">
        <center><a href="<?= base_url('boletines/frontend/ver/'.$detail->id) ?>" style="color:#333; font-size: 12px; margin-top: 20px; margin-bottom: 20px">Si no veus bé aquest email fes clik aquí</a></center>
        <!--[if gte mso 9]>
        <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="<?= base_url('images/boletines')?>/page-bg.jpg" />
        </v:background>
        <![endif]-->
        <table id="page-wrap" width="100%" align="center" cellspacing="0" cellpadding="0" background="<?= base_url('images/boletines')?>/page-bg.jpg">
            <tbody>
                <tr>
                    <td>
                        <table class="email-body-wrap" id="email-body" width="726" align="center" cellspacing="0" cellpadding="0" style=" margin-top: 6px">
                            <tbody>
                                <tr>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Left page bg show-thru --></td>
                                    <td id="page-body" eqid="EQMST-E42EE1D2-FD64-4CC3-81A8-CEDDEEA78958" width="666" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/background.jpg">

                                        <!--Begin of layout container -->
                                        <div id="eqLayoutContainer" style="height: auto; ">
                                            
                                            
                                            <div eqid="EQMST-E7DD055F-9768-41B1-B11F-7203C5F91402" width="100%" class="desktop-only">
                                                <table class="mso-fixed-width-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                        
                                                                                    <a href="http://www.bstim.cat">
                                                                                        <img alt="" eqid="EQMST-16ADD1CD-00A6-4052-AB7B-2F0CAB30F889" src="<?= base_url('images/boletines')?>/image-3.png" style="width: 666px; height: 131px; display: block; " width="666" height="131" border="0">
                                                                                    </a>
                                                                                    
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-188CA4C8-0993-41A1-96D4-3F44AF105B12" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 666px; " width="666">
                                                                                <div class="layout-block-image">
                                                                                    <a href="<?= $detail->link_banner ?>">
                                                                                        <img width="666" height="394" alt="" eqid="EQMST-A3DFEC46-51FA-451A-ACDD-D6ABFF7CCE7F" src="<?= base_url('images/boletines/'.$detail->banner)?>" border="0" style="display: block; width: 666px; height: 394px;">
                                                                                    </a>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            
                                            <div style="margin:25px 50px">
                                                <?= $detail->contenido ?>
                                            </div>
                                            <!---------- FIN CONTENIDO ------------>
                                            <div eqid="EQMST-66A68E4D-7B41-487F-833C-BCCA15FB7D50" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-full-width" style="min-width: 666px; " width="666" valign="top">
                                                                <table class="layout-block-full-width" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="min-width: 260px;text-align:center" width="260">
                                                                                
                                                                              </td>
                                                                              <td>
                                                                                <a href="<?= site_url() ?>"><img src="<?= base_url('images/boletines/logo.png' ) ?>"style="margin-top: 30px;margin-bottom:30px " ></a>
                                                                              </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            

                                            

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div eqid="EQMST-F15E35CC-C0E7-4247-8AF5-2F971251ADA5" width="100%" class="desktop-only">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="layout-block-padding-left" style="min-width: 25px; " width="25">&nbsp;</td>
                                                            <td class="layout-block-content-cell" style="min-width: 616px; " width="616" valign="top" align="left">
                                                                <table class="layout-block-column" style="min-width: 616px; " width="616" align="left" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="padding-left: px; padding-right: 10px; min-width: 596px; " eqid="EQMST-6F1F647C-304E-4260-A64E-BFC53CA5C677" width="596" valign="top" align="left" bgcolor="#ffffff" background="<?= base_url('images/boletines')?>/box-bg-2.jpg">
                                                                                <table class="layout-block-box-padding" style="min-width: 596px; " width="596" cellspacing="0" cellpadding="0">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <td class="layout-block-column" style="min-width: 596px; " width="596" align="left">
                                                                                                <div class="heading" eqid="EQMST-3B228E9E-2621-4001-908E-74A5025C2D05" style="font-size: 16px; font-family:'Arial'; text-align: center; ">
                                                                                                    <div style=" margin-left: px; margin-top: 20px;padding-bottom: 30px; margin-bottom: 20px">
                                                                                                        <a href="https://www.facebook.com/bstimfair/">
                                                                                                            <img src="<?= base_url('images/boletines/face.png') ?>" style="">
                                                                                                        </a>
                                                                                                        <a href="https://twitter.com/bstim_fair">
                                                                                                            <img src="<?= base_url('images/boletines/twit.png') ?>" style=" padding-top: 8px;">
                                                                                                        </a>
                                                                                                    </div>                                                                                                    
                                                                                                    <div style="color:rgb(94,94,94);font-family:'Arial','Helvetica Neue';font-size:11px" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=es&amp;q=http://www.bstim.cat/blog/main/unsubscribe&amp;source=gmail&amp;ust=1482428959994000&amp;usg=AFQjCNETdFS7_JoTq9RFSVY2L0uZ6exhlQ">    
                                                                                                        En compliment de la LOPD 15/99 i LSSI 34/2002, i altres disposicions legals, l'informem que les seves dades de caràcter personal passaran a formar 
                                                                                                        part d'un fitxer automatitzat de caràcter  personal el Responsable del qual és FIRA D'IGUALADA. Autoritza la utilització dels mateixos per a les 
                                                                                                        comunicacions, incloent les realitzades via correu electrònic, que FIRA D'IGUALADA realitzi amb finalitats promocionals o informatives de les activitats 
                                                                                                        que organitza i/o dóna suport amb la seva logística. En cas que contracti algun servei amb FIRA D'IGUALADA, queda informat que les seves dades podran ser 
                                                                                                        comunicades, amb obligació de confidencialitat, a les empreses col·laboradores de FIRA D'IGUALADA, sempre que això sigui necessari a les fins que aquestes 
                                                                                                        realitzin el servei contractat. Queda igualment informat de la possibilitat d'exercitar sobre les dades els drets d'accés, rectificació, cancel·lació i oposició, 
                                                                                                        per la qual cosa haurà de dirigir-se per e-mail  

                                                                                                        <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 10px; color:blue; padding-top: 8px; ">    
                                                                                                            info@firaigualada.org.<br><br><br>
                                                                                                        </a>                                                                                                    
                                                                                                        <a href="http://bstim.cat/avis-legal.html" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px;">
                                                                                                            Avis Legal  
                                                                                                        </a>| 
                                                                                                        <a href="<?= base_url('main/unsubscribe') ?>" style="color: rgb(94, 94, 94); font-family: 'Arial', 'Helvetica Neue'; font-size: 11px; ">    
                                                                                                            Donar-se de baixa <br><br><br>
                                                                                                        </a>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td class="layout-block-padding-right" style="min-width: 25px; " width="25">&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            

                                            <div eqid="EQMST-22C8A407-2599-4B34-98E8-A70458F2ACA3" style="background-image:url(background-3.jpg); background-position: 0% 0%; " class="desktop-only" width="100%">
                                                <table class="layout-block-wrapping-table" style="min-width: 666px; " width="666" cellspacing="0" cellpadding="0" bgcolor="#fffeff" background="<?= base_url('images/boletines')?>/background-3.jpg">
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!--End of layout container -->

                                    </td>
                                    <td class="page-bg-show-thru" width="30">&nbsp;<!--Right page bg show-thru --></td>
                                </tr>
                            </tbody>
                        </table><!--email-body -->
                    </td>
                </tr>
            </tbody>
        </table><!--page-wrap -->
    </body>
</html>