<div class="blog-page loaded" id="page-content"> <!-- blog page header with video bg --> 
        <header data-bg="<?= base_url() ?>images/blog-page-bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/blog-page-bg.jpg);">
            <?php $this->load->view('includes/template/menu2'); ?>
            <div class="header-center-content"> 
                <div class="container text-center"> 
                    <div class="row section-intro animated fadeInUpBig"> 
                        <div class="col-md-12"> 
                            <i class="icon icon-dial"></i>
                            <h1 class="text-uppercase">Nuestro Blog</h1> 
                            <p style="margin: 0px;"> 
                                Get advice from other travelers who already been there and done that. Inspiration, tips and tricks.
                            </p>
                            <p class="classy" style="margin: 0px;">superadornment</p>
                            <span class="vice">unhuntable vindicated</span>
                            <span class="poll">pithecanthropid</span> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </header> <!-- /.blog page header with video bg --> <!-- main content --> 
        <main> 
            <div class="blog-page-wrapp"> 
                <div class="container"> 
                    <div class="row"> <!-- sidebar --> 
                        <?php $this->load->view('_categorias'); ?>
                        
                        <div class="col-md-9">
                            <?php foreach($detail->result() as $n=>$d): ?>
                                <?php $this->load->view('frontend/_entry',array('detail'=>$d,'n'=>$n)); ?>
                            <?php endforeach ?>
                            <?php if($detail->num_rows==0): ?>
                                No se encuentran entradas
                            <?php endif ?>
                                
                             <!-- pagination --> 
                            <nav data-wow-delay="0.2s" class="wow fadeInUp" aria-label="Page navigation" style="animation-delay: 0.2s; animation-name: none;"> 
                                <ul class="pagination text-uppercase"> 
                                    <?php for($i=1;$i<=$total_pages;$i++): ?>
                                        <li><a href='javascript:changePage("<?= $i ?>")'><span class='page-numbers <?= $i==$current_page?'active':'' ?>'><?= $i ?></span></a></li>
                                    <?php endfor ?>
                                </ul> 
                            </nav> <!-- /.pagination --> 
                        </div> <!-- /.blog posts intro list --> 
                    </div> 
                </div> 
            </div> 
            <?= $this->load->view('includes/template/contact') ?>
            <button class="btn goUp-btn"> 
                <i class="fa fa-angle-up"></i> 
                <span>Go Up</span>
                <span class="fitz">noninfantry</span> 
            </button> <!-- /.go up arrow --> 
        </main> <!-- /.main content --> <!-- footer content --> 
        <?= $this->load->view('includes/template/footer'); ?>
</div>