<!-- page content --> 
<div class="tour-page loaded" id="page-content"> 
    <header id="tour-header" data-bg="<?= base_url() ?>images/tour-intro-bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/tour-intro-bg.jpg);">
        <!-- navigation / main menu --> 
        <nav class="navbar navbar-fixed-top"> 
            <div class="container"> 
                <div class="navbar-header"> 
                    <button aria-expanded="false" data-target="#myMenu" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                    </button> 
                    <a href="<?= site_url() ?>" class="navbar-brand"> 
                        <img src="<?= base_url() ?>images/logopetit.jpg" style="width:180px">
                    </a>
                </div> <!-- navbar-collapse --> 
                <div id="myMenu" class="collapse navbar-collapse"> 
                    <ul class="nav navbar-nav text-uppercase text-center"> 
                        <?= $this->load->view('includes/template/menu-item') ?>
                    </ul> 
                    <button aria-expanded="false" data-target="#second-nav" data-toggle="collapse" class="second-nav-button navbar-toggle collapsed" type="button"> 
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                    </button> 
                </div> <!-- /.navbar-collapse --> 
            </div>
            <div class="animated slideInDown collapse navbar-collapse" id="second-nav"> <!-- container --> 
                <div class="container"> 
                    <div class="row"> 
                        <ul role="tablist" class="col-md-12 list-inline nav nav-tabs"> 
                            <li class="active"> 
                                <a href="#trip-actividades-incluidas">Incluidas</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-actividades-extras">Extras</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-alojamientos">Alojamientos</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-discotecas">Discotecas</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-seguros">Seguros</a>
                            </li> 
                            <li> 
                                <a href="#trip-material-viaja-gratis">Material</a>
                            </li> 
                            <li> 
                                <a href="#trip-autorizaciones-paternas">Autorizaciones</a>
                            </li> 
                            <li> 
                                <a href="#trip-preguntas-frecuentes">Preguntas</a>
                            </li> 
                            <li class="text-uppercase pull-right"> 
                                <a href="<?= site_url('p/contactenos') ?>">Contrátalo</a>
                            </li> 
                        </ul> 
                    </div> 
                </div> <!-- /.container --> 
            </div> 
        </nav> 
        <div data-wow-duration="1s" data-wow-delay="0.8s" class="header-center-content animated fadeIn"> 
            <div class="container"> 
                <div class="row text-center"> 
                    <div class="col-md-12"> 
                        <i id="hoverEye" aria-hidden="true" class="fa fa-eye icon"></i> 
                        <h1 class="text-uppercase">Toda la información que necesitas</h1> 
                        <p style="margin: 0px;"> 
                            <a class="header-tag" href="#"> ¿Si tienes alguna duda?</a>
                        </p>
                        <p class="coma" style="margin: 0px;">inapproachability</p>
                        <span class="poll">monogamous incardinating</span> Contacta 
                        <a class="header-tag" href="#">
                            con nosotros
                        </a>
                        <p class="vice" style="margin: 0px;">hyperelegance</p>
                        <span class="sown">undermelody deglutination</span> 
                        <p style="margin: 0px;"></p>
                        <p class="poll" style="margin: 0px;">verrucosity</p>
                        <span class="libeled">extravagance monogyny</span>
                        <span class="vice">recancellation</span> 
                    </div> 
                </div> 
            </div> 
        </div> 
        <div data-wow-delay="0.4s" class="trip-costs animated slideInUp"> 
            <div class="container text-center"> 
                <div class="row"> 
                    <ul class="col-md-12 list-inline"> 
                        <li> 
                            <h3>106</h3> 
                            <span>km</span>
                            <span class="bura">diakonikon</span> 
                        </li> 
                        <li> 
                            <h3>9</h3> 
                            <span>epic places</span>
                            <span class="vice">annotator</span> 
                        </li> 
                        <li> 
                            <h3>2</h3> 
                            <span>nights</span>
                            <span class="lefty">overreflective</span> 
                        </li> 
                        <li> 
                            <h3>4</h3> 
                            <span>adults</span>
                            <span class="fitz">monotrichic</span> 
                        </li> 
                        <li> 
                            <h3>1</h3> 
                            <span>car</span>
                            <span class="divc">ariminum</span> 
                        </li> 
                        <li> 
                            <h3>$229</h3> 
                            <span>/person</span>
                            <span class="lefty">regretable</span> 
                        </li> 
                    </ul> 
                </div> 
            </div> 
        </div> 
    </header> <!-- main content --> 
    <main> 
        <section id="trip-experience"> 
            <div class="container"> <!-- section-intro --> 
                <div class="row text-center section-intro bordered">
                    <div class="col-md-12"> 
                        <i class="icon icon-beaker wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"></i> 
                        <h1 class="text-uppercase wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            Que incluye
                        </h1> 
                        <p class="wow fadeInUp" style="margin: 0px; visibility: visible; animation-name: fadeInUp;">
                            MALLORCA ISLAND EXPERIENCE 2017
                        </p>
                    </div> 
                </div> <!-- /.section-intro --> <!-- small intro --> 
                <div class="row"> 
                    <div class="col-md-offset-1 col-md-10">
                        <div class="small-intro">
                            <p style="margin: 0px;">
                            <h3>Vuelo  IDA + VUELTA EN COMPAÑÍA REGULAR</h3>
							<h4>(VUELING, VOLOTEA, AIR EUROPA O IBERIA)</h4>
							
							• Transporte de equipaje de cabina hasta 10 kg y de bodega hasta 23kg.</br>
							• Mejor acomodación disponible.
 
							<h3>TRANSFERS</h3>
							• Traslado autobús - Instituto / aeropuerto de salida. (OPCIONAL)</br>
							• Traslado autobús – aeropuerto de Mallorca / Hotel, ida y vuelta. (INCLUIDO)</br></br>
							Sabemos que lo que más cuesta a los estudiantes cuando intentan organizarse entre ellos a la hora de buscar su viaje de fin de estudios es cómo llegar a los destinos (horarios, puntos de encuentro, autobuses, aviones, barcos, equipajes, bonos de reserva, documentación, traslados, etc.).
Es en este punto dónde hacemos especial relevancia al trabajo de nuestro equipo técnico.
El viaje se iniciará como punto de encuentro en la localidad de su hijo, dónde nuestro autobús, acompañado de nuestro staff, recogerá a todo su grupo el día de salida a la hora establecida.
Antes del viaje nuestro equipo técnico les dará la información y documentación final del viaje, la pulsera identificativa, etc… según corresponda.</br></br>
Al subir al autobús nuestro equipo le dará la bienvenida al grupo entregándole la documentación del viaje tarjeta de embarque, y demás.
Una vez estacione el autobús en el aeropuerto, todos los grupos que inicien la salida ese mismo día únicamente tendrán que seguir las indicaciones de nuestro staff procediendo a la facturación de sus maletas y al embarque, así de sencillo, para los estudiantes y para la organización.
En el trayecto en avión, sus hijos irán acompañados desde el aeropuerto de Palma de Mallorca, lugar dónde nuevamente nuestro equipo les estará esperando con los autobuses para guiarles hasta el Resort.
Cómo decíamos al principio, los traslados son así de sencillos. Los estudiantes y sus padres no deben preocuparse de nada durante los trayectos ya que en todo momento sus hijos podrán realizar directamente cualquier consulta que puedan tener a nuestros miembros de la organización, ya que les acompañan en todo momento.
 
							<h3>TRANSFERS ALOJAMIENTO en hoteles de  *** y ***sup.</h3>
							Todos los estudiantes se alojaran en habitaciones de los hoteles adecuadas al uso de los estudiantes, suelen estar completamente equipados con tv, baño privado, terraza y sus las habitaciones suelen ser triples o cuádruples (es decir, múltiples). 
Como en el viaje está incluida la Pensión Completa, no se tendrán que preocupar de nada, ya que el hotel dispone de un restaurante buffet libre para que todos los estudiantes puedan desayunar, comer y cenar. A parte, de estos hoteles tenemos otros en los que su régimen alimenticio es el Todo incluido, y que se indicará horarios y contenidos dependiendo del hotel elegido ya que puede tener variaciones de unos a otros.
</br></br>
							• Habitaciones  en distribución múltiple.</br>
							• Pensión completa o Todo Incluido según se elija.
							
							<h3>STAFF EN ORIGEN: Una persona del  equipo de la organización.</h3>
Les darán la bienvenida a sus hijos en el instituto con el autobús para acompañarles al Aeropuerto de salida, y asistirlos durante todo el viaje, incluso el vuelo.</br></br>

							<h3>STAFF EN MALLORCA: Un equipo formado por asistentes y coordinadores  de la organización.</h3>
Les darán la bienvenida a sus hijos a la llegada a Mallorca en el Aeropuerto y se encargarán que toda la organización del viaje se cumpla a la perfección.
Los acompañarán en el traslado a los hoteles y en el check-in para el que tendrán que tener a mano su DNI, y el depósito que solicita el Hotel.</br></br>

							<h3>OTROS GASTOS: EQUIPAJE, IMPUESTOS Y TASAS</h3>
El equipaje necesario para el viaje tanto de mano como de bodega está incluido (debe de cumplir los requisitos que indica cada compañía en su caso, siendo informados de los mismos antes de la salida), los impuestos y las tasas aeroportuarias están incluidas en el precio.</br></br>
                            </p>
                        </div> 
                    </div> 
                </div> <!-- /.small intro --> <!-- trip background -->                 
            </div> 
        </section> 
        <section id="trip-activities" style="margin:85px 0"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row"> <!-- trip activities --> 
                    <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                        <div class="section-intro"> 
                            <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                            <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                Actividades Incluidas
                            </h1> 
                            <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                Durante la estancia de sus hijos nuestra organización tiene preparadas un sinfín de actividades para todos los grupos con el fin de hacer de su viaje de fin de estudios una experiencia inolvidable.
El primer día a la llegada al hotel elegido, nuestro staff les acompañará en el check-in, les atenderá e intentará resolver todas las dudas que puedan tener, prestándoles asistencia 24 horas.
Además les situaran en el mapa, explicarles las actividades, excursiones y ocio que podrán disfrutar durante sus estancias y facilitarles consejos e información práctica a tener en cuenta, como listados telefónicos, puntos de encuentro, lugares de interés, centros de salud, policía, información turística etc.
Todas las actividades son voluntarias  y opcionales y nuestro equipo técnico y staff se encargará de que todos los estudiantes puedan disfrutar de la combinación de eventos deportivos, conciertos, excursiones, playas y sol.
Les informaremos de los teléfonos de referencia de una persona del Staff por hotel, y además estaremos en continua convivencia con ellos en su día a día y a su entera disposición.

                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Pool Parties</h4> 
                                <span>Con barra libre de cócteles.* (Según hoteles y disponibilidad).</span>                                
                            </li> 
                            <li> 
                                <h4>Videos</h4> 
                                <span>Aftermovie & fotógrafias del evento.</span>                                
                            </li> 
                            <li> 
                                <h4>Regalos sorpresa</h4> 
                                <span>Durante el evento. (Participa en los diferentes concursos y eventos)</span>                                
                            </li> 
                        </ul> 
                    </div> 
                    <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Otra actividad</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>                                
                            </li> 
                            <li> 
                                <h4>Otra actividad</h4> 
                                <span>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</span>
                            </li> 
                            <li> 
                                <h4>Otra actividad</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>
                            </li> 
                        </ul> 
                    </div> <!-- /.trip activities --> </div> 
            </div> 
        </section> 
        <section id="trip-actividades-extras" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row"> <!-- trip activities --> 
                    <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                        <div class="section-intro"> 
                            <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                            <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                Actividades Extras
                            </h1> 
                            <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                He aquí una breve lista de las actividades que experimentará
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Excursión Panorámica Palma</h4> 
                                <span>You’ll get a chance to drive your car on the awesome mountain roads.</span>                                
                            </li> 
                            <li> 
                                <h4>Boat party</h4> 
                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>                                
                            </li> 
                            <li> 
                                <h4>Coves Drach</h4> 
                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>                                
                            </li> 
                        </ul> 
                    </div> 
                    <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Concierto Aquasound</h4> 
                                <span>o Univerland según las fechas y disponibilidad.</span>                                
                            </li> 
                            <li> 
                                <h4>Excursión playa de Es Trenc</h4> 
                                <span>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</span>
                            </li> 
                            <li> 
                                <h4>Pack Discotecas</h4> 
                                <span>con los mejores DJ’s, en las  mejores discotecas con asistencia entrada y salida.</span>
                            </li> 
                        </ul> 
                    </div> <!-- /.trip activities --> </div> 
            </div> 
        </section> 
        <section id="trip-alojamientos" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row"> <!-- trip activities --> 
                    <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                        <div class="section-intro"> 
                            <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                            <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                Alojamientos
                            </h1> 
                            <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                Los hoteles principalmente se encuentran en la zona del Arenal, una de las más visitadas y especiales playas  de Mallorca, con su puerto deportivo y mucho ambiente diurno y nocturno. Pudiendo elegir también la zona de Magaluf de así desearlo.
En la zona tienen todos los servicios, centro de salud, ayuntamiento, guardia civil, bares, discotecas, pubs, tiendas, supermercados,  cajeros… (Nuestro equipo les va a situar cada servicio a nuestra llegada por si fuese de su interés o lo necesitaran).

                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Hotel Amazonas</h4> 
                                <span>You’ll get a chance to drive your car on the awesome mountain roads.</span>                                
                            </li> 
                            <li> 
                                <h4>Sun Club El Dorado</h4> 
                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>                                
                            </li> 
                            <li> 
                                <h4>Hotel Blue Sea</h4> 
                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>                                
                            </li> 
                        </ul> 
                    </div> 
                    <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Hotel Costa Verde</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>                                
                            </li> 
                            <li> 
                                <h4>Hotel Fergus Géminis</h4> 
                                <span>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</span>
                            </li> 
                            <li> 
                                <h4>Hotel Kilimanjaro</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>
                            </li> 
                        </ul> 
                    </div> <!-- /.trip activities --> </div> 
            </div> 
        </section> 

        
        <section id="trip-discotecas" style="margin:85px 0"> 
            <div class="container" style="border-bottom: 10px solid #f2f0ec; padding-bottom:30px"> <!-- section-intro --> 
                <div class="row text-center section-intro">
                    <div class="col-md-12"> 
                        <i data-wow-delay="0.2s" class="icon icon-map wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"></i> 
                        <h1 data-wow-delay="0.4s" class="text-uppercase wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            Discotecas (opcionales) +16 años
                        </h1> 
                        <p data-wow-delay="0.4s" class="wow  fadeInUp animated" style="margin: 0px; visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            Colaboramos el Grupo GCO, grupo Cursach, propietario de las discotecas BCM, Titos, Pacha… y te ponemos en contacto con los comerciales oficiales del Grupo, que te atenderán y explicaran los paquetes que tienen y cómo funcionan. Te acompañaran nuestros asistentes/coordinadores tanto a la ida como a la vuelta, que incluye transporte, copa/refresco y/o regalo….
                        </p>
                        <p class="classy" style="margin: 0px;">apsarases</p>
                        <span class="libeled">unmutated vindicated</span>
                        <span class="divc">unargumentative</span> 
                    </div> 
                </div> <!-- /.section-intro --> <!-- place tabs --> 
                <div class="row"> 
                    <div class="col-md-12"> <!-- Nav tabs --> 
                        <ul role="tablist" class="nav nav-tabs places-tabs text-uppercase text-center"> 
                            <li class="active wow pulse" role="presentation" style="visibility: visible; animation-name: pulse;"> 
                                <a data-toggle="tab" role="tab" aria-controls="day1-tab" href="#day1-tab">
                                    BCM
                                </a>
                                <p class="fitz" style="margin: 0px;">lovableness</p>
                                <span class="fitz">stipulation overdelicious</span> 
                            </li> 
                            <li data-wow-delay="0.2s" class="wow pulse" role="presentation" style="visibility: visible; animation-delay: 0.2s; animation-name: pulse;">
                                <a data-toggle="tab" role="tab" aria-controls="day2-tab" href="#day2-tab">
                                   TITO'S
                                </a>
                                <p class="mydiv" style="margin: 0px;">voroshilovsk</p>
                                <span class="frow">bipartisanship nonannexable</span> 
                            </li> 
                            <li data-wow-delay="0.3s" class="wow pulse" role="presentation" style="visibility: visible; animation-delay: 0.3s; animation-name: pulse;"> 
                                <a data-toggle="tab" role="tab" aria-controls="day3-tab" href="#day3-tab">
                                    PACHA
                                </a>
                                <p class="coma" style="margin: 0px;">retabulated</p>
                                <span class="frow">bathymetry inexorable</span> 
                            </li> 
                        </ul> <!-- Tab panes --> 
                        <div class="tab-content places-content" style="border-bottom: 0px solid #f2f0ec; margin-bottom: 0px"> 
                            <div id="day1-tab" class="tab-pane active" role="tabpanel"> 
                                <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map" class="route-map"></div> 
                                    </div>
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize">BCM. Auténtico ambiente Mallorquín</h4> 
                                            <p style="margin: 0px;"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                            <p class="lefty" style="margin: 0px;">unprecautioned</p>
                                            <span class="sown">virility melodramatising</span>
                                            <span class="kiwi">ariminum</span> 
                                            <span> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </span>
                                            <span class="libeled">prepenetration</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>Bonos combiando Pacha + Tito's</h4> 
                                                <span>de tres noches con dos precios 2017.</br> 
                                                • 100€ incluyendo BCM asegurado.</br> 
												• 85€ sin garantizar la entrada a BCM</span>
                                                <span class="lehi">stupefacient</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otra punto</h4> 
                                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>
                                                <span class="frow">incardinating</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otro punto</h4> 
                                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>
                                                <span class="mydiv">overneutralized</span> 
                                            </li> 
                                        </ul> 
                                    </div> 
                                </div> 
                            </div> 
                            <div id="day2-tab" class="tab-pane" role="tabpanel"> 
                                <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map2" class="route-map"></div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize">TITO'S. Auténtico ambiente Mallorquín</h4> 
                                            <p style="margin: 0px;"> 
                                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                                            <p class="vice" style="margin: 0px;">overspicing</p>
                                            <span class="lefty">endamagement supraorbital</span>
                                            <span class="libeled">phonogramic</span> 
                                            <span> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                                            <span class="sown">undistorting</span> 
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>Bonos combiando Pacha + Tito's</h4> 
                                                <span>de tres noches con dos precios 2017.</br> 
                                                • 100€ incluyendo BCM asegurado.</br> 
												• 85€ sin garantizar la entrada a BCM</span>
                                                <span class="lehi">stupefacient</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otra punto</h4> 
                                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>
                                                <span class="frow">incardinating</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otro punto</h4> 
                                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>
                                                <span class="mydiv">overneutralized</span> 
                                            </li> 
                                        </ul> 
                                    </div> 
                                </div> 
                            </div>
                            <div id="day3-tab" class="tab-pane" role="tabpanel"> <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map3" class="route-map"></div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize">TITO'S. Auténtico ambiente Mallorquín</h4> 
                                            <p style="margin: 0px;"> 
                                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                                            <p class="vice" style="margin: 0px;">overspicing</p>
                                            <span class="lefty">endamagement supraorbital</span>
                                            <span class="libeled">phonogramic</span> 
                                            <span> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </span>
                                            <span class="sown">undistorting</span> 
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>Bonos combiando Pacha + Tito's</h4> 
                                                <span>de tres noches con dos precios 2017.</br> 
                                                • 100€ incluyendo BCM asegurado.</br> 
												• 85€ sin garantizar la entrada a BCM</span>
                                                <span class="lehi">stupefacient</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otra punto</h4> 
                                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>
                                                <span class="frow">incardinating</span> 
                                            </li> 
                                            <li> 
                                                <h4>Otro punto</h4> 
                                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>
                                                <span class="mydiv">overneutralized</span> 
                                            </li> 
                                        </ul> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <?= img('images/bg1_1.jpg','width:100%') ?>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <?= img('images/bg1_2.jpg','width:100%') ?>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="trip-seguros" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row"> <!-- trip activities --> 
                    <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                        <div class="section-intro"> 
                            <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                            <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                Seguros
                            </h1> 
                            <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                               Tener un buen seguro es síntoma de tranquilidad. Por ello trabajamos con las compañías aseguradoras líderes en el sector colectivo de estudiantes.
 
En este punto le confirmamos que en el viaje está incluido el seguro obligatorio básico/estándar de asistencia médica y sanitaria y RC siendo el seguro AXA ESTUDIANTES dónde se amplían las coberturas en asistencia médica y sanitaria durante el viaje, el RC e incluye causas de anulación justificadas incluso por suspensos (según condicionado de la póliza):
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Asistencia médica y sanitaria</h4> 
                                <span>en viaje y RC.</span>                                
                            </li> 
                            <li> 
                                <h4>Seguro básico</h4> 
                                <span>Posibilidad de seguro de anulación especial</span>                                
                            </li> 
                            <li> 
                                <h4>Axa estudiantes. Y especial de cancelación.</h4> 
                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>                                
                            </li> 
                        </ul> 
                    </div> 
                    <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>HAZ TU SEGURO A LO GRANDE</h4> 
                                <span>Por ello trabajamos con las compañías aseguradoras líderes en el sector colectivo de estudiantes.
</br></br>En este punto le confirmamos que en el viaje está incluido el seguro obligatorio básico/estándar de asistencia médica y sanitaria y RC siendo el 
seguro AXA ESTUDIANTES dónde se amplían las coberturas en asistencia médica y sanitaria durante el viaje, el RC e incluye causas 
de anulación justificadas incluso por suspensos (según condicionado de la póliza):</span>                                
                            </li> 
                           
                        </ul> 
                    </div> <!-- /.trip activities --> </div> 
            </div> 
        </section>
        <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge2" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg);"> 
            <div class="container"> 
                <div id="price" class="row"> 
                    <div class="col-md-3"> 
                        <i class="icon icon-wallet"></i> 
                        <h1 class="text-uppercase">SEGURO A LO GRANDE</h1> 
                    </div> 
                    <div class="col-md-offset-1 col-md-8"> 
                        <div class="price-block"> 
                            <h1> 
                                €22 <sup>.00</sup> 
                            </h1> 
                            <span>por persona</span>                            
                        </div> 
                        <div class="price-description"> 
                            <p style="margin: 0px; font-size: 24px;">
                                RESERVA DE PLAZA AXA ESTUDIANTES
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-xs-12" style="background: rgba(242, 240, 236, 1) none repeat scroll 0% 0%; margin: 50px auto; min-height: 300px; padding:20px; font-family: serif">
                        <p style="font-size:15px">SERVICIOS INCLUIDOS + SEGURO PLUS ESTUDIANTES Seguro PLUS ESTUDIANTES. Además de cubrir asistencia médica y RC, cubre la anulación por causas médicas y por suspensos cumpliendo ciertos requisitos, y otras circunstancias especificadas en el condicionado</p>
                        <ul>
                            <li>Asistencia médica y sanitaria en España: 601,01€</li>
                            <li>Repatriación o transporte de heridos y/o enfermos: ILIMITADO</li>
                            <li>Repatriación de acompañante INCLUIDO</li>
                            <li>Desplazamiento de un familiar en caso de hospitalización superior a cinco días: ILIMITADO</li>
                            <li>Convalecencia en hotel. Máximo 10 días. 30 € por día</li>
                            <li>Repatriación o transporte del asegurado fallecido. ILIMITADO</li>
                            <li>Búsqueda, localización y envío de equipaje extraviado ILIMITADO</li>
                            <li>Robo y daños materiales del equipaje 150,25 €</li>
                            <li>Transmisión de mensajes urgentes ILIMITADO</li>
                            <li>Seguro de Equipajes AXA 150 €</li>
                            <li>Seguro de Responsabilidad Civil AXA 60.000 €</li>
                            <li>Regreso anticipado por fallecimiento u hospitalización superior a dos días de un familiar de hasta 2º grado. ILIMITADO</li>
                            <li>Teléfono de incidencias: 24 HORAS</li>
                        </ul>
                        <p>A parte, le informamos que, si desea, cuando realice el primer pago de la reserva  puede contratar adicionalmente el seguro ANULACIÓN PLUS ESTUDIANTES que cubre las causas de cancelación por motivos académicos y/o hospitalarios, justificadas hasta 36 según condicionado cubriendo el 100% del importe y sin causa justificada, cubriendo un 88% del importe total del viaje. Ver condiciones generales de cancelación específicas de este seguro. Este seguro se tiene que contratar en el mismo momento que se hace la reserva para que tenga validez, ya que el viaje entra en gastos en el momento de contratarlo.</p>
                    </div>
                </div> 
            </div>                         
        </section>
        
        <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge2" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg); padding-top:0"> 
            <div class="container"> 
                <div id="price" class="row"> 
                    <div class="col-md-offset-4 col-md-8"> 
                        <div class="price-block"> 
                            <h1> 
                                €22 <sup>.00</sup> 
                            </h1> 
                            <span>por persona</span>                            
                        </div> 
                        <div class="price-description"> 
                            <p style="margin: 0px; font-size: 24px;">
                                RESERVA DE PLAZA AXA ESTUDIANTES
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-xs-12" style="background: rgba(242, 240, 236, 1) none repeat scroll 0% 0%; margin: 50px auto; min-height: 300px; padding:20px; font-family: serif">
                        <p style="font-size:15px">SERVICIOS INCLUIDOS + SEGURO PLUS ESTUDIANTES Seguro PLUS ESTUDIANTES. Además de cubrir asistencia médica y RC, cubre la anulación por causas médicas y por suspensos cumpliendo ciertos requisitos, y otras circunstancias especificadas en el condicionado</p>
                        <ul>
                            <li>Asistencia médica y sanitaria en España: 601,01€</li>
                            <li>Repatriación o transporte de heridos y/o enfermos: ILIMITADO</li>
                            <li>Repatriación de acompañante INCLUIDO</li>
                            <li>Desplazamiento de un familiar en caso de hospitalización superior a cinco días: ILIMITADO</li>
                            <li>Convalecencia en hotel. Máximo 10 días. 30 € por día</li>
                            <li>Repatriación o transporte del asegurado fallecido. ILIMITADO</li>
                            <li>Búsqueda, localización y envío de equipaje extraviado ILIMITADO</li>
                            <li>Robo y daños materiales del equipaje 150,25 €</li>
                            <li>Transmisión de mensajes urgentes ILIMITADO</li>
                            <li>Seguro de Equipajes AXA 150 €</li>
                            <li>Seguro de Responsabilidad Civil AXA 60.000 €</li>
                            <li>Regreso anticipado por fallecimiento u hospitalización superior a dos días de un familiar de hasta 2º grado. ILIMITADO</li>
                            <li>Teléfono de incidencias: 24 HORAS</li>
                        </ul>
                        <p>A parte, le informamos que, si desea, cuando realice el primer pago de la reserva  puede contratar adicionalmente el seguro ANULACIÓN PLUS ESTUDIANTES que cubre las causas de cancelación por motivos académicos y/o hospitalarios, justificadas hasta 36 según condicionado cubriendo el 100% del importe y sin causa justificada, cubriendo un 88% del importe total del viaje. Ver condiciones generales de cancelación específicas de este seguro. Este seguro se tiene que contratar en el mismo momento que se hace la reserva para que tenga validez, ya que el viaje entra en gastos en el momento de contratarlo.</p>
                    </div>
                </div> 
            </div>                         
        </section>
        
        <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge2" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg); padding-top:0px"> 
            <div class="container"> 
                <div id="price" class="row"> 
                    <div class="col-md-offset-4 col-md-8"> 
                        <div class="price-block"> 
                            <h1> 
                                €22 <sup>.00</sup> 
                            </h1> 
                            <span>por persona</span>                            
                        </div> 
                        <div class="price-description"> 
                            <p style="margin: 0px; font-size: 24px;">
                                RESERVA DE PLAZA AXA ESTUDIANTES
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-xs-12" style="background: rgba(242, 240, 236, 1) none repeat scroll 0% 0%; margin: 50px auto; min-height: 300px; padding:20px; font-family: serif">
                        <p style="font-size:15px">SERVICIOS INCLUIDOS + SEGURO PLUS ESTUDIANTES Seguro PLUS ESTUDIANTES. Además de cubrir asistencia médica y RC, cubre la anulación por causas médicas y por suspensos cumpliendo ciertos requisitos, y otras circunstancias especificadas en el condicionado</p>
                        <ul>
                            <li>Asistencia médica y sanitaria en España: 601,01€</li>
                            <li>Repatriación o transporte de heridos y/o enfermos: ILIMITADO</li>
                            <li>Repatriación de acompañante INCLUIDO</li>
                            <li>Desplazamiento de un familiar en caso de hospitalización superior a cinco días: ILIMITADO</li>
                            <li>Convalecencia en hotel. Máximo 10 días. 30 € por día</li>
                            <li>Repatriación o transporte del asegurado fallecido. ILIMITADO</li>
                            <li>Búsqueda, localización y envío de equipaje extraviado ILIMITADO</li>
                            <li>Robo y daños materiales del equipaje 150,25 €</li>
                            <li>Transmisión de mensajes urgentes ILIMITADO</li>
                            <li>Seguro de Equipajes AXA 150 €</li>
                            <li>Seguro de Responsabilidad Civil AXA 60.000 €</li>
                            <li>Regreso anticipado por fallecimiento u hospitalización superior a dos días de un familiar de hasta 2º grado. ILIMITADO</li>
                            <li>Teléfono de incidencias: 24 HORAS</li>
                        </ul>
                        <p>A parte, le informamos que, si desea, cuando realice el primer pago de la reserva  puede contratar adicionalmente el seguro ANULACIÓN PLUS ESTUDIANTES que cubre las causas de cancelación por motivos académicos y/o hospitalarios, justificadas hasta 36 según condicionado cubriendo el 100% del importe y sin causa justificada, cubriendo un 88% del importe total del viaje. Ver condiciones generales de cancelación específicas de este seguro. Este seguro se tiene que contratar en el mismo momento que se hace la reserva para que tenga validez, ya que el viaje entra en gastos en el momento de contratarlo.</p>
                    </div>
                </div> 
            </div>                         
        </section>
        <section id="trip-material-viaja-gratis" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row"> <!-- trip activities --> 
                    <div class="col-md-offset-4 col-md-9 col-sm-offset-6 col-sm-6"> 
                        <div class="section-intro"> 
                            <i data-wow-delay="0.4s" class="icon icon-target wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                            <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                Material Viaja Gratis
                            </h1> 
                            <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                 Consigue tu viaje por 0€.
                            </p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-offset-4 col-md-3 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Vende papeletas, merchandising, organiza eventos…</h4> 
                                <span>You’ll get a chance to drive your car on the awesome mountain roads.</span>                                
                            </li> 
                            <li> 
                                <h4>Merchandising</h4> 
                                <span>There are epic landscapes to be seen and pictured. So, prepare your camera!</span>                                
                            </li> 
                            <li> 
                                <h4>Trekking</h4> 
                                <span>Lorem ipsum dolor sit amet enim ipsam voluptatem quia.</span>                                
                            </li> 
                        </ul> 
                    </div> 
                    <div class="col-md-offset-1 col-md-4 col-sm-offset-6 col-sm-6"> 
                        <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                            <li> 
                                <h4>Yoga</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>                                
                            </li> 
                            <li> 
                                <h4>Barbeque</h4> 
                                <span>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur</span>
                            </li> 
                            <li> 
                                <h4>Kayaking</h4> 
                                <span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</span>
                            </li> 
                        </ul> 
                    </div> <!-- /.trip activities --> </div> 
            </div> 
        </section>
        <section id="trip-autorizacion-paterna" class="adventure-select"> 
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="text-uppercase adventure-list experience"> 
                        <div data-wow-duration="1s" data-wow-delay="0.1s" class="col-md-12 col-sm-12 animated fadeInUp"> 
                            <a href="#"> 
                                <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/actividades.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center">                                     
                                    <h5>AUTORIZACIÓN PATERNA</h5> 
                                </div> 
                            </a>
                        </div>
                    </div> <!-- /.adventure list --> 
                </div> 
            </div> 
        </section>
        <section class="adventure-select" style="font-size:15px; font-family: serif">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="col-xs-12 col-md-4">
Condiciones generales producto: MIEX 2017.</br>
REGULACIÓN JURÍDICA APLICABLE Y ORGANIZACIÓN</br>Las partes contratantes se someten expresamente a la jurisdicción de los tribunales de la ciudad correspondiente al domicilio social del Organizador, para resolver todas sus diferencias.</br></br>
La organización de los viajes publicados en este catálogo, ha sido realizada por KANVOY, XG-386., con domicilio en la calle Condes de Andrade no1 local 1, Culleredo, A Coruña.</br></br>
FIANZA Los Hoteles podrán exigir una fianza a cada uno de los clientes en concepto de posibles desperfectos que el grupo pueda ocasionar durante la estancia. El día de salida, la fianza será devuelta en el 100% de su importe, siempre y cuando ningún componente del grupo haya causado desperfecto alguno. Se recomienda, revisar posibles desperfectos ya existentes en la habitación en el momento de la entrada y ponerlos en conocimiento de la recepción (mejor por escrito), con el fin de que no imputen la culpabilidad de lo mismo al grupo.
SEGURO El presupuesto incluye seguro de viaje obligatorio. Se ofrece la posibilidad de contratar el seguro opcional con coberturas especiales ANULACIÓN PLUS.
CONTRATO DE ACEPTACIÓN El ingreso o depósito de las cantidades requeridas y la autorización paterna si fuera requerida, así como la inscripción, implica la aceptación de las condiciones del viaje aquí detalladas.
CONEXIONES KANVOY no se hace responsable de la pérdida de conexiones o salidas de vuelos en el caso de que algún servicio sea contratado por cuenta del cliente, tales como vuelos, traslados o cualquier otro servicio.
VUELOS Y HOTELES Las fechas de vuelos y hoteles indicados en este folleto están sujetas a disponibilidad en el momento de formalizar la reserva del grupo.
FORMA DE PAGO Los grupos serán pre-pagados antes de la salida delosclientes.Eldepósitoo1erplazoserádel30%deltotal,el 2o plazo será antes de la fecha límite acordada por importe de otro 30% del total y el 3o plazo será el 40% del total, en función de los servicios contratados por el cliente antes de la fecha límite de 05/05/2016. En cualquier caso, el viaje debe quedar en su totalidad abonado como máximo 40 días hábiles antes de la salida del mismo. Los pagos de servicios extras como actividades diurnas o nocturnas en su caso también serán abonados con anterioridad excepto que se determine de otra forma entre las partes.
Todo cliente que se encuentre en situación e impago a 35 días naturales de las fechas establecidas, quedará sujeto al pago de los gastos de la reserva ocasionados, así como de la factura de gestión de la agencia.
Formas de pago:
A) Transferencia Bancaria a la cuenta del Banco Popular con iban Número: ES36 0238 8119 6306 0007 7166 el titular de la
cuenta es KANVOY (Ma Belén Muñoz Seijas), con cif
32807078Q y en observaciones hay que indicar el DNI y nombre y apellidos de la persona que viaja. La reserva no quedará formalizada hasta que no se complete el trámite de la aceptación de condiciones, autorización paterna en caso necesario y haber facilitado ficha con datos personales completa y copia DNI del viajer@.
B) Depósito en efectivo en la agencia (C/ Condes de Andrade no1
local 1, Culleredo, A Coruña), o con tarjeta.
NOTA IMPORTANTE
KANVOY podrá solicitar una forma de pago distinta si en los servicios contratados el proveedor requiere fianzas, depósitos o pagos especiales.
PRECIO DESDE Los precios expresados “desde” son precios umbrales mínimos cotizados en base a fechas y disponibilidad.
                    </div>
                    <div class="col-xs-12 col-md-4">
                       PRECIO FINAL El precio final será el indicado en el contrato de viaje Combinado que se firmará con un representante de cada grupo y del cual se dará copia a todos los viajeros de dicho grupo, o en su caso la firma personal de cada viajero si así se acuerda entre las partes.
LAS COMIDAS Las bebidas NO están incluidas en las P.C., salvo indicación expresa. En el T.I. los precios no incluyen bebidas de marcas Premium (reservas, etc.), excepto donde se indique lo contrario. El número de pensiones completas será el mismo de las noches de hotel contratadas, sin servicios adicionales.
Los grupos han de cumplir los horarios establecidos para almuerzos y cenas y reconfirmar la hora de la llegada, de lo contrario KANVOY no se responsabiliza de los trastornos o pérdidas de dichos servicios.
CLIENTES MENORES KANVOY se reserva el derecho de admisión a todo cliente menor de 18 años si no presenta la autorización paterna o tutor legal.
LOS CIRCUITOS El precio de los circuitos puede variar según la disponibilidad de los servicios cotizados. No están incluidas las entradas a los parques, museos, monumentos, etc. ni cuando se realizan excursiones o visitas panorámicas, salvo indicación expresa en el apartado de ‘El precio incluye’. Consúltanos horarios, precios de entrada y días de apertura de museos y monumentos. Las tarifas aéreas cotizadas en los circuitos de avión o de avión + bus, están sujetas a revisión por parte de las compañías aéreas. En los circuitos de autobús están contempladas las interrupciones por descanso según legislación vigente. No se incluyen dentro del programa visitas a ciudades o monumentos no especificados en el itinerario. En los circuitos, el autobús sólo está contemplado para los traslados indicados en cada itinerario, no estando el autobús a disposición de los clientes, una vez terminado el traslado.
CAMBIOS VOLUNTARIOS La empresa informa de que, en el caso de que el cliente, una vez iniciado el viaje, solicite voluntariamente cualquier modificación de los inicio y el final del circuito pueden adelantarse o retrasarse, acortándose o ampliándose servicios contratados (p.ej.: ampliación de noches de estancia, cambios de hotel o vuelos, etc.) los precios de los servicios turísticos podrán no corresponderse con los publicados en el folleto que dio lugar a la contratación. En este caso, los traslados entre hoteles correrán por cuenta del cliente, debiendo asimismo abonar el cliente los gastos de gestión, como consecuencia de la tramitación de la nueva reserva efectuada, o del cambio de vuelo solicitado.
REGRESOS ANTICIPADOS En el caso de abandono del Establecimiento antes de la fecha, KANVOY no se compromete a efectuar devolución alguna. (Véase coberturas cubiertas por la póliza del seguro de viaje).
LOS HOTELES Las habitaciones múltiples pueden constar de 2, 3, 4 camas o ser tipo apartamento 2 + 2, 2 + 3 ó 3 + 3 compartiendo baño. No están incluidos extras como gastos de teléfono, lavanderías, servicios de bar, etc.
En algunos cruceros, podrían existir camas de matrimonio o literas. Si esto ocurre, os informaremos puntualmente para que lo transmitáis al grupo y sean ellos quienes acepten estas condiciones.
Algunas instalaciones en algunos hoteles son operativas exclusivamente en fechas concretas y no toda la temporada como, por ejemplo, el aire acondicionado, calefacción, piscinas, jacuzzis climatizados, etc., que estarán sujetos a lo determinado por el establecimiento hotelero.
Ante los desperfectos que los grupos puedan causar en las distintas instalaciones reservadas por nosotros, KANVOY no se hace responsable de los gastos que se puedan ocasionar.
En algunos países, la categoría de los hoteles no corresponde con su equivalente en España, porque carecen de una clasificación oficial y en cuyo caso KANVOY trasmitirá la información facilitada por los proveedores. En otras ocasiones la calidad hotelera del país de destino es inferior, aunque esté homologado como tal en su clasificación oficial.
                    </div>
                    <div class="col-xs-12 col-md-4">
                        En la mayoría de los establecimientos, la habitación estará a disposición del grupo desde las 14:00 horas del día de llegada hasta las 12:00 horas del día de salida. Si el avión regresa por la tarde, el cliente podrá depositar sus pertenecías en estancias habilitadas para tal fin (el cliente deberá consultar en el hotel correspondiente) y podrá permanecer en zonas comunes del mismo hasta la hora de recogida.
· Los servicios, actividades e instalaciones complementarias pueden tener un coste adicional sujeto a cambios con pago directo siendo su publicación simplemente a título informativo.
· Dependiendo del número de clientes según nacionalidad, los hoteles se reservan el derecho a programar sus actividades exclusivas en idiomas extranjeros.
· Es habitual que los hoteles exijan para el servicio de cena pantalón
largo para los caballeros.
EQUIPAJES En cuanto al transporte terrestre se refiere, se entenderá que el equipaje y demás enseres personales del usuario los conserva consigo, cualquiera que sea la parte del vehículo en que vayan colocados, y que se transporta por cuenta y riesgo del usuario, sin que la agencia organizadora venga obligada a responder a la pérdida, robos o daños que el mismo pudiera sufrir durante el viaje por cualquier causa, incluida la manipulación de traslados hotel/aeropuerto o viceversa cuando existan. Se recomienda a los usuarios que estén presentes en todas las manipulaciones de carga y descarga de los equipajes.
En cuanto al transporte aéreo, ferroviario, marítimo o fluvial de equipaje son de aplicación las condiciones establecidas por las compañías transportadoras, siendo el billete de pasaje el documento que vincula a las citadas compañías y al pasajero. En el supuesto de sufrir algún daño, demora en la entrega o extravío, el consumidor deberá presentar, en el acto, la oportuna reclamación a la compañía de transportes.
En caso de querer transportar para sus vacaciones equipaje de amplio volumen tal como bicicletas, tablas de surf, etc., deberá abonar el correspondiente suplemento.
Se recomienda no guardar objetos de valor, medicamentos, pasaportes, documentación o dinero en el interior del equipaje facturado. En caso de extrema necesidad, recomendamos que se certifique los contenidos antes de viajar, haciendo una declaración de valor.
VUELOS / BARCOS Los viajes realizados en vuelos/barcos regulares o vuelos chárter, por causas ajenas a nuestra organización y dependiendo de la disponibilidad de la Cía. y de los slots aeroportuarios/portuarios de las distintas ciudades participantes, el inicio y el final del circuito pueden adelantarse o retrasarse, acortándose o ampliándose el tiempo de estancia en destino. Si ocurriera dicho suceso, KANVOY como empresa organizadora no tiene responsabilidad alguna.
Siendo la acomodación establecida de turista/lobby bar si no se especifica otro tipo de acomodación en el contrato original de reserva del grupo, en el apartado “el precio incluye”.
DOCUMENTACIÓN Todos los usuarios, sin excepción, deberán llevar en regla su documentación personal y familiar correspondiente, sea el pasaporte o el D.N.I.
Los documentos tanto personales como familiares de todos los pasajeros (niños incluidos) deberán estar en regla y el cliente habrá de comprobar que el pasaporte, visado o DNI son los exigidos por la reglamentación del país que va a visitar (consulte para cada destino en las embajadas y consulados correspondientes). Serán por cuenta del cliente la obtención del pasaporte o cualquier otro requisito exigido. Los ciudadanos de otras nacionalidades deberán consultar con las representaciones consulares u organismos competentes los requisitos de entrada en cada país. En caso de ser rechazada por alguna autoridad la documentación ósea denegada la entrada en el país por no cumplir los requisitos que se exigen o por no poseer la misma, KANVOY no será responsable de los gastos adicionales ni hará devolución del precio del viaje.
                    </div>
                </div> 
            </div> 
        </section>
        <section id="trip-preguntas-frecuentes">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="text-uppercase adventure-list experience"> 
                        <div data-wow-duration="1s" data-wow-delay="0.1s" class="col-md-12 col-sm-12 animated fadeInUp"> 
                            <a href="#"> 
                                <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/actividades.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center">                                     
                                    <h5>PREGUNTAS FRECUENTES</h5> 
                                </div> 
                            </a>
                        </div>
                    </div> <!-- /.adventure list --> 
                </div> 
            </div> 
        </section>
        <section class="adventure-select" style="margin-bottom: 600px; font-size:15px; font-family: serif">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="col-xs-12 col-md-4">
¿Qué es el Fin de Curso Mallorca Island Experience?</br>
respuesta</br>
¿Por qué viajar con kanvoy?</br>
¿De qué se tienen que encargar los estudiantes para realizar el viaje?</br>
¿Qué son los “asistentes/coordinadores”?</br>
¿Tengo los traslados incluidos?</br>
¿Qué podemos hacer durante nuestra estancia en Mallorca?</br>
¿Dónde nos encontraremos con los “asistentes/coordinadores” de Kanvoy?</br>
</div>
                    <div class="col-xs-12 col-md-4">
¿Cómo reservamos los tickets de actividades?
¿Es obligatorio que todo nuestro grupo hagamos las mismas excursiones o actividades?
¿Puedo contratar mi seguro de viaje?
¿Qué documentación necesito para viajar?
¿Cuántas maletas puedo llevar en el viaje?
¿Están incluidas las comidas en el viaje?
¿Puede variar el precio del Viaje Fin de Curso Mallorca Island Experience?
¿Puede viajar con nuestro grupo compañeros/as que no estén estudiando con nosotros?
                    </div>
                    <div class="col-xs-12 col-md-4">
¿Cómo puedo obtener un descuento en mi viaje?
¿Cuáles son los métodos de pago?
¿Una vez hecha la reserva, ¿cómo puedo informarme de todo lo relativo al viaje?
¿Puedo Viajar siendo menor, podré acceder a las discotecas?
¿Queremos hacer unas camisetas para llevarlas al viaje, y otras tantas para venderlas. ¿Podéis hacérnoslas vosotros?
¿Cuándo llegamos a destino, ¿hay alguien que pueda ayudarnos si surge algún imprevisto?

                    </div>
                </div> 
            </div> 
        </section>
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> 
            <span>Go Up</span>
            <span class="lefty">unfugitive</span> 
        </button> <!-- /.go up arrow --> 
    </main> <!-- /.main content --> <!-- footer content --> 
    <?= $this->load->view('includes/template/footer'); ?>
</div> <!-- /.page content --> <!-- plugins we use --> 
<script src="js/functions.js"></script> <!-- snazzy maps --> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVZtQkD2-J7WPeAGKw1U9akvqZ1IsEKvA" type="text/javascript"></script>
    <script> 
      google.maps.event.addDomListener(window, 'load', initMap3);
      var markericon = '<?= base_url() ?>images/map-locator.png';
      var map1, map2, map3;
      function initMap3() {
          var mapOptions1 = {
              center: new google.maps.LatLng(50.763492,27.206607),
              zoom: 4,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map');
          map1 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
            map1.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng(50.763492,27.206607),
            map: map1
          });
      }
      function initMap4() {
          var mapOptions1 = {
              center: new google.maps.LatLng(50.763492,27.206607),
              zoom: 4,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map2');
          map2 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
                map2.setOptions({ 'draggable': false });
            }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng(50.763492,27.206607),
            map: map2
          });
     
      }
      function initMap5() {
          var mapOptions1 = {
              center: new google.maps.LatLng(50.763492,27.206607),
              zoom: 4,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map3');
          map3 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            map3.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng(50.763492,27.206607),
            map: map3
          });     
      }
    </script>
    <script src="<?= base_url() ?>js/main.js"></script>
</body>
</html>