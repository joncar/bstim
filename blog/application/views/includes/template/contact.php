<section data-bg="<?= base_url() ?>images/mountains_bg.jpg" class="contacts-section" style="background-image: url(<?= base_url() ?>images/mountains_bg.jpg);">
    <div class="container text-center"> 
        <div class="row"> 
            <div class="col-md-12">
                <div data-wow-duration="1s" data-wow-delay="0.1s" class="user-avatar wow slideInDown" style="animation-duration: 1s; animation-delay: 0.1s; animation-name: none;">
                    <img class="img-responsive" alt="user-avatar" src="<?= base_url() ?>images/avatar.jpeg"> 
                </div> 
                <h1 class="text-uppercase">Hablemos!</h1>
            </div>
        </div> 
        <div class="row"> 
            <div class="col-md-12"> 
                <p style="margin: 0px;">Si le ha quedado alguna consulta al respecto o desea que le ampliemos la información de algún punto en concreto respecto al viaje de su hijo, no dude en llamar a nuestras operadoras quiénes estarán encantadas de atenderle:
                
</p>
                <p class="divc" style="margin: 0px;">overspicing</p>
                <span class="poll">pithecanthropid megalomania</span>
                <span class="lefty">respondency</span> 
            </div>
        </div>
        <div class="row">
            <ul class="col-md-12 text-uppercase list-inline contacts-list"> 
                <li data-wow-delay="0.1s" class="wow fadeIn" style="animation-delay: 0.1s; animation-name: none;"> 
                    <a href="#"> 
                        <i class="icon icon-facebook"></i> 
                        <span>Facebook</span>
                        <span class="divc">noninfantry</span>
                    </a>
                    <p class="fitz" style="margin: 0px;">exudative</p>
                    <span class="lehi">incardinating unhuntable</span>
                </li>
                <li data-wow-delay="0.3s" class="wow fadeIn" style="animation-delay: 0.3s; animation-name: none;"> 
                    <a href="tel:123-456-003"> 
                        <i class="icon icon-phone"></i>
                        <span>881 992303</span>
                        <span class="divc">exudative</span>
                    </a>
                    <p class="fitz" style="margin: 0px;">admiringly</p>
                    <span class="divc">dynamotor revalidation</span>
                </li>
                <li data-wow-delay="0.5s" class="wow fadeIn" style="animation-delay: 0.5s; animation-name: none;"> 
                    <a href="mailto:info@miex.me">
                        <i class="icon icon-envelope"></i> 
                        <span>info@miex.me</span>
                        <span class="mydiv">neogean</span> 
                    </a>
                    <p class="lefty" style="margin: 0px;">supraorbital</p>
                    <span class="frow">preeliminate remodifying</span> 
                </li> 
            </ul> 
        </div> 
        <div class="row"> 
            <div class="col-md-12 text-uppercase text-center"> 
                <a data-wow-delay="0.2s" class="btn book-btn wow slideInUp" href="#" style="animation-delay: 0.2s; animation-name: none;">Contrátalo ahora</a>
                <p class="mydiv" style="margin: 0px;">saleability</p>
                <span class="coma">gloriousness hexadecane</span>
            </div> 
        </div>
    </div>
</section> <!-- go up arrow --> 
